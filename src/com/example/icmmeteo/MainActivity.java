package com.example.icmmeteo;

import java.util.ArrayList;





import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.icmmeteo.adapter.NavDrawerListAdapter;
import com.example.icmmeteo.cppNative.CppNative;
import com.example.icmmeteo.model.NavDrawerItem;
import com.example.icmmeteo.view.AddForecastFragment;
import com.example.icmmeteo.view.mainFragments.BuyFragment;
import com.example.icmmeteo.view.mainFragments.CreditsFragment;
import com.example.icmmeteo.view.mainFragments.HelpFragment;
import com.example.icmmeteo.view.mainFragments.HomeAddForecastFragment;
import com.example.icmmeteo.view.mainFragments.HomeFragment;
import com.example.icmmeteo.view.mainFragments.MoreAppsFragment;
import com.example.icmmeteo.view.mainFragments.WidgetsFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;


//import android.view.Menu;

public class MainActivity extends SherlockFragmentActivity implements
		ICMActivity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// fragmenty generowane przy starcie, nie przelacza sie ich
	private HomeFragment homeF;
	private BuyFragment buyF;
	private CreditsFragment creditsF;
	private HelpFragment helpF;
	private AddForecastFragment addForecastF;
	private WidgetsFragment widgetsF;
	private MoreAppsFragment moreAppsF;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	FragmentManager fragmentManager = getSupportFragmentManager();
	
	FrameLayout bottomFrame;
	FrameLayout adFrame;
	
	Button xButton;

	static {
		System.loadLibrary("c++_shared");
		System.loadLibrary("ICMMeteo");
	}
	
	public void showThxAdsToast()
	{
		LayoutInflater li = this.getLayoutInflater();
		View layout = li.inflate(R.layout.toast_layout, null);
		Toast toast = new Toast(this);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 10);
		toast.setView(layout);
		toast.show();
	}
	
	private AdView adView;

	private AdView getAdView()
	{
		if (adView.getParent() != null)
		{
			((ViewGroup) adView.getParent()).removeView(adView);
		}
		return adView;
	}

	private boolean adMobClicked;

	private void initAdMob()
	{
		adView = new AdView(this);
		adView.setAdSize(AdSize.BANNER);
		adView.setAdUnitId(getResources().getString(R.string.admob_id));
		adView.setAdListener(new AdListener() {


			@Override
			public void onAdLoaded()
			{
				super.onAdLoaded();
				if (!adMobClicked)
				{
					adFrame.addView(getAdView());
					bottomFrame.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onAdLeftApplication()
			{
				super.onAdLeftApplication();
				bottomFrame.setVisibility(View.GONE);
				adMobClicked = true;
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.toast_thx_text), Toast.LENGTH_LONG).show();
				
				//showThxAdsToast();
			}
		});

		AdRequest builder = new AdRequest.Builder().build();
		adView.loadAd(builder);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CppNative nat = CppNative.INSTANCE;
		boolean loaded = nat.init();

		if (loaded == false)
			Toast.makeText(getApplicationContext(),
					"GUNWO Z LADOWANIEM BIBLIOTEKI", Toast.LENGTH_LONG).show();

		createFragments();

		setContentView(R.layout.activity_main);
		
			bottomFrame = (FrameLayout) findViewById(R.id.bottomFrame);
			adFrame = (FrameLayout) findViewById(R.id.adFrame);
			xButton = (Button) findViewById(R.id.xButton); // tego x to chyba wyjebe w ogole
			if (adView == null)
				initAdMob();
			
			xButton.setOnClickListener( new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.toast_disablead_text), Toast.LENGTH_LONG).show();
					bottomFrame.setVisibility(View.GONE);
					
					
				}
			});
		
//        AdView ad = (AdView) findViewById(R.id.adView);
//        AdRequest req = new AdRequest.Builder().addTestDevice("f55077249a3d60e4").build();
//        ad.loadAd(req);

		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
//		 Find People
//		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
//				.getResourceId(1, -1)));
		// Photos
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		// Communities, Will add a counter here
//		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
//				.getResourceId(3, -1), true, "22"));
		// Pages
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		// What's hot, We will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			@Override
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}

	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getSupportMenuInflater().inflate(R.menu.empty, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == android.R.id.home) {

			if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				mDrawerLayout.openDrawer(mDrawerList);
			}
		}

		return super.onOptionsItemSelected(item);
	}

	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_settings).setVisible(!drawerOpen);

		{
			for (int i = 0; i < menu.size(); i++)
				menu.getItem(i).setVisible(!drawerOpen);
		}

		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
//		case 0:
//			fragment = homeF;
//			homeF.setPosition(position);
//			break;
//		case 1:
//			fragment = widgetsF;
//			widgetsF.setPosition(position);
//			break;
//		case 2:
//			fragment = helpF;
//			helpF.setPosition(position);
//			break;
//		case 3:
//			fragment = buyF;
//			buyF.setPosition(position);
//			break;
//		case 4:
//			fragment = moreAppsF;
//			moreAppsF.setPosition(position);
//			break;
//		case 5:
//			fragment = creditsF;
//			creditsF.setPosition(position);
//			// fragment = new Reseller();

		case 0:
			fragment = homeF;
			homeF.setPosition(position);
			break;
		case 1:
			fragment = helpF;
			helpF.setPosition(position);
			break;
		case 2:
			fragment = moreAppsF;
			moreAppsF.setPosition(position);
			break;
		case 3:
			fragment = creditsF;
			creditsF.setPosition(position);
			break;

		default:
			break;
		}

		if (fragment != null) {
			
			
			Fragment current = fragmentManager
					.findFragmentByTag("AddForecastFragment");
			
			//Log.d("FRAGMENTS_DEBUG", "Sprawdzanie czy jest dodawanie prognozy w managerze: " + (current != null) ); //+ " i czy jest widoczny: " + current.isVisible() );

			if (current != null && current.isVisible())
				fragmentManager
						.beginTransaction()
						.replace(R.id.frame_container, fragment,
								((ICMFragmentClass) fragment).getNAME())
						.commit();
			else
				fragmentManager
						.beginTransaction()
						.replace(R.id.frame_container, fragment,
								((ICMFragmentClass) fragment).getNAME())
						.addToBackStack(null).commit();
			
			


			

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			// setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void createFragments() {

		homeF = new HomeFragment();
		buyF = new BuyFragment();
		creditsF = new CreditsFragment();
		helpF = new HelpFragment();
		addForecastF = new HomeAddForecastFragment();
		widgetsF = new WidgetsFragment();
		moreAppsF = new MoreAppsFragment();

	}
	


	public void setMenuSelection(int position) {
		mDrawerList.setItemChecked(position, true);
		mDrawerList.setSelection(position);
	}

	@Override
	public void onBackPressed() {
		// fragmentManager = getSupportFragmentManager();
		// System.out.println(fragmentManager.getBackStackEntryCount());
		if (fragmentManager.getBackStackEntryCount() == 1){
			fragmentManager.popBackStack();
			findViewById(R.id.frame_container).setVisibility(View.INVISIBLE);
		}
			

		super.onBackPressed();
	}

	public HomeFragment getHomeFragment() {
		return homeF;
	}

	public HomeFragment getHomeF() {
		return homeF;
	}

	public BuyFragment getBuyF() {
		return buyF;
	}

	public CreditsFragment getCreditsF() {
		return creditsF;
	}

	public HelpFragment getHelpF() {
		return helpF;
	}

	public AddForecastFragment getAddForecastF() {
		return addForecastF;
	}

	public WidgetsFragment getWidgetsF() {
		return widgetsF;
	}

	public MoreAppsFragment getMoreAppsF() {
		return moreAppsF;
	}

}