package com.example.icmmeteo.cppNative;

public class FORECAST_TYPE
{
    public static int blank = -1;
    public static int temperature = 0;
    public static int wind = 1;
    public static int pressure = 2;
    public static int rain = 3;
    public static int clouds = 4;  
    public static int humidity = 5;
}
