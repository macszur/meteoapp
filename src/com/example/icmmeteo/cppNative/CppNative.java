package com.example.icmmeteo.cppNative;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AndroidException;

import com.example.icmmeteo.model.CityItem;

public enum CppNative {
	INSTANCE; // - teraz jest to SINGLETON !
	
	//public native Location[] getAllLocations();
	
	private boolean properInit = false;
	private CityItem[] cities = null;
	
	private native boolean initializeLib();
	public native int loadCitiesList();
	public native int fillCitiesList(CityItem[] items);
	public native boolean loadNewestWeatherForecast(CityItem location);
	public native ForecastResult getNewestWeatherForecastOnAndroid(int ftype);
	public native byte[] getLoadedWeatherBitmapBytes();
	
	public native int testDownloadImage();
	
	
	public boolean init()
	{
		properInit = initializeLib();
		
		return true;
	}
	
	public CityItem[] getCities() throws AndroidException
	{
		if(cities == null)
		{
			int ile = loadCitiesList();
			cities = new CityItem[ile];
			for(int i = 0; i < ile; ++i)
				cities[i] = new CityItem();
			int git = fillCitiesList(cities);
			if(git != 1)
				throw new android.util.AndroidException("COS SIE ZLE ZROBILO");
		}
		
		return cities;
	}
	
	public Bitmap getLoadedWeatherBitmap()
	{
		//Bitmap bt = null;
		
		byte[] rgbData = getLoadedWeatherBitmapBytes();
				int nrOfPixels = rgbData.length / 4; // Three bytes per pixel.
				int pixels[] = new int[nrOfPixels];
				for(int i = 0; i < nrOfPixels; i++) {
				   int r = rgbData[4*i] & 0xff;
				   int g = rgbData[4*i + 1] & 0xff;
				   int b = rgbData[4*i + 2] & 0xff;
				   int a = rgbData[4*i + 3] & 0xff;
				   pixels[i] = Color.argb(a, r, g, b);
				}
				Bitmap bitmap = Bitmap.createBitmap(pixels, 540, 660, Bitmap.Config.ARGB_8888);
		
		return bitmap;
	}
	
	public void getNewestWeatherForecast(int ftype)
	{
		//ForecastResult fResult = new ForecastResult();
		ForecastResult fResult = getNewestWeatherForecastOnAndroid(ftype);
	}

}
