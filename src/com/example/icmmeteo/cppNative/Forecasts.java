package com.example.icmmeteo.cppNative;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class Forecasts implements Serializable {
	
//	private Bitmap image;
	
	private float[] temperature;
	private float[] wind;
	private float[] pressure;
	private float[] rain;
	private float[] cloud;
	
	
	
	public float[] getCloud() {
		return cloud;
	}
	public void setCloud(float[] cloud) {
		this.cloud = cloud;
	}
	public int[] getTempScale() {
		return tempScale;
	}
	public void setTempScale(int[] tempScale) {
		this.tempScale = tempScale;
	}
	public int[] getRainScale() {
		return rainScale;
	}
	public void setRainScale(int[] rainScale) {
		this.rainScale = rainScale;
	}
	public int[] getPressScale() {
		return pressScale;
	}
	public void setPressScale(int[] pressScale) {
		this.pressScale = pressScale;
		this.pressScale[1] = this.pressScale[1] + 5;
	}
	public int[] getWindScale() {
		return windScale;
	}
	public void setWindScale(int[] windScale) {
		this.windScale = windScale;
	}

	private int[] tempScale;
	private int[] rainScale;
	private int[] pressScale;
	private int[] windScale;
	private int[] cloudScale;
	
	public int[] getCloudScale() {
		return cloudScale;
	}
	public void setCloudScale(int[] cloudScale) {
		this.cloudScale = cloudScale;
	}
	public float[] getRain() {
		return rain;
	}
	public void setRain(float[] rain) {
		this.rain = rain;
	}

	private double[] timestamps;
	
	public double[] getTimestamps() {
		return timestamps;
	}
	public void setTimestamps(double[] timestamps) {
		this.timestamps = timestamps;
	}
	public float[] getTemperature() {
		return temperature;
	}
	public void setTemperature(float[] temperature) {
		this.temperature = temperature;
	}
	public float[] getWind() {
		return wind;
	}
	public void setWind(float[] wind) {
		this.wind = wind;
	}
	public float[] getPressure() {
		return pressure;
	}
	public void setPressure(float[] pressure) {
		this.pressure = pressure;
	}
	
	public int getClosestTimestampIndex(){
		
		Date now = new Date();

		double currentTimestamp = now.getTime()/1000;

		double distance = Math.abs(timestamps[0] - currentTimestamp);
		int idx = 0;
		for(int c = 1; c < timestamps.length; c++){
		    double cdistance = Math.abs(timestamps[c] - currentTimestamp);
		    if(cdistance < distance){
		        idx = c;
		        distance = cdistance;
		    }
		}
		return idx;
	}

	public float getCurrentPressure(){
		return Math.round(pressure[getClosestTimestampIndex()]);
				//Math.round(pressure[getClosestTimestampIndex(now.getTime()/1000)]);
	}
	
	public float getCurrentTemperature(){
		return Math.round(temperature[getClosestTimestampIndex()]);
	}
	
	public float getCurrentWind(){
		return Math.round(wind[getClosestTimestampIndex()]);
	}
	
	public float getCurrentClouds(){
		return Math.round(cloud[getClosestTimestampIndex()]);
	}
	
	public float getCurrentRain(){
		return Math.round(rain[getClosestTimestampIndex()]);
	}
	
	/** Zwraca jaka jest aktualnie pogoda
	 * @return  0 - deszcz, 1 - slonce, 2 - czesciowe zachmurzenie, 3 - zachmurzenie
	 */
	public int getCurrentWeather(){
		float currentRain = getCurrentRain();
		float currentClouds = getCurrentClouds();
		
		if (currentRain>0)
			return 0;	// deszcz
		else if (currentClouds<3)
			return 1;	// slonce
		else if (currentClouds<6)
			return 2; // czesciowe zachmurzenie
		else 
			return 3;	// zachmurzenie
		
	}
	
	public float[] getMarkerTable(){		
		float[] result = new float[cloud.length];
		int nowIndex = getClosestTimestampIndex();
		boolean contin = true;
		for(int i = 0 ; i< result.length ; i++){
			if (contin && i==nowIndex){
				result[i]=1500;
				contin = false;
			} else {
				result[i] = 0;
			}
		}
		
		return result;
		
		
	}
	
	public boolean isUpToDate(){
		Date now = new Date();
		
		if (now.getTime()/1000 - timestamps[0] > 5 * 60 *60 )	//TODO zmienic
			return false;		
		else
			return true;	
	}
	

}
