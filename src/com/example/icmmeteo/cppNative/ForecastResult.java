package com.example.icmmeteo.cppNative;

public class ForecastResult
{
	public double[] timeStamps;
    public float[] values;
    public int ftype;
    public int[] scale;
    
    public ForecastResult(double[] tsData, float[] valTab, int[] _scale) {
		timeStamps = tsData;
		values = valTab;
		scale = _scale;
	}
}

//struct forecastResult
//{
//    int pointsNum = 0;
//    plotPoint * points;
//    FORECAST_TYPE ftype;
//};