package com.example.icmmeteo.view.mainFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;

public class CreditsFragment extends ICMFragmentClass {

	public CreditsFragment() {
		setNAME("CreditsFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setTITLE( getResources().getString(R.string.credits_fragment_title) );
		View rootView = inflater.inflate(R.layout.fragment_credits, container,
				false);
		manageLayout();
		return rootView;
	}

}