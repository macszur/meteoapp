package com.example.icmmeteo.view.mainFragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;
import com.example.icmmeteo.logic.manageXml.XMLManager;
import com.example.icmmeteo.model.CityItem;
import com.example.icmmeteo.view.MapActivity;
import com.example.icmmeteo.adapter.CityListAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class HomeFragment extends ICMFragmentClass {

	ListView listview;
	ArrayAdapter<CityItem> adapter;

	

	public HomeFragment() {
		setNAME("HomeFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setTITLE( getResources().getString(R.string.home_fragment_title) );

		View rootView = inflater.inflate(R.layout.fragment_home, container,
				false);
		listview = (ListView) rootView.findViewById(R.id.homeListview);


		XMLManager manager = new XMLManager(getActivity()
				.getApplicationContext());

//        AdView ad = (AdView) rootView.findViewById(R.id.adView);
//        AdRequest req = new AdRequest.Builder().addTestDevice("f55077249a3d60e4").build();
//        ad.loadAd(req);

		adapter = new CityListAdapter(
				getActivity().getApplicationContext(), R.layout.city_list_item_2,
				manager.createCityItemsList());

		listview.setAdapter(adapter);
		// registerForContextMenu(listview);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				CityItem item = (CityItem) parent.getAdapter()
						.getItem(position);
				
				Bundle bundl = new Bundle();
				bundl.putString("name", item.getName());				
				bundl.putInt("x", item.getX());
				bundl.putInt("y", item.getY());
				

				
				Fragment fragment = new ForecastTabsFragment();
				fragment.setArguments(bundl);
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, fragment).addToBackStack(null)
						.commit();

				// System.out.println("KLIK KLIK");

			}
		});
		

		listview.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				CityItem item = (CityItem) parent.getAdapter()
						.getItem(position);
				showDialog(item);
				return true;
			}
		});


		manageLayout();
		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		// menu.
		inflater.inflate(R.menu.add, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items

		if (item.getItemId() == R.id.action_add) {
			changeFragment(new HomeAddForecastFragment(), "Nowe miejsce");
			return true;
		} else
			return super.onOptionsItemSelected(item);

	}

	private void changeFragment(SherlockFragment fragment, String title) {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.frame_container, fragment, ((ICMFragmentClass) fragment ).getNAME() ).addToBackStack(null)
				.commit();
		getActivity().setTitle(title);
	}


	private void showDialog(final CityItem item) {
		final CharSequence[] options = {
				getResources().getString(R.string.toast_menu_delete),
				getResources().getString(R.string.toast_menu_exit) };

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(item.getName());

		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == 0)// Option 1
				{
					XMLManager manager = new XMLManager(getActivity()
							.getApplicationContext());
					manager.removeCity(item.getCityID());
					
					adapter = new CityListAdapter(
							getActivity().getApplicationContext(), R.layout.city_list_item_2,
							manager.createCityItemsList());

					listview.setAdapter(adapter);
				} else if (which == 1)// Option 2

				{
				}
				// etc..
			}
		});

		AlertDialog dlg = builder.create();
		dlg.show();
	}


 
}