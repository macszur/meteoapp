package com.example.icmmeteo.view.mainFragments;

import java.lang.reflect.Field;
import java.util.Date;

import android.app.Activity;
import android.app.FragmentManager.BackStackEntry;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.MainActivity;
import com.example.icmmeteo.R;
import com.example.icmmeteo.adapter.ViewPagerAdapter;
import com.example.icmmeteo.cppNative.CppNative;
import com.example.icmmeteo.cppNative.FORECAST_TYPE;
import com.example.icmmeteo.cppNative.ForecastResult;
import com.example.icmmeteo.cppNative.Forecasts;
import com.example.icmmeteo.logic.ImageUtils;
import com.example.icmmeteo.logic.NetworkUtils;
import com.example.icmmeteo.model.CityItem;
import com.google.android.gms.drive.internal.GetMetadataRequest;
import com.google.gson.Gson;


public class ForecastTabsFragment extends ICMFragmentClass {

	int x;
	int y;
	String forecastName;
	Forecasts forecasts;
	ViewPager mViewPager;
	LayoutInflater currentInflater;
	View currentView;
	ViewGroup currentContainer;
	boolean initialized = false;

	public ForecastTabsFragment() {
		setNAME("ForecastTabsFragment");
	}
	
//	@Override
//	public void onAttach(android.app.Activity activity) {
//		Bundle bundl = getArguments();
//		x = bundl.getInt("x");
//		y = bundl.getInt("y");
//		forecastName = bundl.getString("name");
//		new DownloadForecastsTask().execute(x,y);
//		super.onAttach(activity);
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("CHART_DEBUG", "onCreateView ForecastTabFragment. initialized: " + initialized);
		setTITLE(getResources().getString(R.string.home_fragment_title));
		
		View view = inflater.inflate(R.layout.fragment_forecast_tabs,
				container, false);
		// Locate the ViewPager in viewpager_main.xml
		mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
		// Set the ViewPagerAdapter into ViewPager
		
		currentInflater = inflater;
		currentView = view;
		currentContainer = container;

		
		if(savedInstanceState==null){
		Bundle bundl = getArguments();
		x = bundl.getInt("x");
		y = bundl.getInt("y");
		forecastName = bundl.getString("name");
		handleForecast(forecastName, x, y, getActivity().getApplicationContext());

		
		//new DownloadForecastsTask().execute(x,y);

		
	}else{
		forecasts = (Forecasts) savedInstanceState.getSerializable("forecasts");
		currentView.findViewById(R.id.progressBar1).setVisibility(View.INVISIBLE);
		mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), forecasts, forecastName, x, y));
		
	}
		

		
//		
		manageLayout();
		return view;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class
					.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	private class DownloadForecastsTask extends AsyncTask<Integer, Void, Forecasts> {
	/**
	 * The system calls this to perform work in a worker thread and delivers
	 * it the parameters given to AsyncTask.execute()
	 */
	protected Forecasts doInBackground(Integer... coordinates) {
		
		CppNative nat = CppNative.INSTANCE;
		
		Forecasts result = new Forecasts();
		CityItem location = new CityItem();
		location.setX(coordinates[0]);
	    location.setY(coordinates[1]);

		nat.loadNewestWeatherForecast(location);
		
		Log.i("ICMMeteo", "zaraz przetworze najnowsza pogode z nativa");
		ForecastResult fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.temperature);
		result.setTemperature(fr.values);
		result.setTempScale(fr.scale);
		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.wind);
		result.setWind(fr.values);
		result.setWindScale(fr.scale);
		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.pressure);
		result.setPressure(fr.values);
		result.setPressScale(fr.scale);
		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.rain);
		result.setRain(fr.values);
		result.setRainScale(fr.scale);
		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.clouds);
		result.setCloud(fr.values);
		result.setCloudScale(fr.scale);	
		Log.i("ICMMeteo", "skala chmur: " + fr.scale[0] + " " + fr.scale[1]);
		result.setTimestamps(fr.timeStamps);
		ImageUtils.storeForecastImage(nat.getLoadedWeatherBitmap(), forecastName+x+y);
		//result.setImage(nat.getLoadedWeatherBitmap());
		
		return result;
	}
	
	/**
	 * The system calls this to perform work in the UI thread and delivers
	 * the result from doInBackground()
	 */
	protected void onPostExecute(Forecasts result) {
//		String napis = "";
//		for(int i = 0; i < val.length; ++i)
//		{
//			napis += val[i] + " ";
//		}
//		Toast.makeText(getActivity().getApplicationContext(),
//				"Temp w WWA: " + napis, Toast.LENGTH_LONG).show();
		
//		currentView= currentInflater.inflate(R.layout.fragment_forecast_tabs,
//				currentContainer, false);
//		// Locate the ViewPager in viewpager_main.xml
//		mViewPager = (ViewPager) currentView.findViewById(R.id.viewPager);
//		// Set the ViewPagerAdapter into ViewPager
		
		forecasts = result;
		currentView.findViewById(R.id.progressBar1).setVisibility(View.INVISIBLE);
		mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), forecasts, forecastName, x,y));
		saveForecast(forecastName, x, y, result);
	}
}

//	public void tabHelpClickFunction(View view) {
//		new testDownloadTask().execute(1,1);
//		
//	}
	
	private void handleForecast(String name, int x, int y, Context context){
		
		boolean networkAvalible = NetworkUtils.isNetworkAvalible(getActivity().getApplicationContext());
		Forecasts forecast = getForecast(name, x, y, context);
		boolean isUpToDate;
		if (forecast != null)
			isUpToDate = forecast.isUpToDate();
		else 
			isUpToDate = false;
			
		if (isUpToDate){
			Log.i("ICM_METEO_HANDLING_FORECAST", "Archiwalna prognoza aktualna - wczytuje prognoze z pamieci");
			forecasts = forecast;
			currentView.findViewById(R.id.progressBar1).setVisibility(View.INVISIBLE);
			mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), forecasts, forecastName, x,y));
			return;
		}
		if(networkAvalible){
			Log.i("ICM_METEO_HANDLING_FORECAST", "Sciagam prognoze z serwera");
			new DownloadForecastsTask().execute(x,y);
			return;
		}
		if(!networkAvalible && forecast!=null){
			Log.i("ICM_METEO_HANDLING_FORECAST", "Archiwalna prognoza nieaktualna, ale nie ma internetu - wczytuje prognoze z pamieci");
			forecasts = forecast;
			currentView.findViewById(R.id.progressBar1).setVisibility(View.INVISIBLE);
			mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), forecasts, forecastName,x ,y));
			Toast.makeText(context,
					getResources().getString(R.string.toast_archival_forecast), Toast.LENGTH_LONG).show();
			return;
			
		}
		if(!networkAvalible && forecast==null){
			Log.i("ICM_METEO_HANDLING_FORECAST", "Nie ma ani archiwalnej pogody, ani internetu - wracam do menu");
			Toast.makeText(context,
					getResources().getString(R.string.toast_noarchival_forecast), Toast.LENGTH_LONG).show();
			FragmentManager fragmentManager = getFragmentManager();

			fragmentManager.popBackStack();

			return;
			
		}
				
		
		
	}
	
	private void saveForecast(String name, int x, int y, Forecasts forecast){
		
		  SharedPreferences myPrefs = getActivity().getSharedPreferences("Data",
	        		Activity.MODE_MULTI_PROCESS);
	        SharedPreferences.Editor prefsEditor = myPrefs.edit();
	       // prefsEditor.putString(file_name, data);
	        Gson gson = new Gson();
	        String json = gson.toJson(forecast);
	        prefsEditor.putString(name+x+y, json);
	        prefsEditor.commit();	
	}
	
    private Forecasts getForecast(String name, int x, int y, Context context) {
        SharedPreferences myPrefs = context.getSharedPreferences("Data",
                Activity.MODE_MULTI_PROCESS);
        Gson gson = new Gson();
        String json = myPrefs.getString(name+x+y, "");
        if (json.isEmpty())
        	return null;
        Forecasts forecast = gson.fromJson(json, Forecasts.class);     
        return forecast;
    }

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putSerializable("forecasts", forecasts);
	}
	
	

}