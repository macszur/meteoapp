package com.example.icmmeteo.view.mainFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;

public class WidgetsFragment extends ICMFragmentClass {


	
	public WidgetsFragment() {
		setNAME("WidgetsFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setTITLE( getResources().getString(R.string.widgets_fragment_title) );
		View rootView = inflater.inflate(R.layout.fragment_widgets, container,
				false);
		manageLayout();
		return rootView;
	}


}