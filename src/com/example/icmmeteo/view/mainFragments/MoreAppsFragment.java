package com.example.icmmeteo.view.mainFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;



public class MoreAppsFragment extends ICMFragmentClass {
	Button btn;


	public MoreAppsFragment() {
		setNAME("MoreAppsFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setTITLE( getResources().getString(R.string.more_apps_fragment_title) );
		View rootView = inflater.inflate(R.layout.fragment_more_apps, container,
				false);
		
		btn = (Button) rootView.findViewById(R.id.button1);
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Toast.makeText(getActivity().getApplicationContext(),
						"Nie ma nas jeszce w Google Play Store :(", Toast.LENGTH_LONG).show();

			}
		});
		manageLayout();
		return rootView;
	}


}