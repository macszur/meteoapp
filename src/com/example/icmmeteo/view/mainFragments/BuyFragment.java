package com.example.icmmeteo.view.mainFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class BuyFragment extends ICMFragmentClass {

	public BuyFragment() {
		setNAME("BuyFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setTITLE(getResources().getString(R.string.buy_fragment_title));
		View rootView = inflater.inflate(R.layout.fragment_buy, container,
				false);
		
//		GoogleMap map = ((MapFragment) getFragmentManager()
//                .findFragmentById(R.id.map)).getMap();
		
//			    Marker hamburg = map.addMarker(new MarkerOptions().position(HAMBURG)
//			        .title("Hamburg"));
//			    Marker kiel = map.addMarker(new MarkerOptions()
//			        .position(KIEL)
//			        .title("Kiel")
//			        .snippet("Kiel is cool")
//			        .icon(BitmapDescriptorFactory
//			            .fromResource(R.drawable.ic_launcher)));
//
//			    // Move the camera instantly to hamburg with a zoom of 15.
//			    map.moveCamera(CameraUpdateFactory.newLatLngZoom(HAMBURG, 15));
//
//			    // Zoom in, animating the camera.
//			    map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
		
//		  LatLng sydney = new LatLng(-33.867, 151.206);
//
//	        map.setMyLocationEnabled(true);
//	        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));
//
//	        map.addMarker(new MarkerOptions()
//	                .title("Sydney")
//	                .snippet("The most populous city in Australia.")
//	                .position(sydney));
		
		manageLayout();
		return rootView;
	}

}
