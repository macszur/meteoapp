package com.example.icmmeteo.view.mainFragments;

import com.example.icmmeteo.ICMActivity;
import com.example.icmmeteo.R;
import com.example.icmmeteo.logic.manageXml.XMLManager;
import com.example.icmmeteo.model.CityItem;
import com.example.icmmeteo.view.AddForecastFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.AdapterView;

public class HomeAddForecastFragment extends AddForecastFragment {

	@Override
	public void onCityItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		XMLManager manager = new XMLManager(getActivity()
				.getApplicationContext());

		CityItem item = (CityItem) parent.getAdapter()
				.getItem(position);

		manager.addCity(item);

		ICMActivity activity = (ICMActivity) getActivity();
		//activity.getHomeFragment();

		// Fragment fragment = new HomeFragment();
		Fragment fragment = activity.getHomeFragment();
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.popBackStack();
		fragmentManager.beginTransaction()
				.replace(R.id.frame_container, fragment).commit();

		// System.out.println("KLIK KLIK");

	}

}
