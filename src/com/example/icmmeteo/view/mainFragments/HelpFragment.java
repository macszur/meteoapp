package com.example.icmmeteo.view.mainFragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AndroidException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;
import com.example.icmmeteo.adapter.CityListAdapter;
import com.example.icmmeteo.cppNative.CppNative;
import com.example.icmmeteo.cppNative.FORECAST_TYPE;
import com.example.icmmeteo.cppNative.ForecastResult;
import com.example.icmmeteo.model.CityItem;

public class HelpFragment extends ICMFragmentClass {


	Button btn;


	public HelpFragment() {
		setNAME("HelpFragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setTITLE(getResources().getString(R.string.help_fragment_title) );
		View rootView = inflater.inflate(R.layout.fragment_help, container,
				false);


//		btn = (Button) rootView.findViewById(R.id.button1);
//		btn.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				tabHelpClickFunction(arg0);
//
//			}
//		});

		manageLayout();
		return rootView;
	}


	private class testDownloadTask extends AsyncTask<Void, Void, float[]> {
	/**
	 * The system calls this to perform work in a worker thread and delivers
	 * it the parameters given to AsyncTask.execute()
	 */
	protected float[] doInBackground(Void... params) {
		
		CppNative nat = CppNative.INSTANCE;
		

		CityItem location = new CityItem();
		location.setX(250);
	    location.setY(406);

		nat.loadNewestWeatherForecast(location);
		
		Log.i("ICMMeteo", "zaraz przetworze najnowsza pogode z nativa");
		ForecastResult fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.clouds);
		return fr.values;
	}
	
	/**
	 * The system calls this to perform work in the UI thread and delivers
	 * the result from doInBackground()
	 */
	protected void onPostExecute(float[] val) {
		String napis = "";
		for(int i = 0; i < val.length; ++i)
		{
			napis += val[i] + " ";
		}
		Toast.makeText(getActivity().getApplicationContext(),
				"Scala w WWA: " + napis, Toast.LENGTH_LONG).show();
	}
}

	public void tabHelpClickFunction(View view) {
		new testDownloadTask().execute();
		
	}



}