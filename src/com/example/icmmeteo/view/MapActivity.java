package com.example.icmmeteo.view;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import android.app.Activity;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.SyncStateContract.Constants;

import com.example.icmmeteo.R;

public class MapActivity extends Activity {

    private Location location;
	private LatLng myLocation;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_buy);
        GoogleMap map = ((MapFragment) getFragmentManager()
                .findFragmentById(R.id.map)).getMap();

//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                new LatLng(-18.142, 178.431), 2));
        
        map.setMyLocationEnabled(true);

        location = map.getMyLocation();

        if (location != null) {
            myLocation = new LatLng(location.getLatitude(),
                    location.getLongitude());
        }
        
//        CameraUpdate center=
//                CameraUpdateFactory.newLatLng(new LatLng(40.76793169992044,
//                                                         -73.98180484771729));
//            CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);
//
//            map.moveCamera(center);
//            map.animateCamera(zoom);
//            
//            map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16));
        
        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 16));

        // Other supported types include: MAP_TYPE_NORMAL,
        // MAP_TYPE_TERRAIN, MAP_TYPE_HYBRID and MAP_TYPE_NONE
            
     // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location locationCurr = locationManager.getLastKnownLocation(provider);    
        
            CameraPosition cameraPosition = new CameraPosition.Builder()
            .target(new LatLng(locationCurr.getLatitude(), locationCurr.getLongitude())  )    // Sets the center of the map to Mountain View
            .zoom(17)                   // Sets the zoom
            .bearing(0)                // Sets the orientation of the camera to east
            .tilt(30)                   // Sets the tilt of the camera to 30 degrees
            .build();                   // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            
        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }
}