package com.example.icmmeteo.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.AndroidException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.example.icmmeteo.ICMActivity;
import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;
import com.example.icmmeteo.adapter.CityListAdapter;
import com.example.icmmeteo.cppNative.CppNative;
import com.example.icmmeteo.logic.manageXml.XMLManager;
import com.example.icmmeteo.model.CityItem;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public abstract class AddForecastFragment extends ICMFragmentClass {

	ListView listview;
	SearchView mSearchView;
	ArrayAdapter<CityItem> adapter = null;
	CityItem[] objects;
	int licznik = 0;
	private Button mapBtn;

	public AddForecastFragment() {
		setNAME("AddForecastFragment");
	}

	private class LoadCitiesTask extends AsyncTask<Void, Void, CityItem[]> {
		/**
		 * The system calls this to perform work in a worker thread and delivers
		 * it the parameters given to AsyncTask.execute()
		 */
		protected CityItem[] doInBackground(Void... params) {
			CityItem[] list = null;
			try {
				// adapter = new ArrayAdapter<CityItem>(getActivity()
				// .getApplicationContext(), R.layout.city_list_item,
				// CppNative.INSTANCE.getCities()); // NATIVE - CO ZROBI�
				// // �EBY ZA KA�DYM
				// // RAZEM NIE ROBIC
				// // NOWEJ TABLICY I
				// // JEJ NIE
				// // INICJALIZOWAC ITD

				// adapter = new CityListAdapter(getActivity()
				// .getApplicationContext(), R.layout.city_list_item_2,
				// CppNative.INSTANCE.getCities());

				list = CppNative.INSTANCE.getCities();

			} catch (AndroidException e) {
				Toast.makeText(getActivity(), "CHUUUUUJSTWO", Toast.LENGTH_LONG)
						.show();
				e.printStackTrace();
			} // TO
			return list;
		}

		/**
		 * The system calls this to perform work in the UI thread and delivers
		 * the result from doInBackground()
		 */
		protected void onPostExecute(CityItem[] result) {

			adapter = new CityListAdapter(
					getActivity().getApplicationContext(),
					R.layout.city_list_item_2, result);

			listview.setAdapter(adapter);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Debug.startMethodTracing("icmmeteo");
		setTITLE(getResources().getString(R.string.add_forecast_fragment_title));
		View rootView = inflater.inflate(R.layout.fragment_add_forecast,
				container, false);

		// //////////////////////////////////////////////////////////////////
		// /// TU SIE DZIEJE MAGIA

		listview = (ListView) rootView.findViewById(R.id.addForecastListview);
		mapBtn = (Button) rootView.findViewById(R.id.button1);
		
//        AdView ad = (AdView) rootView.findViewById(R.id.adView);
//        AdRequest req = new AdRequest.Builder().addTestDevice("f55077249a3d60e4").build();
//        ad.loadAd(req);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				onCityItemClick( parent,  view,
						 position,  id);

				

			}
		});
		
mapBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent("com.example.icmmeteo.view.MapActivity");
				startActivity(intent);
			}
		});

		// if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
		// new
		// DownloadImageTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		// } else {
		new LoadCitiesTask().execute();
		// }

		// new DownloadImageTask().execute();

		// ///////////////////////////////////////////////////////////////
		// //////////KONIEC MAGII

		// Debug.stopMethodTracing();
		//manageLayout();

		return rootView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.search, menu);
		mSearchView = (SearchView) menu.findItem(R.id.menu_search)
				.getActionView();
		mSearchView.setQueryHint(getResources().getString(R.string.search_hint));

		licznik++;

		Log.d("ICM_METEO", "onCreateOptionsMenu po raz " + licznik);

		//if (licznik == 2) {
		if (true) {
			if (mSearchView == null)
				Log.e("ICM_METEO", "mSearchView jest NULL");

			Log.d("ICM_METEO", "Ustawiam listenera za " + licznik + " razem");

			mSearchView.setOnQueryTextListener(new OnQueryTextListener() {
				@Override
				public boolean onQueryTextChange(String text) {
					
					
					if (adapter== null) {
						Log.d("ICM_METEO", "adapter == null");
						return true;

					} else {

						Log.d("ICM_METEO", "Zmiana tekstu na: " + text);

						adapter.getFilter().filter(text);

						return true;
					}
				}

				@Override
				public boolean onQueryTextSubmit(String text) {
					return true;
				}
			});
		}

		super.onCreateOptionsMenu(menu, inflater);

	}
	public abstract void onCityItemClick(AdapterView<?> parent, View view,
			int position, long id);

}