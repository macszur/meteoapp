package com.example.icmmeteo.view.tabFragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.icmmeteo.R;
import com.example.icmmeteo.cppNative.CppNative;
import com.example.icmmeteo.cppNative.Forecasts;
import com.example.icmmeteo.logic.ImageUtils;
import com.example.icmmeteo.model.CityItem;

public class ImageTabFragment extends SherlockFragment {
	// TODO
	// Generalnie to trzeba zrobic tak, ze jest tylko jedna tabka do tzw 
	// overview pogody, i jedna do wyswietlania wykresow, tyle ze sparametryzowana, 
	// nie potrzeba tyle chujstwa

	
	private Bitmap img;
	private String forecastName;
	private int x;
	private int y;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		View view = inflater.inflate(R.layout.tab_img, container, false);
		
		Bundle bundl = getArguments();
		forecastName = bundl.getString("name");
		x = bundl.getInt("x");
		y = bundl.getInt("y");
		
		img = ImageUtils.loadForecastImage(forecastName+x+y);
		
		//CppNative nat = CppNative.INSTANCE;
		
//		CityItem location = new CityItem();
//		location.setX(250);
//	    location.setY(406);
//
//		nat.loadNewestWeatherForecast(location);
		
		//Bitmap bt = ImageUtils.loadForecastImage(filename)
		
		WindowManager wm = (WindowManager) getActivity().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int screenWidth = size.x;
		int screenHeight = size.y;
		
		final ImageView image = (ImageView) view.findViewById(R.id.imageView1);
		image.setImageBitmap(img);
		
//		int bitmapWidth = image.getWidth();
//		int bitmapHeight = image.getHeight();
//		
//		// set maximum scroll amount (based on center of image)
//	    int maxX = (int)((bitmapWidth / 2) - (screenWidth / 2));
//	    int maxY = (int)((bitmapHeight / 2) - (screenHeight / 2));
//
//	    // set scroll limits
//	    final int maxLeft = (maxX * -1);
//	    final int maxRight = maxX;
//	    final int maxTop = (maxY * -1);
//	    final int maxBottom = maxY;
//
//	    // set touchlistener
//	    image.setOnTouchListener(new View.OnTouchListener()
//	    {
//	        float downX, downY;
//	        int totalX, totalY;
//	        int scrollByX, scrollByY;
//	        
//	        public boolean onTouch(View v, MotionEvent event)
//	        {
//	            float currentX, currentY;
//	            switch (event.getAction())
//	            {
//	                case MotionEvent.ACTION_DOWN:
//	                    downX = event.getX();
//	                    downY = event.getY();
//	                    break;
//
//	                case MotionEvent.ACTION_MOVE:
//	                    currentX = event.getX();
//	                    currentY = event.getY();
//	                    scrollByX = (int)(downX - currentX);
//	                    scrollByY = (int)(downY - currentY);
//
//	                    // scrolling to left side of image (pic moving to the right)
//	                    if (currentX > downX)
//	                    {
//	                        if (totalX == maxLeft)
//	                        {
//	                            scrollByX = 0;
//	                        }
//	                        if (totalX > maxLeft)
//	                        {
//	                            totalX = totalX + scrollByX;
//	                        }
//	                        if (totalX < maxLeft)
//	                        {
//	                            scrollByX = maxLeft - (totalX - scrollByX);
//	                            totalX = maxLeft;
//	                        }
//	                    }
//
//	                    // scrolling to right side of image (pic moving to the left)
//	                    if (currentX < downX)
//	                    {
//	                        if (totalX == maxRight)
//	                        {
//	                            scrollByX = 0;
//	                        }
//	                        if (totalX < maxRight)
//	                        {
//	                            totalX = totalX + scrollByX;
//	                        }
//	                        if (totalX > maxRight)
//	                        {
//	                            scrollByX = maxRight - (totalX - scrollByX);
//	                            totalX = maxRight;
//	                        }
//	                    }
//
//	                    // scrolling to top of image (pic moving to the bottom)
//	                    if (currentY > downY)
//	                    {
//	                        if (totalY == maxTop)
//	                        {
//	                            scrollByY = 0;
//	                        }
//	                        if (totalY > maxTop)
//	                        {
//	                            totalY = totalY + scrollByY;
//	                        }
//	                        if (totalY < maxTop)
//	                        {
//	                            scrollByY = maxTop - (totalY - scrollByY);
//	                            totalY = maxTop;
//	                        }
//	                    }
//
//	                    // scrolling to bottom of image (pic moving to the top)
//	                    if (currentY < downY)
//	                    {
//	                        if (totalY == maxBottom)
//	                        {
//	                            scrollByY = 0;
//	                        }
//	                        if (totalY < maxBottom)
//	                        {
//	                            totalY = totalY + scrollByY;
//	                        }
//	                        if (totalY > maxBottom)
//	                        {
//	                            scrollByY = maxBottom - (totalY - scrollByY);
//	                            totalY = maxBottom;
//	                        }
//	                    }
//
//	                    image.scrollBy(scrollByX, scrollByY);
//	                    downX = currentX;
//	                    downY = currentY;
//	                    break;
//
//	            }
//
//	            return true;
//	        }
//
//	    });
		//image.setImageBitmap(bt);
		//image.setImageDrawable(bt); //czy jak tam chcesz
		
		return view;
	}
}