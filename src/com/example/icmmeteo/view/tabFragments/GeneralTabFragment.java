package com.example.icmmeteo.view.tabFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.icmmeteo.R;
import com.example.icmmeteo.cppNative.Forecasts;

public class GeneralTabFragment extends SherlockFragment {
	// TODO
	// Generalnie to trzeba zrobic tak, ze jest tylko jedna tabka do tzw 
	// overview pogody, i jedna do wyswietlania wykresow, tyle ze sparametryzowana, 
	// nie potrzeba tyle chujstwa
	TextView cityName;
	TextView temperature;
	TextView rain;
	TextView pressure;
	TextView wind;
	ImageView weatherIcon;
	int weatherType;
	Forecasts forecasts;
	String name;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		
		View view = inflater.inflate(R.layout.tab_title, container, false);
		
		View titlePage = view.findViewById(R.id.weather_title_layout);	
		View dataPage = view.findViewById(R.id.weather_data_layout);

		cityName = (TextView) titlePage.findViewById(R.id.cityName);
		temperature = (TextView) titlePage.findViewById(R.id.temperatureText);
		weatherIcon = (ImageView) titlePage.findViewById(R.id.weatherIcon);
		rain = (TextView) dataPage.findViewById(R.id.rain);
		pressure = (TextView) dataPage.findViewById(R.id.pressure);
		wind = (TextView) dataPage.findViewById(R.id.wind);
		
		Bundle args = getArguments();
		forecasts = (Forecasts) args.getSerializable("forecasts");
		name = args.getString("forecastName");
		
		cityName.setText(name);
		temperature.setText(String.valueOf((int)forecasts.getCurrentTemperature()) + "�");
		rain.setText(String.valueOf((int)forecasts.getCurrentRain()) + "mm");
		pressure.setText(String.valueOf((int)forecasts.getCurrentPressure()) + "hPa");
		wind.setText(String.valueOf((int)forecasts.getCurrentWind()) + "m/s");
		
		weatherType = forecasts.getCurrentWeather();
		if (weatherType == 0) // deszcz
			weatherIcon.setImageDrawable(getResources().getDrawable(
					R.drawable.big_rain));
		else if (weatherType == 1)
			weatherIcon.setImageDrawable(getResources().getDrawable(
					R.drawable.sun));
		else if (weatherType == 2)
			weatherIcon.setImageDrawable(getResources().getDrawable(
					R.drawable.sun_cloudy));
		else if (weatherType == 3)
			weatherIcon.setImageDrawable(getResources().getDrawable(
					R.drawable.clouds));

		return view;
	}
}