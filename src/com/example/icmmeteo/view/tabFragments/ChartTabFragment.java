package com.example.icmmeteo.view.tabFragments;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer.FillOutsideLine;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.icmmeteo.R;
import com.example.icmmeteo.cppNative.CppNative;
import com.example.icmmeteo.cppNative.FORECAST_TYPE;
import com.example.icmmeteo.cppNative.ForecastResult;

public class ChartTabFragment extends SherlockFragment {

	String title;
	int color;
	int fillColor;
	float[] values;
	Date[] dates;
	int[]  scale;

	// ArrayList<int> values;

	private LinearLayout chartLayout;
	private float[] markerValues;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		
		Bundle bundl = getArguments();
		values = bundl.getFloatArray("values");
		markerValues = bundl.getFloatArray("markerValues");
		dates = makeDates(bundl.getDoubleArray("timestamps"));
		title = bundl.getString("name");
		scale = bundl.getIntArray("scale");
		color = bundl.getInt("color");
		fillColor = bundl.getInt("fillColor");
		
		
		View view = inflater.inflate(R.layout.tab_chart, container, false);
		chartLayout = (LinearLayout) view.findViewById(R.id.chart_layout);
		
		List<Date[]> datesList = new ArrayList<Date[]>();
		List<double[]> valuesList = new ArrayList<double[]>();
		String[] names = {"",""};
		
		datesList.add(dates);
		datesList.add(dates);
		
		valuesList.add(floatToDoubleArray(values));
		valuesList.add(floatToDoubleArray(markerValues));
		
		XYMultipleSeriesDataset testDataset = buildDateDataset(names, datesList, valuesList);
		
		XYMultipleSeriesRenderer testRenderer = getRenderer();
		

		
//		Log.d("CHART_DEBUG", "Rozmiar danych: " + values.length + " " + dates.length);
		
//		Log.d("CHART_DEBUG", "testDates to null: " + (testDates == null));
//		Log.d("CHART_DEBUG", "testValues to null: " + (testValues == null));
		Log.d("CHART_DEBUG", "testDataset to null: " + (testDataset == null) + " liczba seriesow: " + testDataset.getSeriesCount());
		Log.d("CHART_DEBUG", "testRenderer to null: " + (testRenderer == null) + " liczba seriesow: " + testRenderer.getSeriesRendererCount());
		
//		testDataset.getSeriesCount()
//		testRenderer.getSeriesRendererCount()
		
		makeChart(testDataset, testRenderer );
		
		return view;
	}

	private XYMultipleSeriesRenderer getRenderer() {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(18);

		renderer.setLegendTextSize(15);
		renderer.setPointSize(2f);
		renderer.setShowGridX(true);
		renderer.setYLabelsAlign(Align.LEFT);
		renderer.setYLabelsPadding(-3);
		//renderer.setShowGridY(true);
		// renderer.setGridColor(getResources().getColor(R.color.txt));
		renderer.setMargins(new int[] { 20, 10, 0, 10 });
		renderer.setPanEnabled(true, false);
		renderer.setZoomButtonsVisible(false);
		//renderer.setMarginsColor(Color.WHITE);
		renderer.setMarginsColor(Color.parseColor("#eaeaea"));
		renderer.setPanEnabled(false, false); 

//		 renderer.setXAxisMax(cal.getTime());
//		 cal.add(Calendar.DATE, -ForexWiz.getVisibleRange());
//		 renderer.setXAxisMin(cal.getTime());
		
		 XYSeriesRenderer r = new XYSeriesRenderer();
		 
		 r.setColor(color);
		 r.setPointStyle(PointStyle.POINT);
		 r.setFillPoints(false);
		 r.setPointStrokeWidth(3);
		 r.setLineWidth(3);
		 
		 FillOutsideLine fill = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_ALL);
		 fill.setColor(fillColor);
		 r.addFillOutsideLine(fill);
		 
		 renderer.addSeriesRenderer(r);
		 
		 
		 XYSeriesRenderer marker = new XYSeriesRenderer();
		 
		 marker.setColor(Color.parseColor("#9f41c9"));
		 marker.setPointStyle(PointStyle.POINT);
		 marker.setFillPoints(false);
		 marker.setPointStrokeWidth(3);
		 marker.setLineWidth(3);
		 
//		 FillOutsideLine fillMarker = new FillOutsideLine(FillOutsideLine.Type.BOUNDS_ALL);
//		 fill.setColor(Color.parseColor("#9f41c9"));
//		 marker.addFillOutsideLine(fill);
		 
		 renderer.addSeriesRenderer(marker);
		 
		
		 renderer.setGridColor(Color.GRAY);
		 renderer.setShowLegend(false);
		 renderer.setAxesColor(Color.DKGRAY);
		 renderer.setXLabelsColor(Color.BLACK);
		 renderer.setYLabelsColor(0, Color.BLACK);
		 renderer.setLabelsColor(Color.BLACK);
		 renderer.setYAxisMin(scale[0]);
		 renderer.setYAxisMax(scale[1]);
		

		
		return renderer;
	}

	private class ChartTask extends AsyncTask<long[], Void, Date[]> { // takie
																		// tam
																		// na
																		// oko
																		// zeby
																		// bylo
																		// wiadomo
																		// co tu
																		// zrobic
		private TimeSeries series; // ma dawac timestampy i valuesy
		private int forecastType;

		protected void onPreExecute() {
			series = new TimeSeries("title");
			forecastType = FORECAST_TYPE.temperature;// com.example.icmmeteo.cppNative.
		}

		@Override
		protected Date[] doInBackground(long[]... params) {
			//ForecastResult fResults = CppNative.INSTANCE.getNewestWeatherForecastOnAndroid(forecastType);
			Date[] dates = null;
			float[] values = null;

			try {
				ForecastResult fResults = CppNative.INSTANCE.getNewestWeatherForecastOnAndroid(forecastType);
				dates = makeDates(fResults.timeStamps);
				values = fResults.values;

			} catch (Exception e) {
				Log.e("tag", e.getMessage(), e);
			}
			return dates;
		}

		@Override
		protected void onPostExecute(Date[] result) {

		}
	}

	/**
	 * Builds an XY multiple time dataset using the provided values.
	 * 
	 * @param titles
	 *            the series titles
	 * @param xValues
	 *            the values for the X axis
	 * @param yValues
	 *            the values for the Y axis
	 * @return the XY multiple time dataset
	 */
	protected XYMultipleSeriesDataset buildDateDataset(String[] titles,
			List<Date[]> xValues, List<double[]> yValues) {
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		int length = titles.length;
		for (int i = 0; i < length; i++) {
			TimeSeries series = new TimeSeries(titles[i]);
			Date[] xV = xValues.get(i);
			double[] yV = yValues.get(i);
			int seriesLength = xV.length;
			for (int k = 0; k < seriesLength; k++) {
				series.add(xV[k], yV[k]);
			}
			dataset.addSeries(series);
		}
		return dataset;
	}

	protected XYMultipleSeriesDataset buildDateDataset(String title,
			Date[] xValues, double[] yValues) {

		String[] titles = new String[1];
		titles[0] = title;

		List<Date[]> dates = new ArrayList<Date[]>();
		dates.add(xValues);

		List<double[]> values = new ArrayList<double[]>();
		values.add(yValues);

		return buildDateDataset(titles, dates, values);
	}

	private void makeChart(String title, Date[] dates, double[] values) {
		GraphicalView chartView = ChartFactory.getTimeChartView(getActivity(),
				buildDateDataset(title, dates, values), getRenderer(),
				"EEEHH:mm");
		chartLayout.addView(chartView);
	}
	
	private void makeChart(XYMultipleSeriesDataset dataset, XYMultipleSeriesRenderer renderer) {
		GraphicalView chartView = ChartFactory.getTimeChartView(getActivity(),
				dataset,renderer,
				"EEEHH:mm");
		chartLayout.addView(chartView);
	}

	private Date[] makeDates(long[] timestamps) {
		Date[] dates = new Date[timestamps.length];

		for (int i = 0; i < timestamps.length; i++) {
			dates[i] = new Date(timestamps[i]);
		}

		return dates;
	}
	
	private Date[] makeDates(double[] timestampsSeconds) {
		Date[] dates = new Date[timestampsSeconds.length];

		for (int i = 0; i < timestampsSeconds.length; i++) {
			dates[i] = new Date((long)(timestampsSeconds[i] * 1000));
		}

		return dates;
	}
	
	
	private Date[]  makeTestDates(){
		Date[] dates = new Date[10];
		dates[0] = new Date();
		for (int i = 1 ; i < dates.length ; i++){
			dates[i] = new Date(dates[0].getTime() + (3600000*i));
		}
		return dates;
	}
	
	private double[] makeTestValues(){
		double[] values = new double[10];
		values[0] = 20;
		for (int i = 1 ; i < values.length ; i++){
			values[i] = values[0] + ( Math.random() % 20 );
		}
		
		return values;
	}
	

//	/**
//	 * Executes the chart demo.
//	 * 
//	 * @param context
//	 *            the context
//	 * @return the built intent
//	 */
//	public Intent execute(Context context) {
//		String[] titles = new String[] { "New tickets", "Fixed tickets" };
//		List<Date[]> dates = new ArrayList<Date[]>();
//		List<double[]> values = new ArrayList<double[]>();
//		int length = titles.length;
//		for (int i = 0; i < length; i++) {
//			dates.add(new Date[12]);
//			dates.get(i)[0] = new Date(108, 9, 1);
//			dates.get(i)[1] = new Date(108, 9, 8);
//			dates.get(i)[2] = new Date(108, 9, 15);
//			dates.get(i)[3] = new Date(108, 9, 22);
//			dates.get(i)[4] = new Date(108, 9, 29);
//			dates.get(i)[5] = new Date(108, 10, 5);
//			dates.get(i)[6] = new Date(108, 10, 12);
//			dates.get(i)[7] = new Date(108, 10, 19);
//			dates.get(i)[8] = new Date(108, 10, 26);
//			dates.get(i)[9] = new Date(108, 11, 3);
//			dates.get(i)[10] = new Date(108, 11, 10);
//			dates.get(i)[11] = new Date(108, 11, 17);
//		}
//		values.add(new double[] { 142, 123, 142, 152, 149, 122, 110, 120, 125,
//				155, 146, 150 });
//		values.add(new double[] { 102, 90, 112, 105, 125, 112, 125, 112, 105,
//				115, 116, 135 });
//		length = values.get(0).length;
//		int[] colors = new int[] { Color.BLUE, Color.GREEN };
//		PointStyle[] styles = new PointStyle[] { PointStyle.POINT,
//				PointStyle.POINT };
//		XYMultipleSeriesRenderer renderer = null;// buildRenderer(colors,
//													// styles);
//		// setChartSettings(renderer, "Project work status", "Date", "Tickets",
//		// dates.get(0)[0].getTime(),
//		// dates.get(0)[11].getTime(), 50, 190, Color.GRAY, Color.LTGRAY);
//		renderer.setXLabels(0);
//		renderer.setYLabels(10);
//		renderer.addYTextLabel(100, "test");
//		length = renderer.getSeriesRendererCount();
//		for (int i = 0; i < length; i++) {
//			SimpleSeriesRenderer seriesRenderer = renderer
//					.getSeriesRendererAt(i);
//			seriesRenderer.setDisplayChartValues(true);
//		}
//		renderer.setXRoundedLabels(false);
//		return ChartFactory
//				.getTimeChartIntent(context,
//						buildDateDataset(titles, dates, values), renderer,
//						"MM/dd/yyyy");
//	}
	
 private double[] floatToDoubleArray(float[] input){
	 double[] doubleArray = new double[input.length];
	 for (int i = 0 ; i < input.length; i++)
	 {
		 doubleArray[i] = (double) input[i];
	 }
	 
	 return doubleArray;
	 
 }



}