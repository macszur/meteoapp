package com.example.icmmeteo.logic;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;



public class ImageUtils {
	
	public static boolean storeForecastImage(Bitmap imageData, String filename) {
		//get path to external storage (SD card)
		String asciiFilename = filename.replaceAll("[^\\x00-\\x7F]", "");
		asciiFilename = asciiFilename.replace(' ', '_');
		String iconsStoragePath = Environment.getExternalStorageDirectory() + "/icmMeteo/forecasts/";
		File sdIconStorageDir = new File(iconsStoragePath);

		//create storage directories, if they don't exist
		sdIconStorageDir.mkdirs();

		try {
			String filePath = sdIconStorageDir.toString() + "/" + asciiFilename + ".frc";
			FileOutputStream fileOutputStream = new FileOutputStream(filePath);

			BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

			//choose another format if PNG doesn't suit you
			imageData.compress(CompressFormat.PNG, 100, bos);

			bos.flush();
			bos.close();

		} catch (FileNotFoundException e) {
			Log.w("TAG", "Error saving image file: " + e.getMessage());
			return false;
		} catch (IOException e) {
			Log.w("TAG", "Error saving image file: " + e.getMessage());
			return false;
		}

		return true;
	}
	
	public static Bitmap loadForecastImage(String filename){
		String asciiFilename = filename.replaceAll("[^\\x00-\\x7F]", "");
		asciiFilename = asciiFilename.replace(' ', '_');
		String imagePath = Environment.getExternalStorageDirectory() + "/icmMeteo/forecasts/" + asciiFilename + ".frc";
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
		return bitmap;
	}


}
