package com.example.icmmeteo.logic.manageXml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import android.content.Context;

import com.example.icmmeteo.model.CityItem;

public class XMLManager {

	private final Context context;
	private Document doc;
	private Element root;
	private final String FILENAME = "citiesList2.xml";

	public XMLManager(final Context context_) {
		context = context_;
	}

	public void test() {

		addCity("Warszawa", 5, 5);
		addCity("Legionowo", 7, 8);

	}

	public void newXml() {

		root = new Element("citiesList");
		doc = new Document(root);


	}

	public void saveXml() {

		boolean success = true;
		final XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		try {
			// FileOutputStream fos = context.openFileOutput(FILENAME,
			// context.MODE_APPEND );
			final FileOutputStream fos = context.openFileOutput(FILENAME,
					Context.MODE_PRIVATE);
			xmlOutput.output(doc, fos);
			fos.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success = false;
		}
		if (success == true)
			System.out
					.println("WYGENEROWANO PLIK XML! NIE MUSI PAN TEGO ROBIC RECZNIE! ");
	}

	@Deprecated
	public void writeCity(final String cityName, final int x, final int y) {
		final Element city = new Element("city");
		city.setAttribute("name", cityName);
		city.setAttribute("x", String.valueOf(x));
		city.setAttribute("y", String.valueOf(y));
		root.addContent(city);
	}

	public void writeCity(CityItem cityItem) {
		final Element city = new Element("city");
		city.setAttribute("name", cityItem.getName());
		city.setAttribute("province", cityItem.getPowiat());
		city.setAttribute("x", String.valueOf(cityItem.getX()));
		city.setAttribute("y", String.valueOf(cityItem.getY()));
		city.setAttribute("ID", String.valueOf(cityItem.getCityID()));
		root.addContent(city);
	}

	public void printXml() {

		// reading can be done using any of the two 'DOM' or 'SAX' parser
		// we have used saxBuilder object here
		// please note that this saxBuilder is not internal sax from jdk
		final SAXBuilder saxBuilder = new SAXBuilder();

		// obtain file object

		try {

			final FileInputStream fis = context.openFileInput(FILENAME);
			// converted file to document object
			final Document document = saxBuilder.build(fis);

			// get root node from xml
			final Element rootNode = document.getRootElement();

			// got all xml elements into a list
			final List<Element> studentList = rootNode.getChildren("city");

			// simple iteration to see the result on console
			for (int i = 0; i <= studentList.size() - 1; i++) {
				final Element element = studentList.get(i);
				System.out.println("Nazwa : "
						+ element.getAttributeValue("name"));
				System.out.println("X : " + element.getAttributeValue("x"));
				System.out.println("Y : " + element.getAttributeValue("y"));
			}

		} catch (final JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadXml() {
		// reading can be done using any of the two 'DOM' or 'SAX' parser
		// we have used saxBuilder object here
		// please note that this saxBuilder is not internal sax from jdk
		final SAXBuilder saxBuilder = new SAXBuilder();

		// obtain file object

		try {

			final FileInputStream fis = context.openFileInput(FILENAME);
			// converted file to document object
			doc = saxBuilder.build(fis);

			// get root node from xml
			root = doc.getRootElement();

		} catch (final JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Deprecated
	public void addCity(final String cityName, final int x, final int y) {

		if (!ifXMLExists())
			newXml();
		else
			loadXml();

		boolean exists = false;
		final List<Element> citiesList = root.getChildren("city");

		// simple iteration to see the result on console
		for (int i = 0; i <= citiesList.size() - 1; i++) {
			if (citiesList.get(i).getAttributeValue("name").equals(cityName)) {
				exists = true;
				break;
			}
		}

		if (!exists) {
			writeCity(cityName, x, y);
			saveXml();
		}

	}

	public void addCity(CityItem city) {

		if (!ifXMLExists())
			newXml();
		else
			loadXml();

		boolean exists = false;
		final List<Element> citiesList = root.getChildren("city");

		// simple iteration to see the result on console
		for (int i = 0; i <= citiesList.size() - 1; i++) {
			if (citiesList.get(i).getAttributeValue("ID")
					.equals(String.valueOf(city.getCityID()))) {
				exists = true;
				break;
			}
		}

		if (!exists) {
			writeCity(city);
			saveXml();
		}

	}

	public boolean ifXMLExists() {
		final File file = context.getFileStreamPath(FILENAME);
		return file.exists();

	}

	// TODO
	// MARIAN ZDEBUGUJ TO PLOX
	@Deprecated
	public boolean removeCity(final String cityname) {

		if (!ifXMLExists())
			return false;
		else
			loadXml();

		boolean removed = false;
		List<Element> citiesList = new ArrayList<Element>(
				root.getChildren("city"));

		// simple iteration to see the result on console
		for (int i = 0; i <= citiesList.size() - 1; i++) {
			// System.out.println(citiesList.get(i).getAttributeValue("name") +
			// " " +
			// citiesList.get(i).getAttributeValue("name").equals(cityname));
			if (citiesList.get(i).getAttributeValue("name").equals(cityname)) {
				citiesList.remove(i);
				removed = true;
				break;
			}
		}

		if (removed) {

			root.removeContent();
			root.setContent(citiesList);
			saveXml();
		}

		return removed;

	}

	public boolean removeCity(final int cityID) {

		if (!ifXMLExists())
			return false;
		else
			loadXml();

		boolean removed = false;
		List<Element> citiesList = new ArrayList<Element>(
				root.getChildren("city"));

		// simple iteration to see the result on console
		for (int i = 0; i <= citiesList.size() - 1; i++) {
			// System.out.println(citiesList.get(i).getAttributeValue("name") +
			// " " +
			// citiesList.get(i).getAttributeValue("name").equals(cityname));
			if (citiesList.get(i).getAttributeValue("ID")
					.equals(String.valueOf(cityID))) {
				citiesList.remove(i);
				removed = true;
				break;
			}
		}

		if (removed) {

			root.removeContent();
			root.setContent(citiesList);
			saveXml();
		}

		return removed;

	}

	public CityItem[] createCityItemsList() {

		if (!ifXMLExists())
			return new CityItem[0];
		else
			loadXml();

		ArrayList<CityItem> cityItemsList = new ArrayList<CityItem>();
		List<Element> citiesList = root.getChildren("city");
		Element city;
		CityItem cityItem;

		// simple iteration to see the result on console
		for (int i = 0; i <= citiesList.size() - 1; i++) {
			city = citiesList.get(i);
			cityItem = new CityItem();
			cityItem.setName(city.getAttributeValue("name"));
			cityItem.setPowiat(city.getAttributeValue("province"));
			cityItem.setCityID(Integer.parseInt(city.getAttributeValue("ID")));
			cityItem.setX(Integer.parseInt(city.getAttributeValue("x")));
			cityItem.setY(Integer.parseInt(city.getAttributeValue("y")));
			cityItemsList.add(cityItem);
		}

		CityItem[] array = cityItemsList.toArray(new CityItem[cityItemsList
				.size()]);

		return array;

	}

	public void getCitiesList() {
		final List<Element> citiesList = root.getChildren("city");

		// simple iteration to see the result on console
		for (int i = 0; i <= citiesList.size() - 1; i++) {

		}
	}
	
	
}