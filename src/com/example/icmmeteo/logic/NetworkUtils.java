package com.example.icmmeteo.logic;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkUtils {
	
	public static boolean isNetworkAvalible(Context context)
	{
		Log.d("NETWORK_UTILS", "isNetworkAvailable()");
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetworkInfo;

			activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

		if (activeNetworkInfo == null)
			return false;
		if (!activeNetworkInfo.isAvailable())
			return false;
		if (!activeNetworkInfo.isConnected())
			return false;
		return true;
	}




}
