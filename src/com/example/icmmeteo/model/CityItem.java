package com.example.icmmeteo.model;

import com.example.icmmeteo.cppNative.Location;

public class CityItem {

	private String name;
	private int x;
	private int y;
	private int cityID;
	private String powiat;
	

	public CityItem()
	{
		x = 0;
		y = 0;
		cityID = 0;
		name = "";
		powiat = "";
	}
	
	public CityItem(String name_, int x_, int y_){
		setName(name_);
		setX(x_);
		setY(y_);
	}
	
	public CityItem(Location loc){
		setName(String.valueOf(loc.name));
		setX(loc.X);
		setY(loc.Y);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public String toString(){
		return name;
		
	}
	
	public String getPowiat() {
		return powiat;
	}

	public void setPowiat(String powiat) {
		this.powiat = powiat;
	}
	
	public int getCityID() {
		return cityID;
	}

	public void setCityID(int cityID) {
		this.cityID = cityID;
	}
	
}
