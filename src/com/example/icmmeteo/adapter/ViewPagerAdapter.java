package com.example.icmmeteo.adapter;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.icmmeteo.cppNative.Forecasts;
import com.example.icmmeteo.view.tabFragments.ChartTabFragment;
import com.example.icmmeteo.view.tabFragments.CommentTabFragment;
import com.example.icmmeteo.view.tabFragments.GeneralTabFragment;
import com.example.icmmeteo.view.tabFragments.ImageTabFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

	
	// Declare the number of ViewPager pages

	private String titles[] = new String[] { "Podsumowanie", "Temperatura", "Opad", "Ci�nienie", "Wiatr", "Zachmurzenie", "Komentarz synoptyka", "Wykresy"   };
	final int PAGE_COUNT = titles.length;
	ChartTabFragment temperatureChartFragment;
	ChartTabFragment pressureChartFragment;
	ChartTabFragment windChartFragment;
	GeneralTabFragment generalFragment ;
	ChartTabFragment rainChartFragment;
	ChartTabFragment cloudsChartFragment;
	ImageTabFragment imageTabFragment;
	

	public ViewPagerAdapter(FragmentManager fm, Forecasts forecasts, String forecastName, int x, int y) {
		super(fm);
		
		generalFragment = new GeneralTabFragment();		
		Bundle generalBundl = new Bundle();
		generalBundl.putSerializable("forecasts", forecasts);
		generalBundl.putString("forecastName", forecastName);
		generalFragment.setArguments(generalBundl);
		
		temperatureChartFragment =  new ChartTabFragment();		
		Bundle tempBundl = new Bundle();
		tempBundl.putString("name", "Temperatura");				
		tempBundl.putFloatArray("values", forecasts.getTemperature());
		tempBundl.putFloatArray("markerValues", forecasts.getMarkerTable());
		tempBundl.putDoubleArray("timestamps", forecasts.getTimestamps());
		tempBundl.putIntArray("scale", forecasts.getTempScale());
		tempBundl.putInt("color", Color.rgb(255, 0, 0));
		tempBundl.putInt("fillColor", Color.rgb(250, 220, 220));
		temperatureChartFragment.setArguments(tempBundl);
		
		rainChartFragment =  new ChartTabFragment();
		Bundle rainBundl = new Bundle();
		rainBundl.putString("name", "Opad");				
		rainBundl.putFloatArray("values", forecasts.getRain()); 
		rainBundl.putFloatArray("markerValues", forecasts.getMarkerTable());
		rainBundl.putDoubleArray("timestamps", forecasts.getTimestamps());
		rainBundl.putInt("color", Color.rgb(0, 156, 0));
		rainBundl.putInt("fillColor", Color.rgb(0, 156, 0));
		rainBundl.putIntArray("scale", forecasts.getRainScale());
		rainChartFragment.setArguments(rainBundl);
		
		pressureChartFragment =  new ChartTabFragment();		
		Bundle pressBundl = new Bundle();
		pressBundl.putString("name", "Ci�nienie");				
		pressBundl.putFloatArray("values", forecasts.getPressure()); 
		pressBundl.putFloatArray("markerValues", forecasts.getMarkerTable());
		pressBundl.putDoubleArray("timestamps", forecasts.getTimestamps());		
		pressBundl.putIntArray("scale", forecasts.getPressScale());
		pressBundl.putInt("color", Color.rgb(106, 90, 205));
		pressBundl.putInt("fillColor", Color.rgb(185, 220, 255));
		pressureChartFragment.setArguments(pressBundl);
		
		windChartFragment =  new ChartTabFragment();		
		Bundle windBundl = new Bundle();
		windBundl.putString("name", "Wiatr");				
		windBundl.putFloatArray("values", forecasts.getWind()); 
		windBundl.putFloatArray("markerValues", forecasts.getMarkerTable());
		windBundl.putDoubleArray("timestamps", forecasts.getTimestamps());	
		windBundl.putIntArray("scale", forecasts.getWindScale());
		windBundl.putInt("color", Color.rgb(255, 150, 0));
		windBundl.putInt("fillColor", Color.rgb(255, 200, 0));
		windChartFragment.setArguments(windBundl);
		
		cloudsChartFragment =  new ChartTabFragment();		
		Bundle cloudsBundl = new Bundle();
		cloudsBundl.putString("name", "Zachmurzenie");				
		cloudsBundl.putFloatArray("values", forecasts.getCloud()); 
		cloudsBundl.putFloatArray("markerValues", forecasts.getMarkerTable());
		cloudsBundl.putDoubleArray("timestamps", forecasts.getTimestamps());	
		cloudsBundl.putIntArray("scale", forecasts.getCloudScale());
		cloudsBundl.putInt("color", Color.rgb(90, 90, 90));
		cloudsBundl.putInt("fillColor", Color.rgb(150, 150, 150));
		cloudsChartFragment.setArguments(cloudsBundl);
		
		imageTabFragment  = new ImageTabFragment();
		Bundle imgBundl = new Bundle();
		imgBundl.putString("name", forecastName);	
		imgBundl.putInt("x", x);	
		imgBundl.putInt("y", y);	
		imageTabFragment.setArguments(imgBundl);
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		
		// tutaj se przekazemy to przez bundla

		// Open FragmentTab1.java
		case 0:		
			return generalFragment;

			// Open FragmentTab2.java
		case 1:
			return temperatureChartFragment;
			
		case 2:
			return rainChartFragment;
			
		case 3:
			return pressureChartFragment;
			
		case 4:		
			return windChartFragment;
			
		case 5:		
			return cloudsChartFragment;
			
		case 6:
			CommentTabFragment fragmenttab4 = new CommentTabFragment();
			return fragmenttab4;
			
		case 7:
			return imageTabFragment;
		}
		return null;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return titles[position];
	}

	@Override
	public int getCount() {
		return PAGE_COUNT;
	}

}