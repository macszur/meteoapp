package com.example.icmmeteo.adapter;
import java.util.ArrayList;
import java.util.Arrays;

import com.example.icmmeteo.R;
import com.example.icmmeteo.model.CityItem;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class CityListAdapter extends ArrayAdapter<CityItem> implements Filterable {

	// declaring our ArrayList of items
	private CityItem[] objects;
	private CityItem[] filteredObjects;
	int textViewResourceId;
	private ItemFilter mFilter = new ItemFilter();
	
	public void setObjects(CityItem[] objects) {
		this.objects = objects;
	}
 
	public int getCount() {
		return filteredObjects.length;
	}
 
	public CityItem getItem(int position) {
		return filteredObjects[position];
	}
 
	public long getItemId(int position) {
		return position;
	}

	/* here we must override the constructor for ArrayAdapter
	* the only variable we care about now is ArrayList<Item> objects,
	* because it is the list of objects we want to display.
	*/
	public CityListAdapter(Context context, int textViewResourceId, CityItem[] objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.filteredObjects = objects;
		this.textViewResourceId = textViewResourceId;
	}

	/*
	 * we are overriding the getView method here - this is what defines how each
	 * list item will look.
	 */
	public View getView(int position, View convertView, ViewGroup parent){

		// assign the view we are converting to a local variable
		View v = convertView;

		// first check to see if the view is null. if so, we have to inflate it.
		// to inflate it basically means to render, or show, the view.
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(textViewResourceId, null);
		}

		/*
		 * Recall that the variable position is sent in as an argument to this method.
		 * The variable simply refers to the position of the current object in the list. (The ArrayAdapter
		 * iterates through the list we sent it)
		 * 
		 * Therefore, i refers to the current Item object.
		 */
		CityItem i = filteredObjects[position];

		if (i != null) {

			// This is how you obtain a reference to the TextViews.
			// These TextViews are created in the XML files we defined.

			TextView name = (TextView) v.findViewById(R.id.text1);
			TextView province = (TextView) v.findViewById(R.id.text2);
	

			// check to see if each individual textview is null.
			// if not, assign some text!
			if (name != null){
				name.setText(i.getName());
			}
			if (province != null){
				province.setText(i.getPowiat());
			}
	
		}

		// the view must be returned to our activity
		return v;

	}
	
	 static class ViewHolder {
	        TextView text;
	    }
	 
		public Filter getFilter() {
			return mFilter;
		}
	
	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			
//			if (constraint.equals("")){
//				final ArrayList<CityItem> nlist =
//				new ArrayList<CityItem>(Arrays.asList(objects));
//				
//				FilterResults results = new FilterResults();
//				
//				results.values = nlist;
//				results.count = nlist.size();
//	 
//				return results;		
//			}
			
			
			String filterString = constraint.toString().toLowerCase();
			
			Log.d("ICM_METEO", "Probuje filtrowac za pomoca: " + filterString);
			
			FilterResults results = new FilterResults();
			
			final CityItem[] list = objects;
 
			int count = list.length;
			final ArrayList<CityItem> nlist = new ArrayList<CityItem>(count);
 
			String filterableString ;
			
			for (int i = 0; i < count; i++) {
				filterableString = list[i].getName();
//				if (filterableString.toLowerCase().contains(filterString)) {
//					nlist.add(list[i]);
//				}
				if (filterableString.toLowerCase().startsWith(filterString)) {
					nlist.add(list[i]);
				}
			}
			
			results.values = nlist;
			results.count = nlist.size();
 
			return results;
		}
 
		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			ArrayList<CityItem> filteredData = (ArrayList<CityItem>) results.values;
			filteredObjects = filteredData.toArray(new CityItem[filteredData
			                                      				.size()]);
			notifyDataSetChanged();
		}
	}
	

}