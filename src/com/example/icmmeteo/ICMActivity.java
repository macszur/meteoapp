package com.example.icmmeteo;

import com.example.icmmeteo.view.AddForecastFragment;
import com.example.icmmeteo.view.mainFragments.BuyFragment;
import com.example.icmmeteo.view.mainFragments.CreditsFragment;
import com.example.icmmeteo.view.mainFragments.HelpFragment;
import com.example.icmmeteo.view.mainFragments.HomeFragment;
import com.example.icmmeteo.view.mainFragments.MoreAppsFragment;
import com.example.icmmeteo.view.mainFragments.WidgetsFragment;

public interface ICMActivity {
	public HomeFragment getHomeFragment();

	public HomeFragment getHomeF();

	public BuyFragment getBuyF();

	public CreditsFragment getCreditsF();

	public HelpFragment getHelpF();

	public AddForecastFragment getAddForecastF();

	public WidgetsFragment getWidgetsF();

	public MoreAppsFragment getMoreAppsF();
	
	public void setMenuSelection(int position);

}
