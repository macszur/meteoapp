package com.example.icmmeteo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;

public abstract class ICMFragmentClass extends SherlockFragment {

	String NAME = null;
	static String TITLE = null;
	int position;

	public  String getNAME() {
		return NAME;
	}

	public  void setNAME(String nAME) {
		NAME = nAME;
	}

	public static String getTITLE() {
		return TITLE;
	}

	public static void setTITLE(String tITLE) {
		TITLE = tITLE;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position_) {
		position = position_;
	}

	
	public void manageLayout() {

		getActivity().setTitle(TITLE);

		((ICMActivity) getActivity()).setMenuSelection(position);

	}

}
