package com.example.icmmeteo.widget;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.AdapterView;

import com.example.icmmeteo.R;
import com.example.icmmeteo.model.CityItem;
import com.example.icmmeteo.view.AddForecastFragment;
import com.example.icmmeteo.widget.small.WidgetHomeFragmentSmall;

public class WidgetAddForecastFragment extends AddForecastFragment {

	@Override
	public void onCityItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		CityItem item = (CityItem) parent.getAdapter().getItem(position);

		Bundle bundl = new Bundle();
		bundl.putString("name", item.getName());
		bundl.putString("province", item.getPowiat());
		bundl.putInt("x", item.getX());
		bundl.putInt("y", item.getY());

		// WidgetActivity activity = (WidgetActivity) getActivity();
		//
		// Fragment fragment = (Fragment) activity.getHomeFragment();

		Fragment fragment = (Fragment) new WidgetHomeFragmentSmall();

		fragment.setArguments(bundl);

		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.popBackStack();
		fragmentManager.beginTransaction()
				.replace(R.id.frame_container, fragment).commit();

	}

}
