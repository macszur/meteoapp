package com.example.icmmeteo.widget.small;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.example.icmmeteo.ICMFragmentClass;
import com.example.icmmeteo.R;
import com.example.icmmeteo.view.mainFragments.HomeAddForecastFragment;
import com.example.icmmeteo.view.mainFragments.HomeFragment;
import com.example.icmmeteo.widget.WidgetActivity;
import com.example.icmmeteo.widget.WidgetAddForecastFragment;
import com.example.icmmeteo.widget.WidgetHomeFragment;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class WidgetConfigSmall extends SherlockFragmentActivity implements
		WidgetActivity {

	Button configOkButton;
	FragmentManager fragmentManager = getSupportFragmentManager();
	WidgetAddForecastFragment addForecastF = new WidgetAddForecastFragment();
	WidgetHomeFragmentSmall homeF;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_widget_config);

		Bundle bundl = new Bundle();
		bundl.putString("name", "Wybierz miasto...");
		bundl.putString("province", "");
		bundl.putInt("x", -1);
		bundl.putInt("y", -1);

		homeF = new WidgetHomeFragmentSmall();

		homeF.setArguments(bundl);

		fragmentManager.beginTransaction().replace(R.id.frame_container, homeF)
				.addToBackStack(null).commit();

	}

	@Override
	public WidgetHomeFragment getHomeFragment() {
		// TODO Auto-generated method stub
		return (WidgetHomeFragment) homeF;
	}

}