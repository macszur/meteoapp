package com.example.icmmeteo.widget.small;

import android.appwidget.AppWidgetProvider;

import java.util.Random;
import java.util.concurrent.ExecutionException;

import com.example.icmmeteo.R;
import com.example.icmmeteo.adapter.ViewPagerAdapter;
import com.example.icmmeteo.cppNative.CppNative;
import com.example.icmmeteo.cppNative.FORECAST_TYPE;
import com.example.icmmeteo.cppNative.ForecastResult;
import com.example.icmmeteo.cppNative.Forecasts;
import com.example.icmmeteo.model.CityItem;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class SmallWidgetProvider extends AppWidgetProvider {

	private static final String ACTION_CLICK = "ACTION_CLICK";
	Forecasts forecasts;
	private int weatherType;

	@Override
	public void onReceive(Context context, Intent intent) {
		

		super.onReceive(context, intent);

	};

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		// Get all ids
		ComponentName thisWidget = new ComponentName(context,
				SmallWidgetProvider.class);
		int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
		//taka tablica forecastow ile jest widgetidsow
		for (int widgetId : allWidgetIds) {
			// create some random data
			int[] xy = getWidgetData(context, widgetId);
			int x = xy[0];
			int y = xy[1];
			
			if(x!=-1 && y!=-1){
			AsyncTask<Integer, Void, Forecasts> task = new DownloadForecastsTask().execute(x,y);
			
			try {
				forecasts = task.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int number = (int)forecasts.getCurrentTemperature();
			
			
			
			

			RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
					R.layout.widget_small_layout);
			Log.d("WIDGET_ICM", "prognoza to:  " + number);
			
			// Set the text
			remoteViews.setTextViewText(R.id.temperatureText,
					String.valueOf(number)+"�");
			
			// i obrazek
			weatherType = forecasts.getCurrentWeather();
			if (weatherType == 0) // deszcz
				remoteViews.setImageViewResource(R.id.weatherIcon, R.drawable.big_rain);
			else if (weatherType == 1)
				remoteViews.setImageViewResource(R.id.weatherIcon, R.drawable.sun);
			else if (weatherType == 2)
				remoteViews.setImageViewResource(R.id.weatherIcon, R.drawable.sun_cloudy);
			else if (weatherType == 3)
				remoteViews.setImageViewResource(R.id.weatherIcon, R.drawable.clouds);
				
			

			// Register an onClickListener
			Intent intent = new Intent(context, SmallWidgetProvider.class);

			intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);

			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.temperatureText,
					pendingIntent);
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
			}
		}
	}
	
//    public int[] getWidgetData(Context context, int widgetID) {
//        SharedPreferences myPrefs = context.getSharedPreferences("Data",
//                Activity.MODE_MULTI_PROCESS);
//        int[] xy = {myPrefs.getInt(file_name+"x", -1), myPrefs.getInt(file_name+"y", -1)};       
//        return xy;
//    }
    
    public int[] getWidgetData(Context context, int widgetId) {
        SharedPreferences myPrefs = context.getSharedPreferences("Data",
                Activity.MODE_MULTI_PROCESS);
        int[] xy = {myPrefs.getInt("Widget"+widgetId+"x", -1), myPrefs.getInt("Widget"+widgetId+"y", -1)};       
        return xy;
    }
    
    private class DownloadForecastsTask extends AsyncTask<Integer, Void, Forecasts> {
    	/**
    	 * The system calls this to perform work in a worker thread and delivers
    	 * it the parameters given to AsyncTask.execute()
    	 */
    	protected Forecasts doInBackground(Integer... coordinates) {
    		
    		CppNative nat = CppNative.INSTANCE;
    		
    		Forecasts result = new Forecasts();
    		CityItem location = new CityItem();
    		location.setX(coordinates[0]);
    	    location.setY(coordinates[1]);

    		nat.loadNewestWeatherForecast(location);
    		
    		Log.i("ICMMeteo", "zaraz przetworze najnowsza pogode z nativa");
    		ForecastResult fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.temperature);
    		result.setTemperature(fr.values);
//    		result.setTempScale(fr.scale);
//    		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.wind);
//    		result.setWind(fr.values);
//    		result.setWindScale(fr.scale);
//    		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.pressure);
//    		result.setPressure(fr.values);
//    		result.setPressScale(fr.scale);
//    		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.rain);
//    		result.setRain(fr.values);
//    		result.setRainScale(fr.scale);
    		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.rain);
    		result.setRain(fr.values);
    		result.setRainScale(fr.scale);
    		fr = nat.getNewestWeatherForecastOnAndroid(FORECAST_TYPE.clouds);
    		result.setCloud(fr.values);
    		result.setCloudScale(fr.scale);	
    		result.setTimestamps(fr.timeStamps);
    		
    		return result;
    	}
    	
    	/**
    	 * The system calls this to perform work in the UI thread and delivers
    	 * the result from doInBackground()
    	 */
    	protected void onPostExecute(Forecasts result) {
//    		
//    		forecasts = result;
//    		currentView.findViewById(R.id.progressBar1).setVisibility(View.INVISIBLE);
    	}
    }

}