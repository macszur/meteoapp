package com.example.icmmeteo.widget.small;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.example.icmmeteo.R;
import com.example.icmmeteo.adapter.CityListAdapter;
import com.example.icmmeteo.model.CityItem;
import com.example.icmmeteo.widget.WidgetAddForecastFragment;
import com.example.icmmeteo.widget.WidgetHomeFragment;

public class WidgetHomeFragmentSmall extends SherlockFragment implements
		WidgetHomeFragment {

	ArrayAdapter<CityItem> adapter;
	private ListView listview;
	int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
	private Button configOkButton;
	private CityItem city;
	private int thisWidgetId;
	public static String ACTION_WIDGET_CONFIGURE = "WIDGET_CONFIGURED";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		getIdOfCurrentWidget(savedInstanceState);
		
		Activity activity = getActivity();
		activity.setResult(Activity.RESULT_CANCELED);

		View rootView = inflater.inflate(
				R.layout.fragment_main_widget_config_small, container, false);
		listview = (ListView) rootView.findViewById(R.id.forecastListview);

		Bundle bundl = getArguments();
		String cityName = bundl.getString("name");
		String provinceName = bundl.getString("province");
		int x = bundl.getInt("x");
		int y = bundl.getInt("y");

		city = new CityItem();
		city.setName(cityName);
		city.setPowiat(provinceName);
		city.setX(x);
		city.setY(y);

		CityItem[] list = new CityItem[1];
		list[0] = city;

		adapter = new CityListAdapter(getActivity().getApplicationContext(),
				R.layout.city_list_item_2, list);

		listview.setAdapter(adapter);


		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				changeFragment(new WidgetAddForecastFragment(),
						"Wybierz miasto");

			}
		});

		configOkButton = (Button) rootView.findViewById(R.id.okconfig);
		configOkButton.setOnClickListener(new Button.OnClickListener() {

			@Override
            public void onClick(View arg0) {
                
                if (city.getX()!=-1 && city.getY()!=-1) {	// tutaj sprawdzic czy wybano miasto itp
                	updateWidget();
                    saveToPreferences(thisWidgetId, city.getX(), city.getY());	//tu mozna sobie zapisac dane,
                    													// aby z kazdego miejca, np
                    setResultDataToWidget(Activity.RESULT_OK);			//updatu widgeta miec
                } else{
            		Toast.makeText(getActivity().getApplicationContext(),
            				"Wybierz miasto!", Toast.LENGTH_LONG).show();
                    //setResultDataToWidget(Activity.RESULT_CANCELED);
            }
		}});


		return rootView;
	}


	private void changeFragment(SherlockFragment fragment, String title) {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.frame_container, fragment).addToBackStack(null)
				.commit();
		getActivity().setTitle(title);
	}
	
    void getIdOfCurrentWidget(Bundle savedInstanceState) {
    	 
    	getActivity().setResult(Activity.RESULT_CANCELED);
 
        Bundle extras = getActivity().getIntent().getExtras();
 
        if (extras != null) {
            thisWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            if (getWidgetData(thisWidgetId) != null) {
//                save.setText("Update");
//                ed.append(getWidgetData("Widget" + thisWidgetId));
                
            }
 
//            widgetId.setText("Widget ID = " + thisWidgetId);
        }
 
        // If they gave us an intent without the widget id, just bail.
        if (thisWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
        	getActivity().finish();
        }
 
    }
    
    /**
     * Update the Current Widget - This is very important to ensure the widget
     * is enabled
     **/
    void updateWidget() {
    	Activity activity = getActivity();
    	
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(activity);
       
        RemoteViews remoteViews = new RemoteViews(activity.getPackageName(),
                R.layout.widget_small_layout);
        Intent clickIntent = activity.getIntent();
        
        clickIntent.setAction(ACTION_WIDGET_CONFIGURE);
        remoteViews.setTextViewText(R.id.cityName, city.getName());
        clickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, thisWidgetId);
        PendingIntent pendingIntent = PendingIntent.getActivity(activity,
                thisWidgetId, clickIntent, 0);
        remoteViews.setOnClickPendingIntent(R.id.widgetRoot, pendingIntent);
        // update this widget
        appWidgetManager.updateAppWidget(thisWidgetId, remoteViews);
    }
 
    void setResultDataToWidget(int result) {
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, thisWidgetId);
        getActivity().setResult(result, resultValue);
        getActivity().finish();
    }
 
    public void saveToPreferences(int widgetId, int x, int y) {
        SharedPreferences myPrefs = getActivity().getSharedPreferences("Data",
        		Activity.MODE_MULTI_PROCESS);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
       // prefsEditor.putString(file_name, data);
        prefsEditor.putInt("Widget"+widgetId+"x", x);
        prefsEditor.putInt("Widget"+widgetId+"y", y);
        prefsEditor.commit();
    }
 
    public int[] getWidgetData(int widgetId) {
        SharedPreferences myPrefs = getActivity().getSharedPreferences("Data",
                Activity.MODE_MULTI_PROCESS);
        int[] xy = {myPrefs.getInt("Widget"+widgetId+"x", -1), myPrefs.getInt("Widget"+widgetId+"y", -1)};       
        return xy;
    }

}
