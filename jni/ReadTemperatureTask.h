//
//  ReadTemperatureTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadTemperatureTask__
#define __meteoAppModel__ReadTemperatureTask__

#include <iostream>
#include <future>
#include "globalDefs.h"
#include "ITask.h"
#include "ReadTemperatureTaskAns.h"
#include "structures.h"
#include "patto.h"

class ReadTemperatureTask : public ITask
{
    //std::thread workerT;
    ReadTemperatureTaskAns taskAns;
    const Image_t * dataPtr;
    

    range temperature_range;

    patto patternGen;
    imgmatrix imgMatx;
    float temperature_with_column(int column);
public:
    ReadTemperatureTask()
    {
        type = std::launch::async;
        temperature_range.loc = 58;
        temperature_range.len = 76;
        
    }
    ~ReadTemperatureTask() = default;
    virtual void setProcessingData(const Image_t * _processData)
    {
        dataPtr = _processData;
        imgMatx.setMatrix(dataPtr);
        patternGen.png = &imgMatx;
    }
    virtual void operator()();
    virtual void getRetValue(ITaskAnsJoiner * joiner);
    
    
    
    //void setCurrImgType(UM_IMAGE_DATE_T type);
};

#endif /* defined(__meteoAppModel__ReadTemperatureTask__) */
