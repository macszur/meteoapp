//
//  imgmatrix.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__imgmatrix__
#define __meteoAppModel__imgmatrix__

#include <iostream>
#include "globalDefs.h"

class imgmatrix {
    //int x, y;
    
//    png_structp png_ptr;
//    png_infop info_ptr;
    
    //unsigned char * row_pointers;
    
    const Image_t * imgBuff;
    
public:
    imgmatrix() = default;
    ~imgmatrix() = default;
	void setMatrix(const Image_t * _imgBuff)
    {
        imgBuff = _imgBuff;
    }
    
    const Image_t * getMatrix()
    {
        return imgBuff;
    }
    
    int red_pixel(int _x, int _y);
    int green_pixel(int _x, int _y);
    int blue_pixel(int _x, int _y);
    
    inline bool rgbEqualToRgb(int r, int g, int b, int er, int eg, int eb)
    {
        if(r == er && g == eg && b == eb)
            return true;
        return false;
    }
    
    inline bool rgbEqualToVal(int r, int g, int b, int val)
    {
        if(r == val && g == val && b == val)
            return true;
        return false;
    }
    
    inline bool rgbNotEqualTo(int r, int g, int b, int val)
    {
        if(r == val && g == val && b == val)
            return false;
        return true;
    }
};

#endif /* defined(__meteoAppModel__imgmatrix__) */
