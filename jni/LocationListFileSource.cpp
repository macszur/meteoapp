//
//  LocationListFileSource.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 27.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "LocationListFileSource.h"

#include <cstdlib>
#include <string>
//#include <android/log.h>

LocationListFileSource::location_hash_set_t LocationListFileSource::getAllLocationsFromFile()
{
    
    bool succes = false;
    location_hash_set_t ret;
    auto handFunc = [](std::ifstream &file, location_hash_set_t & vect)
    {
    	//__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Jestem w handFunc !!!!!! \n");
        bool ret = false;
        location tmpLoc;
        std::string tmp;
        std::string tmp2;
        //std::string name2;
        while (std::getline(file, tmp))
        {
        	//__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Pobralem linijke ! %s !!!!!! \n", tmp.c_str());
            std::size_t pos = tmp.find('\"');
            std::size_t pos2 = tmp.find('\"', pos + 1);
            tmp2 = tmp.substr(pos+1, pos2 - pos - 1);
            //std::copy(tmp2.begin(), tmp2.end(), tmpLoc.name);
            strcpy(tmpLoc.name, tmp2.c_str());
            //            tmpLoc.name = tmp2.c_str();
            
            
            std::size_t pos3 = tmp.find('\"', pos2 + 1);
            std::size_t pos4 = pos2;
            
            if(pos3 != std::string::npos)
            {
                pos4 = tmp.find('\"', pos3 + 1);
                tmp2 = tmp.substr(pos3+1, pos4 - pos3 - 1);
                //std::copy(tmp2.begin(), tmp2.end(), tmpLoc.powiat);
                strcpy(tmpLoc.powiat, tmp2.c_str());
            }
            
            
            std::size_t num1pos1 = tmp.find(',', pos4 + 1);
            std::size_t num1pos2 = tmp.find(',', num1pos1 + 1);
            tmpLoc.locID = atoi(tmp.substr(num1pos1+1, num1pos2 - num1pos1 - 1).c_str());
            
            std::size_t num2pos1 = num1pos2;//tmp.find(',', num1pos2 + 1);
            std::size_t num2pos2 = tmp.find(',', num2pos1 + 1);
            tmpLoc.X = atoi(tmp.substr(num2pos1+1, num2pos2 - num2pos1 - 1).c_str());
            
            std::size_t num3pos1 = num2pos2; //tmp.find(',', num2pos2 + 1);
            std::size_t num3pos2 = tmp.find(')', num3pos1 + 1);
            tmpLoc.Y = atoi(tmp.substr(num3pos1+1, num3pos2 - num3pos1 - 1).c_str());
            
            //vect.insert(tmpLoc);
            vect.push_back(tmpLoc);
        }
        if(vect.size() > 0)
            ret = true;
        
        
        return ret;
    };
    
    
    locationsFile.setNewPath(LOCATION_LIST_PATH);
    locationsFile.setNewFileName(LOCATION_LIST_FILENAME);
    locationsFile.setSpecialLoadHandler(handFunc);
    
    succes = locationsFile.readWholeFile(ret);
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Przeczytalem caly plik z sukcesem %d !!!!!! \n", (int)succes);
    if(succes)
        return ret;
    else
        return location_hash_set_t();
}
