//
//  ReadXScale.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadXScaleTask.h"
#include "Log.h"

void ReadXScaleTask::operator()()
{
    taskAns.setTestVal(123.123);
    
  
    // skonstruowac timestamp od zaczynajacego start_hour i start_tm
    // wpisac do std::vector<timestamp> 412 pozycji z kolejnymi ts'ami
    // ten vector bedzie w taskAns
    if(ftype == FORECAST_TYPE::temperature)
    {
        first_line(temperature_scale_frame, taskAns.tempScale.maxVal, taskAns.tempScale.maxY);
        last_line(temperature_scale_frame, taskAns.tempScale.minVal, taskAns.tempScale.minY);
        Log("zakres temp od :" + std::to_string(taskAns.tempScale.maxVal) + " do : " + std::to_string(taskAns.tempScale.minVal));
    }
    else if(ftype == FORECAST_TYPE::wind)
    {
        first_line(wind_scale_frame, taskAns.windScale.maxVal, taskAns.windScale.maxY);
        last_line(wind_scale_frame, taskAns.windScale.minVal, taskAns.windScale.minY);
        Log("zakres windu od :" + std::to_string(taskAns.windScale.maxVal) + " do : " + std::to_string(taskAns.windScale.minVal));
    }
    else if(ftype == FORECAST_TYPE::pressure)
    {
        first_line(pressure_scale_frame, taskAns.pressureScale.maxVal, taskAns.pressureScale.maxY);
        last_line(pressure_scale_frame, taskAns.pressureScale.minVal, taskAns.pressureScale.minY);
        Log("zakres pressu od :" + std::to_string(taskAns.pressureScale.maxVal) + " do : " + std::to_string(taskAns.pressureScale.minVal));
    }
    else if(ftype == FORECAST_TYPE::rain)
    {
        first_line(rain_scale_frame, taskAns.rainScale.maxVal, taskAns.rainScale.maxY);
        last_line(rain_scale_frame, taskAns.rainScale.minVal, taskAns.rainScale.minY);
        Log("zakres rainu od :" + std::to_string(taskAns.rainScale.maxVal) + " do : " + std::to_string(taskAns.rainScale.minVal));
    }
    else if(ftype == FORECAST_TYPE::clouds)
    {
        first_line(cloud_scale_frame, taskAns.cloudScale.maxVal, taskAns.cloudScale.maxY);
        last_line(cloud_scale_frame, taskAns.cloudScale.minVal, taskAns.cloudScale.minY);
        Log("zakres cloudu od :" + std::to_string(taskAns.cloudScale.maxVal) + " do : " + std::to_string(taskAns.cloudScale.minVal));
    }
}

void ReadXScaleTask::getRetValue(ITaskAnsJoiner * joiner)
{
    taskAns.Accept(joiner);
    //return 0;
}

void ReadXScaleTask::first_line(frame _frame, int &v, int &r){
    int line;
    int counter;
    char *buffer;
    
    line = -1;
    counter = 0;
    buffer = new char[5];
    memset(buffer, 0, 5);
	
	unsigned long long int pattern = 0;
    // first line
    for (int x = _frame.x; x < _frame.x + _frame.height; x++) for (int y = _frame.y; y < _frame.y + _frame.width; y++)
	{
		frame _patframe = {x,y,5,8};
		pattern = patternGen.pattern_for_frame(_patframe);
        //		pattern = pattern_generator -> pattern_for_point_and_size(x, y, 5, 8);
		for (int i=0; i<PATTERNS_COUNT; i++) if (patterns[i] == pattern) {
            if (line != x && line != -1) break;
            buffer[counter++] = chars[i];
            line = x;
            break;
        }
	}
	buffer[counter] = '\0';
	
    v = atoi(buffer);
    r = line + 4;
    delete [] buffer;
}

void ReadXScaleTask::last_line(frame _frame, int &v, int &r){
    int line;
    int counter;
    char *buffer;
    
    line = -1;
    counter = 0;
    buffer = new char[5];
	
	unsigned long long int pattern = 0;
    // first line
    for (int x = _frame.x + _frame.height - 1; x >= _frame.x; x--) for (int y = _frame.y; y < _frame.y + _frame.width; y++)
	{
		frame _patframe = {x,y,5,8};
		pattern = patternGen.pattern_for_frame(_patframe);
		//pattern = pattern_generator -> pattern_for_point_and_size(x, y, 5, 8);
		for (int i=0; i<PATTERNS_COUNT; i++) if (patterns[i] == pattern) {
            if (line != x && line != -1) break;
            buffer[counter++] = chars[i];
            line = x;
            break;
        }
	}
	buffer[counter] = '\0';
	
    v = atoi(buffer);
    r = line + 4;
}