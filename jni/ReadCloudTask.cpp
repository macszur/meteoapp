//
//  ReadCloudTask.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 10.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadCloudTask.h"
#include "Log.h"

void ReadCloudTask::operator()()
{
    taskAns.setTestVal(123.123);
    int startColumn = FORECAST_STARTING_COLUMN;
    for(int i = 0; i < FORECAST_WIDTH; ++i)
    {
        taskAns.addImVal(cloud_with_column(startColumn + i));
    }
    
    
    //Log("zakres od :" + std::to_string(taskAns.firstLineVal) + " do : " + std::to_string(taskAns.lastLineVal));
    
    
}

void ReadCloudTask::getRetValue(ITaskAnsJoiner * joiner)
{
    taskAns.Accept(joiner);
    //return 0;
}



//bool rgbNotEqualTo(int r, int g, int b, int val)
//{
//    if(r == val && g == val && b == val)
//        return false;
//    return true;
//}

float ReadCloudTask::cloud_with_column(int column)
{
    range _range = cloud_range;
    float immidiateCloud = 0;
    //static float lastVal = 0;
    //bool found = false;
    // dwie kolejne 255
    
    for(int i = _range.loc; i < _range.loc + _range.len; ++i)
    {
        int r = patternGen.png->red_pixel(column, i);
        int g = patternGen.png->green_pixel(column, i);
        int b = patternGen.png->blue_pixel(column, i);
        
        if(patternGen.png->rgbEqualToVal(r, g, b, 150) || patternGen.png->rgbEqualToVal(r, g, b, 255) || patternGen.png->rgbEqualToVal(r, g, b, 90))
        {
            immidiateCloud = i;
            //lastVal = i;
            //found = true;
            break;
        }
        else if(patternGen.png->rgbEqualToRgb(r, g, b, 255, 180, 0))
        {
            float lastValTmp = cloud_with_column(column - 1);
            immidiateCloud = lastValTmp;
            
        }
    }
    
    //    if(found == false)
    //    {
    //        immidiateRain = i;
    //
    //    }
    
    
    return immidiateCloud;
}