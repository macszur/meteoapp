//
//  ReadWindTaskAns.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 03.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadWindTaskAns.h"

void ReadWindTaskAns::Accept(ITaskAnsJoiner * joiner)
{
    if(joiner != nullptr)
        joiner->joinTaskAns(this);
}