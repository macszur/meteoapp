//
//  DataContainer.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 28.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "DataContainer.h"

class DataContainer::DataContainerImpl
{
    LocationListFileSource llfs;
    LocationListFileSource::location_hash_set_t locations;
    
    std::unique_ptr<IForecastImageSource> ifS;
    
    Image_t imgBuff;
    std::shared_ptr<const Image_t> imgBuffPtr;
    
    pngToRawImage conv;
    
public:
    DataContainerImpl()
    {
        ifS = std::unique_ptr<IForecastImageSource>(new ForecastImageNetSource());
//        imgBuff.data = new char [50 * 1024];
//        imgBuff.size = 50*1024;
    }
    size_t loadLocationListFromFile()
    {
        locations = llfs.getAllLocationsFromFile();
        if(locations.size() > 0)
            return locations.size();
        else return -1;
    }
    
    LocationListFileSource::location_hash_set_t getLocationListFromFile()
    {
        if(locations.size() < 1)
            loadLocationListFromFile();
        return locations;
    }
    void setNewLocationListSrc(LocationListFileSource &newSrc)
    {
        // NOT IMPLEMENTED YET
        //llfs = newSrc;
    }
    
    bool loadCurrentForecastImg(const location &loc)
    {
        size_t ret = ifS->loadImage(loc, UM_IMAGE_DATE_T::CURRENT);
        if(ret > 0)
        {
            conv.makeRaw(ifS->getImageData(), ret, imgBuff);
            return true;
        }
        //conv.makePng(ifS->getImageData(tmpSize))
        return false;
    }
    
    const Image_t& getCurrentForecastImg()
    {
        return imgBuff;
        
    }
    
//    std::shared_ptr<const Image_t> getCurrentForecastImgPtr()
//    {
//        imgBuffPtr = std::shared_ptr<const Image_t>(&imgBuff);
//        return imgBuffPtr;
//    }
    
};

DataContainer::DataContainer() : impl(new DataContainerImpl)
{
    
}

DataContainer::~DataContainer()// : impl(new DataContainerImpl)
{
    
}

bool DataContainer::loadCurrentForecastImg(const location &l)
{
    return impl->loadCurrentForecastImg(l);
}

const Image_t& DataContainer::getCurrentForecastImg()
{
    return impl->getCurrentForecastImg();
}

//std::shared_ptr<const Image_t> DataContainer::getCurrentForecastImgPtr()
//{
//    return impl->getCurrentForecastImgPtr();
//}

void DataContainer::setExternalForecastSource(std::unique_ptr<IForecastImageSource> imgSrc)
{
    
}

size_t DataContainer::loadLocationListFromFile()
{
    return impl->loadLocationListFromFile();
}

LocationListFileSource::location_hash_set_t DataContainer::getLocationListFromFile()
{
    return impl->getLocationListFromFile();
}



void DataContainer::setLocationListSource(LocationListFileSource &newSrc)
{
    impl->setNewLocationListSrc(newSrc);
}