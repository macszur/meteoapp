//
//  ReadCloudTaskAns.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 10.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadCloudTaskAns.h"

void ReadCloudTaskAns::Accept(ITaskAnsJoiner * joiner)
{
    if(joiner != nullptr)
        joiner->joinTaskAns(this);
}