//
//  ForecastImageNetSource.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 14.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ForecastImageNetSource.h"


ForecastImageNetSource::ForecastImageNetSource()
{

}

ForecastImageNetSource::~ForecastImageNetSource()
{
    
}

const buff_pt ForecastImageNetSource::getImageData()//unsigned long & count)
{
    //netS.
    return netS.getImageData();
}

size_t ForecastImageNetSource::loadImage(const location &l, UM_IMAGE_DATE_T t)
{
    lb.setCol(l.X);
    lb.setRow(l.Y);
    netS.init(lb.getProperImageLink(t));
    
    return netS.downloadFromUrl();
}