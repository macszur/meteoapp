//
//  NullConnector.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "NullConnector.h"

NullConnector::NullConnector()
{
    
}

NullConnector::~NullConnector()
{
    
}

bool NullConnector::initialize()
{
    return false;
}

bool NullConnector::loadNewestWeatherForecast(location where)
{
    return false;
}

const Image_t& NullConnector::getLoadedForecastImage()
{
    return Image_t();
}

forecastResult NullConnector::getNewestWeatherForecast(FORECAST_TYPE type)
{
    return forecastResult();
}
forecastResult NullConnector::newestWeatherForecast(GPSpoint where)
{
    return forecastResult();
}

currentForecastResult NullConnector::getCurrentWeather(location where)
{
    return currentForecastResult();
}
currentForecastResult NullConnector::getCurrentWeather(GPSpoint where)
{
    return currentForecastResult();
}

void NullConnector::workOnLocalImage(Image_t * img, bool onoff)
{
    
}

int NullConnector::loadDefaultLocationList(int &count)
{
    return 0;
}

std::vector<location> NullConnector::getDefaultLocationList()
{
    return std::vector<location>();
}
location NullConnector::getXYfromGPS(GPSpoint pkt)
{
    return location();
}

