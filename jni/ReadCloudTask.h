//
//  ReadCloudTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 10.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadCloudTask__
#define __meteoAppModel__ReadCloudTask__

#include <iostream>
#include <future>
#include "globalDefs.h"
#include "ITask.h"
#include "ReadCloudTaskAns.h"
#include "structures.h"
#include "patto.h"

class ReadCloudTask : public ITask
{
    //std::thread workerT;
    ReadCloudTaskAns taskAns;
    const Image_t * dataPtr;
    
    
    range cloud_range;
    
    patto patternGen;
    imgmatrix imgMatx;
    float cloud_with_column(int column);
public:
    ReadCloudTask()
    {
        type = std::launch::async;
        cloud_range.loc = 524;
        cloud_range.len = 76;
        
    }
    ~ReadCloudTask() = default;
    virtual void setProcessingData(const Image_t * _processData)
    {
        dataPtr = _processData;
        imgMatx.setMatrix(dataPtr);
        patternGen.png = &imgMatx;
    }
    virtual void operator()();
    virtual void getRetValue(ITaskAnsJoiner * joiner);
    
    
    
    //void setCurrImgType(UM_IMAGE_DATE_T type);
};


#endif /* defined(__meteoAppModel__ReadCloudTask__) */
