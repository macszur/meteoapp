//
//  Log.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 09.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__Log__
#define __meteoAppModel__Log__

#include <iostream>
#include <string>
#include "globalDefs.h"

#ifdef m__And
#include <android/log.h>
#endif

#define LOG_TAG "ICM_METEO_LIB"

void Log(std::string str);

#endif /* defined(__meteoAppModel__Log__) */
