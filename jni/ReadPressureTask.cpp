//
//  ReadPressureTask.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 07.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadPressureTask.h"
#include "Log.h"

void ReadPressureTask::operator()()
{
    taskAns.setTestVal(123.123);
    int startColumn = FORECAST_STARTING_COLUMN;
    for(int i = 0; i < FORECAST_WIDTH; ++i)
    {
        taskAns.addImVal(pressure_with_column(startColumn + i));
    }
    
    
    //Log("zakres od :" + std::to_string(taskAns.firstLineVal) + " do : " + std::to_string(taskAns.lastLineVal));
    
    
}

void ReadPressureTask::getRetValue(ITaskAnsJoiner * joiner)
{
    taskAns.Accept(joiner);
    //return 0;
}

float ReadPressureTask::pressure_with_column(int column)
{
    range _range = pressure_range;
    float immidiatePressu = 0;
    bool found = false;
    // dwie kolejne 255
    
    for(int i = _range.loc; i < _range.loc + _range.len; ++i)
    {
        int r = patternGen.png->red_pixel(column, i);
        int g = patternGen.png->green_pixel(column, i);
        int b = patternGen.png->blue_pixel(column, i);
        if(patternGen.png->rgbNotEqualTo(r, g, b, 206) && patternGen.png->rgbNotEqualTo(r, g, b, 226) && patternGen.png->rgbNotEqualTo(r, g, b, 234)
           && patternGen.png->rgbNotEqualTo(r, g, b, 0) && patternGen.png->rgbNotEqualTo(r, g, b, 255) && patternGen.png->rgbNotEqualTo(r, g, b, 20)
           && patternGen.png->rgbNotEqualTo(r, g, b, 224))
        {
            immidiatePressu = i;
            found = true;
            break;
        }
    }
    if(found == false)
    {
        immidiatePressu = pressure_with_column(column - 1);
        int test = 1;
        int b = test * 50;
        int c = b;
    }
    
    
    return immidiatePressu;
}
