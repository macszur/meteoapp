//
//  ITaskAnsJoiner.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ITaskAnsJoiner__
#define __meteoAppModel__ITaskAnsJoiner__

#include <iostream>

#include "ITaskAns.h"

class ReadAndPrepareTimeTaskAns;
class ReadXScaleTaskAns;
class ReadTemperatureTaskAns;
class ReadWindTaskAns;
class ReadPressureTaskAns;
class ReadRainTaskAns;
class ReadCloudTaskAns;
class TaskAnsB;

class ITaskAnsJoiner
{
public:
    //virtual void joinTaskAns(ITaskAns * task) = 0;
    virtual void joinTaskAns(ReadAndPrepareTimeTaskAns * task) = 0;
    virtual void joinTaskAns(ReadXScaleTaskAns * task) = 0;
    virtual void joinTaskAns(ReadTemperatureTaskAns * task) = 0;
    virtual void joinTaskAns(ReadWindTaskAns * task) = 0;
    virtual void joinTaskAns(ReadPressureTaskAns * task) = 0;
    virtual void joinTaskAns(ReadRainTaskAns * task) = 0;
    virtual void joinTaskAns(ReadCloudTaskAns * task) = 0;
    virtual void joinTaskAns(TaskAnsB * task) = 0;

};

#endif /* defined(__meteoAppModel__ITaskAnsJoiner__) */
