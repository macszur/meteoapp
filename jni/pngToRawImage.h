//
//  pngToRawImage.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 17.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__pngToRawImage__
#define __meteoAppModel__pngToRawImage__

#include <iostream>
#include <memory>
#include "globalDefs.h"
#include "lodepng.h"
#include "Log.h"

class pngToRawImage {
    
    
public:
    bool makeRaw(buff_pt pngToConvert, size_t pngSize, Image_t& imageBuff);
};

#endif /* defined(__meteoAppModel__pngToRawImage__) */
