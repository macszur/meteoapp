//
//  Log.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 09.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "Log.h"



void Log(std::string str)
{
#ifdef m__x86
    std::cout<<LOG_TAG<<": "<<str<<std::endl;
#elif m__iOS
    std::cout<<LOG_TAG<<": "<<str<<std::endl;
#elif m__And
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "\n %s \n", str.c_str());
#endif
    
}