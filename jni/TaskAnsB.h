//
//  TaskAnsB.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__TaskAnsB__
#define __meteoAppModel__TaskAnsB__

#include <iostream>
#include "ITaskAnsJoiner.h"

class TaskAnsB : public ITaskAns
{
public:
    
    virtual void Accept(ITaskAnsJoiner * joiner);
};

#endif /* defined(__meteoAppModel__TaskAnsB__) */
