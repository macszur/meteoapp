//
//  CommonConnector.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_CommonConnector_h
#define meteoAppModel_CommonConnector_h

#include "IConnector.h"
#include "globalDefs.h"
#include <vector>
#include <memory>

class CommonConnector : public IConnector
{
    class CommonConnectorImpl;
    std::unique_ptr<CommonConnectorImpl> impl;
public:
    CommonConnector();
    virtual ~CommonConnector();
    
    
    virtual bool initialize();
    
    virtual bool loadNewestWeatherForecast(location where);
    virtual const Image_t& getLoadedForecastImage();
    virtual forecastResult getNewestWeatherForecast(FORECAST_TYPE type);
    virtual forecastResult newestWeatherForecast(GPSpoint where);
    
    virtual currentForecastResult getCurrentWeather(location where);
    virtual currentForecastResult getCurrentWeather(GPSpoint where);
    
    virtual void workOnLocalImage(Image_t * img, bool onoff);
    
    virtual std::vector<location> getDefaultLocationList();
    virtual int loadDefaultLocationList(int &count);
    
    virtual location getXYfromGPS(GPSpoint pkt);
};

#endif
