//
//  NetSource.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "NetSource.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#ifdef m__And
#include <jni.h>
#include <android/log.h>
#include <sstream>

template<class T>
std::string to_string(const T& t) {
    std::ostringstream os;
    os << t;
    return os.str();
}
#endif



NetSource::~NetSource()
{
    if(imageBuff != nullptr)
    {
        imageBytesNum = 0;
        delete [] imageBuff;
        imageBuff = nullptr;
    }
    
    close(s_fd);
}

/***
 
 UWAGA: -
 
 ***/
bool NetSource::init(std::string _url, bool async)
{
    // rozbic url na hosta i getStr
    // pobrac adres ip serwera
    std::size_t pos1 = _url.find('/');
    host = _url.substr(0, pos1);
    getStr = _url.substr(pos1 + 1, std::string::npos);
    
    if(imageBuff != nullptr)
    {
        imageBytesNum = 0;
        delete [] imageBuff;
        imageBuff = nullptr;
    }
    
    //std::cout<<"Host : "<<host<<std::endl;
    //std::cout<<"getStr : "<<getStr<<std::endl;
    
    
    
    return true;
}


void NetSource::getDNSip()
{
    
}

// get sockaddr, IPv4 or IPv6:
void * NetSource::get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}



bool NetSource::connectToRemote()
{
    //socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    
    
    
    int rv;
    char s[INET6_ADDRSTRLEN];
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    
//    char msgBuff[255] = {0};
//    sprintf(msgBuff, "Host: %s | Port: %s | GetStr: %s", host.c_str(), to_string(port).c_str(), getStr.c_str());
//    Log(msgBuff);
    
#ifdef m__And
    if ((rv = getaddrinfo(host.c_str(), /*std::*/std::to_string(port).c_str(), &hints, &servinfo)) != 0)
#else
        if ((rv = getaddrinfo(host.c_str(), std::to_string(port).c_str(), &hints, &servinfo)) != 0)
#endif
        {
        	Log(std::string("getaddrinfo: ") + std::string(gai_strerror(rv) + std::string(" Host : ") + host + std::string(":") + std::to_string(port)));
        	freeaddrinfo(servinfo);
            //printf("getaddrinfo: %s\n", gai_strerror(rv));
            return false;
        }
    
    s_fd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if(s_fd == -1)
    {
        //std::cout<<"Blad socketa !"<<std::endl;
        Log(std::string("Blad socketa !"));
        freeaddrinfo(servinfo);
        return false;
    }
    
    struct timeval tv;
    
    tv.tv_sec = MAX_TIMEOUT_SEC;  /* timeout po ktorym przestaniemy odbierac */
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors
    
    setsockopt(s_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
    
    int conn = connect(s_fd, servinfo->ai_addr, servinfo->ai_addrlen);
    if(conn == -1)
    {
        //std::cout<<"Blad polaczenia !"<<std::endl;
        Log(std::string("Blad polaczenia !"));
        close(s_fd);
        freeaddrinfo(servinfo);
        return false;
    }
    
    
    inet_ntop(servinfo->ai_family, get_in_addr((struct sockaddr *)servinfo->ai_addr),
              s, sizeof s);
    //printf("client: connecting to %s\n", s);
    
    freeaddrinfo(servinfo); // all done with this structure
    
    
    
    //close(s_fd);
    
    return true;
}

void NetSource::buildHTTPRequest()
{
    char * buff = new char[1024];
    memset(buff, 0, 1024);
    sprintf(buff,
            "GET /%s HTTP/1.0\r\n"
            "Accept: */*\r\n"
            "User-Agent: macszur.icmmeteo\r\n"
            "Host: %s\r\n"
            "Connection: close\r\n"
            "\r\n",
            getStr.c_str(), host.c_str());
    request = buff;
    delete [] buff;
    
}

bool NetSource::sendHTTPRequest()
{
    //std::cout<<"REKLEST : \r\n"<<request;
    if(send(s_fd, request.c_str(), request.size() + 1, 0) == -1)
    {
        char msg[255] = {0};
        sprintf(msg, "Blad - %d ", errno);
        Log(std::string("Blad wysylania zapytania: ") + std::string(msg));
        perror(msg);
        Log(std::string("Blad wysylania zapytania: ") + std::string(strerror(errno)));
        close(s_fd);
        return false;
        
    }
    return true;
}

bool NetSource::getResponseHead()
{
    int size = 2048;
    std::unique_ptr<char[]> headBuff(new char[size]);
    memset(headBuff.get(), 0, size);
    int i;
    char *code;
    char *status;
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Poczatek to : %s !!!!!! \n", headBuff.get());
    
    Log(std::string("Poczatek to :") + std::string(headBuff.get()));
    
    for(i = 0; i < size - 1; i++)
    {
        if(recv(s_fd, headBuff.get() + i, 1, 0)!=1)
        {
            perror("blad odbierania");
            //Log(std::string("BREJKUJE PETLE ODBIERANIA NAGLOWKA !"));
            Log(std::string("BREJKUJE PETLE ODBIERANIA NAGLOWKA ! : ") + std::string(strerror(errno)));
            //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n BREJKUJE PETLE ODBIERANIA NAGLOWKA ! \n");
            return false;
        }
        if(strstr(headBuff.get(), "\r\n\r\n"))
            break;
    }
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n ODP to : %s !!!!!! \n", headBuff.get());
    Log(std::string("ODP to : ") + std::string(headBuff.get()));
    if(i >= size - 1)
    {
        puts("Http response head too long.");
        //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Http response head too long.");
        Log(std::string("Http response head too long."));
        return false;
    }
    code = strstr(headBuff.get()," 200 ");
    status = strstr(headBuff.get(),"\r\n");
    if(code != nullptr && status != nullptr)
    {
        //std::cout<<"KOD : "<<code<<std::endl;
        //std::cout<<"STATUS : "<<status<<std::endl;
    }
    //std::cout<<"ODP : \r\n"<<headBuff.get()<<std::endl;
    
    if(!code || code>status){
        *status = 0;
        printf("Bad http response:\"%s\"\n", headBuff.get());
        //        __android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n ZWRACAM fols, odpowiedz serwera to : %s !!!!!! \n", headBuff.get());
        Log(std::string("ZWRACAM fols, odpowiedz serwera to : ") + std::string(headBuff.get()));
        return false;
    }
    
    //    __android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n ZWRACAM TRU, odpowiedz serwera to : %s !!!!!! \n", headBuff.get());
    Log(std::string("ZWRACAM TRU, odpowiedz serwera to : ") + std::string(headBuff.get()));
    return true;
    
    //    std::size_t numbytes;
    //    if ((numbytes = recv(s_fd, buff, 4, 0)) == -1)
    //    {
    //        char msg[255] = {0};
    //        sprintf(msg, "Blad - %d ", errno);
    //        perror(msg);
    //        close(s_fd);
    //        return false;
    //    }
    //
    //    buff[numbytes] = '\0';
    //
    //    printf("client: received '%s'\n",buff);
    //
    //    return true;
}

bool NetSource::getResponseData()
{
    imageBuff = new buff_t[MAX_BUFF_SIZE];
    memset(imageBuff, 0, MAX_BUFF_SIZE);
    std::size_t numbytes;
    std::size_t totalBytes = 0;
    while((numbytes = recv(s_fd, imageBuff + totalBytes, MAX_BUFF_SIZE, 0)) > 0)
    {
        totalBytes += numbytes;
    }
    // = recv(s_fd, imageBuff, MAX_BUFF_SIZE, 0))
    if (numbytes == -1)
    {
        char msg[255] = {0};
        sprintf(msg, "Blad - %d ", errno);
        perror(msg);
        close(s_fd);
        return false;
    }
    imageBytesNum = totalBytes;
    imageBuff[imageBytesNum] = '\0';
    
    // printf("client: received %lu\n", imageBytesNum);
    
    return true;
}

//bool NetSource::removeHTTPChunksStuff()
//{
//
//    return true;
//}

size_t NetSource::downloadFromUrl()
{
    bool ret = true;
    ret = connectToRemote();
    if(ret)
    	buildHTTPRequest();
    if(ret)
    	ret = sendHTTPRequest();
    if(ret)
    	ret = getResponseHead();
    if(ret)
    	ret = getResponseData();
    //removeHTTPChunksStuff();
    char buff[255] = {0};
    sprintf(buff, "Ret w downloadFromUrl ma wartosc : %d", ret);
    Log(std::string(buff));
    if(ret)
        return imageBytesNum;
    return -1;
}

const buff_pt NetSource::getImageData()
{
    //count = imageBytesNum;
    return imageBuff;
}
