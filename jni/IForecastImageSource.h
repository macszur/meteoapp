//
//  IDataSource.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_IDataSource_h
#define meteoAppModel_IDataSource_h

#include "globalDefs.h"

class IForecastImageSource
{
public:
    virtual const buff_pt getImageData(/*unsigned long & count*/) = 0;
    virtual size_t loadImage(const location &l, UM_IMAGE_DATE_T t) = 0;
    //virtual size_t getImageSize() = 0;
    
    virtual ~IForecastImageSource()
    {
        
    }
};

#endif
