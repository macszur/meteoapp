//
//  ReadWindTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 03.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadWindTask__
#define __meteoAppModel__ReadWindTask__

#include <iostream>
#include <future>
#include "globalDefs.h"
#include "ITask.h"
#include "ReadWindTaskAns.h"
#include "structures.h"
#include "patto.h"

class ReadWindTask : public ITask
{
    //std::thread workerT;
    ReadWindTaskAns taskAns;
    const Image_t * dataPtr;
    
    
    range wind_range;
    
    patto patternGen;
    imgmatrix imgMatx;
    float wind_with_column(int column);
public:
    ReadWindTask()
    {
        type = std::launch::async;
        wind_range.loc = 316;
        wind_range.len = 76;
        
    }
    ~ReadWindTask() = default;
    virtual void setProcessingData(const Image_t * _processData)
    {
        dataPtr = _processData;
        imgMatx.setMatrix(dataPtr);
        patternGen.png = &imgMatx;
    }
    virtual void operator()();
    virtual void getRetValue(ITaskAnsJoiner * joiner);
    
    
    
    //void setCurrImgType(UM_IMAGE_DATE_T type);
};


#endif /* defined(__meteoAppModel__ReadWindTask__) */
