//
//  ReadAndPreparePTask.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadAndPrepareTimePTask.h"
#include "Log.h"

//
//ReadAndPrepareTimePTask::ReadAndPrepareTimePTask()
//{
//    
//}
//
//ReadAndPrepareTimePTask::ReadAndPrepareTimePTask()
//{
//    
//}

void ReadAndPrepareTimePTask::setProcessingData(const Image_t * _processData)
{
    
    //ParallelTaskRunner::run(_processData);
    //return 0;
}

void ReadAndPrepareTimePTask::operator()()
{
    taskAns.setTestVal(123.123);
    
    // skonstruowac timestamp od zaczynajacego start_hour i start_tm
    // wpisac do std::vector<timestamp> 412 pozycji z kolejnymi ts'ami
    // ten vector bedzie w taskAns
    
    start_tm.tm_hour = start_hour;
    start_tm.tm_min = 0;
    start_tm.tm_sec = 0;

    double currTs = mktime(&start_tm);
    //taskAns.addNewTs(currTs);
    double totalSecTime = FORECAST_HOUR_WIDTH * 3600;
    double onePxTime = totalSecTime / FORECAST_WIDTH;
    for(int i = 0; i < FORECAST_WIDTH; ++i)
    {
        taskAns.addNewTs(currTs);
        currTs += onePxTime;
        
    }
    Log("ostatni ts to :" + std::to_string(currTs));
    
    
}

void ReadAndPrepareTimePTask::getRetValue(ITaskAnsJoiner * joiner)
{
    taskAns.Accept(joiner);
    //return 0;
}

void ReadAndPrepareTimePTask::setCurrImgType(UM_IMAGE_DATE_T type)
{
    std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
    std::time_t yesterday_t = std::chrono::system_clock::to_time_t(today - std::chrono::hours(24));
    std::time_t today_t = std::chrono::system_clock::to_time_t(today);
    start_tm = *std::localtime(&today_t);
    tm yestPtm = *std::localtime(&yesterday_t);
    
    if(type == UM_IMAGE_DATE_T::CURRENT)
    {

        
        auto curr_hour = start_tm.tm_hour;
        start_hour = 0;
        
        if (curr_hour < 6)
        {
            start_hour = 21;
            start_tm = yestPtm;
        }
        else if (curr_hour < 12) start_hour = 3;
        else if (curr_hour < 18) start_hour = 9;
        else start_hour = 15;
    }
    else
    {
        // NOT IMPLEMENTED YET :(
    }
    Log("Start_hour to : " + std::to_string(start_hour));

}