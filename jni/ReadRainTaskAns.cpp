//
//  ReadRainTaskAns.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 07.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadRainTaskAns.h"

void ReadRainTaskAns::Accept(ITaskAnsJoiner * joiner)
{
    if(joiner != nullptr)
        joiner->joinTaskAns(this);
}