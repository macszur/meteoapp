//
//  LocationListFileSource.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 27.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_LocationListFileSource_h
#define meteoAppModel_LocationListFileSource_h

#include <vector>
#include <unordered_set>
#include <string>

#include "FileSource.h"
#include "globalDefs.h"

class LocationListFileSource
{
    class location_hash
    {
    public:
        std::size_t operator()(const location &pt) const
        {
            return std::hash<int>()(pt.X) ^ std::hash<int>()(pt.Y);
        }
    };
    
    class location_equal
    {
    public:
        bool operator()(const location &p1, const location &p2) const
        {
            return (p1.X == p2.X) && (p1.Y == p2.Y);
        }
    };
    
    
    
public:
    using location_hash_set_t = std::vector<location>; //std::unordered_set<location, location_hash, location_equal>;
private:
    
    FileSource<location_hash_set_t> locationsFile;
    
public:
    
    location_hash_set_t getAllLocationsFromFile();
};

#endif
