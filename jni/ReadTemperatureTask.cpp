//
//  ReadTemperatureTask.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadTemperatureTask.h"
#include "Log.h"

void ReadTemperatureTask::operator()()
{
    taskAns.setTestVal(123.123);
    int startColumn = FORECAST_STARTING_COLUMN;
    for(int i = 0; i < FORECAST_WIDTH; ++i)
    {
        taskAns.addImVal(temperature_with_column(startColumn + i));
    }
    
    
    //Log("zakres od :" + std::to_string(taskAns.firstLineVal) + " do : " + std::to_string(taskAns.lastLineVal));
    
    
}

void ReadTemperatureTask::getRetValue(ITaskAnsJoiner * joiner)
{
    taskAns.Accept(joiner);
    //return 0;
}

float ReadTemperatureTask::temperature_with_column(int column)
{
    range _range = temperature_range;
    float immidiateTemp = 0;
    bool found = false;
    // dwie kolejne 255
    
    for(int i = _range.loc; i < _range.loc + _range.len; ++i)
    {
        if(patternGen.png->red_pixel(column, i) == 255 && patternGen.png->green_pixel(column, i) == 0)
        {
            immidiateTemp = i;
            found = true;
            break;
        }
    }
    if(found == false)
    {
        immidiateTemp = temperature_with_column(column - 1);
    }
    
    return immidiateTemp;
}

