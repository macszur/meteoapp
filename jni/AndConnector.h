//
//  AndConnector.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_AndConnector_h
#define meteoAppModel_AndConnector_h

#include "IConnector.h"
#include "CommonConnector.h"
#include "Log.h"

#ifdef m__And
#include <jni.h>
#include <android/log.h>
#endif

class AndConnector : public IConnector
{
    std::unique_ptr<CommonConnector> commonPart;
public:
    AndConnector();
    virtual ~AndConnector();
    
    
    virtual bool initialize();
    
    virtual bool loadNewestWeatherForecast(location where);
    virtual const Image_t& getLoadedForecastImage();
    virtual forecastResult getNewestWeatherForecast(FORECAST_TYPE type);
    virtual forecastResult newestWeatherForecast(GPSpoint where);
    
    virtual currentForecastResult getCurrentWeather(location where);
    virtual currentForecastResult getCurrentWeather(GPSpoint where);
    
    virtual void workOnLocalImage(Image_t * img, bool onoff);
    
    virtual std::vector<location> getDefaultLocationList();
    virtual location getXYfromGPS(GPSpoint pkt);
    
    
    int loadDefaultLocationList(int &count);
#ifdef m__And
    bool loadNewestWeatherForecastOnAndroid(JNIEnv * env, jobject thiz, jobject loc);
    jobject getNewestWeatherForecastOnAndroid(JNIEnv * env, jobject thiz, int ftype);
    jbyteArray getLoadedForecastImageOnAndroid(JNIEnv * env, jobject thiz);
    void fillCityItemElement(JNIEnv * env, location & ls, jobject obj);
    int getDefaultLocationListOnAndroid(JNIEnv * env, jobject thiz, jobjectArray listObj);
#endif
};

#endif
