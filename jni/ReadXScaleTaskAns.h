//
//  ReadXScaleTaskAns.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadXScaleTaskAns__
#define __meteoAppModel__ReadXScaleTaskAns__

#include <iostream>
#include <vector>
#include "globalDefs.h"
#include "ITaskAnsJoiner.h"

class ReadXScaleTaskAns : public ITaskAns
{
    float val = -5;
    

public:
    ReadXScaleTaskAns() = default;
    scale_t tempScale = {0, 0, 0, 0};
    scale_t pressureScale = {0, 0, 0, 0};
    scale_t windScale = {0, 0, 0, 0};
    scale_t rainScale  = {0, 0, 0, 0};
    scale_t cloudScale  = {0, 0, 0, 0};

//    int firstLineY = 0;
//    int firstLineVal = 0;
//    int lastLineY = 0;
//    int lastLineVal = 0;
    
    void setTestVal(float _val)
    {
        val = _val;
    }
    float getTestVal()
    {
        return val;
    }
    virtual void Accept(ITaskAnsJoiner * joiner);
};

#endif /* defined(__meteoAppModel__ReadXScaleTaskAns__) */
