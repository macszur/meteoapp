//
//  CCreator.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 25.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "CCreator.h"
#include "x86Connector.h"
#include "iOSConnector.h"
#include "AndConnector.h"
#include "NullConnector.h"

#include "globalDefs.h"

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>
#include <cstring>

CCreator::CCreator()
{
#ifdef m__x86
    currentEnv = RUNNING_ENV::env_x86;
#elif m__iOS
    currentEnv = RUNNING_ENV::env_iOS;
#elif m__And
    currentEnv = RUNNING_ENV::env_And;
#else
    currentEnv = RUNNING_ENV::env_null;
#endif
}

RUNNING_ENV CCreator::getRunningEnv()
{
    return currentEnv;
}

std::unique_ptr<IConnector> CCreator::createProperFacade()
{
    std::unique_ptr<IConnector> newFacade;
    
    switch (currentEnv) {
        case RUNNING_ENV::env_x86:
            newFacade = std::unique_ptr<IConnector>(new x86Connector());
            break;
        case RUNNING_ENV::env_iOS:
            newFacade = std::unique_ptr<IConnector>(new iOSConnector());
            break;
        case RUNNING_ENV::env_And:
            newFacade = std::unique_ptr<IConnector>(new AndConnector());
            break;
            
            
        default:
            newFacade = std::unique_ptr<IConnector>(new NullConnector());
            break;
    }
    
    return newFacade;
}

//extern char * getcwd(char*, int);

location CCreator::andTest()
{
	location ret;
	strcpy(ret.powiat, "TO JEST TO ZIOOOM");
	std::cout<<"TO JEST AND TEST !, STWORZYMY PLIK I GO SE ODCZYTAMY env to : "<<(int)getRunningEnv()<<" !\r\n";
    //	std::ofstream plik("dupa.txt", std::ios::out);
    //	if(!plik.is_open())
    //	{
    //		std::cout<<"BAGRIATONOWSK !"<<std::endl;
    //		plik.close();
    //		return ret;
    //	}
    //	plik<<"PUPA\r\n";
    //	plik.close();
    
	char cwd[255] = {0};
	if ((getcwd(cwd, 64)) != NULL)
	{
		std::cout<<cwd<<std::endl;
        //free(cwd); /* free memory allocated by getcwd() */
	}
	strcpy(ret.name, cwd);
	strcpy(ret.powiat, "DDDDUPA");
	ret.X = 500;
	ret.Y = 123;
    
	return ret;
    
}
