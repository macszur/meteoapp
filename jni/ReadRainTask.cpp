//
//  ReadRainTask.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 07.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadRainTask.h"
#include "Log.h"

void ReadRainTask::operator()()
{
    taskAns.setTestVal(123.123);
    int startColumn = FORECAST_STARTING_COLUMN;
    for(int i = 0; i < FORECAST_WIDTH; ++i)
    {
        taskAns.addImVal(rain_with_column(startColumn + i));
    }
    
    
    //Log("zakres od :" + std::to_string(taskAns.firstLineVal) + " do : " + std::to_string(taskAns.lastLineVal));
    
    
}

void ReadRainTask::getRetValue(ITaskAnsJoiner * joiner)
{
    taskAns.Accept(joiner);
    //return 0;
}


//bool rgbNotEqualTo(int r, int g, int b, int val)
//{
//    if(r == val && g == val && b == val)
//        return false;
//    return true;
//}

float ReadRainTask::rain_with_column(int column)
{
    range _range = rain_range;
    float immidiateRain = 0;
    //bool found = false;
    // dwie kolejne 255
    
    for(int i = _range.loc; i < _range.loc + _range.len; ++i)
    {
        int r = patternGen.png->red_pixel(column, i);
        int g = patternGen.png->green_pixel(column, i);
        int b = patternGen.png->blue_pixel(column, i);
        
        if(patternGen.png->rgbEqualToRgb(r, g, b, 20, 176, 20) || patternGen.png->rgbEqualToRgb(r, g, b, 0, 156, 0))
        {
            immidiateRain = i;
            //found = true;
            break;
        }
    }
    
//    if(found == false)
//    {
//        immidiateRain = i;
//
//    }
    
    
    return immidiateRain;
}