//
//  ReadPressureTaskAns.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 07.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadPressureTaskAns__
#define __meteoAppModel__ReadPressureTaskAns__

#include <iostream>
#include <vector>
#include "globalDefs.h"
#include "ITaskAnsJoiner.h"


class ReadPressureTaskAns : public ITaskAns
{
    std::vector<float> intermidiateValues;
    float val = 0;
    
public:
    ReadPressureTaskAns()
    {
        intermidiateValues.reserve(FORECAST_WIDTH);
    }
    void addImVal(float && imVal)
    {
        intermidiateValues.push_back(imVal);
    }
    std::vector<float> & getIntermidiateValuesVector()
    {
        return intermidiateValues;
    }
    
    
    
    void setTestVal(float _val)
    {
        val = _val;
    }
    float getTestVal()
    {
        return val;
    }
    virtual void Accept(ITaskAnsJoiner * joiner);
};

#endif /* defined(__meteoAppModel__ReadPressureTaskAns__) */
