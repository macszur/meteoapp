//
//  TaskAnsA.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadAndPrepareTimeTaskAns.h"

void ReadAndPrepareTimeTaskAns::Accept(ITaskAnsJoiner * joiner)
{
    if(joiner != nullptr)
        joiner->joinTaskAns(this);
}