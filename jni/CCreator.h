//
//  CCreator.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 25.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_CCreator_h
#define meteoAppModel_CCreator_h

#include <cstdint>
#include <memory>
#include "globalDefs.h"
#include "IConnector.h"

enum class RUNNING_ENV : std::int8_t
{
    env_null = 0,
    env_x86 = 1,
    env_iOS = 2,
    env_And = 3
    
    };
    
    
    /*
     * Klasa sŇāuŇľńÖca do poprawnego stworzenia fasady do komunikacji z pozostaŇāńÖ czńôŇõcińÖ programu
     * jest to rodzaj fabryki abstrakcyjnej, tzn no powinnien byńá
     *
     *
     *
     *
     **/
    
    
    
    class CCreator
    {
        RUNNING_ENV currentEnv;
    public:
        RUNNING_ENV getRunningEnv();
        std::unique_ptr<IConnector> createProperFacade();
        
        CCreator();
        
        location andTest();
    };
    
    
    
#endif
