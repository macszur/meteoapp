//
//  structures.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __graphicPart__structures_h__
#define __graphicPart__structures_h__

struct frame {int x,y,width,height;};
struct range {int loc, len;};

#endif
