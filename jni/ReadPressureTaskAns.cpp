//
//  ReadPressureTaskAns.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 07.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadPressureTaskAns.h"

void ReadPressureTaskAns::Accept(ITaskAnsJoiner * joiner)
{
    if(joiner != nullptr)
        joiner->joinTaskAns(this);
}