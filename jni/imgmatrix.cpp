//
//  imgmatrix.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "imgmatrix.h"

//imgmatrix::imgmatrix(char* file_name){
//    char header[8];    // 8 is the maximum size that can be checked
//    
//    /* open file and test for it being a png */
//    FILE *file = fopen(file_name, "rb");
//    fread(header, 1, 8, file);
//    
//    /* initialize stuff */
//    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
//    info_ptr = png_create_info_struct(png_ptr);
//    
//    png_init_io(png_ptr, file);
//    png_set_sig_bytes(png_ptr, 8);
//    
//    png_read_info(png_ptr, info_ptr);
//    
//    width = png_get_image_width(png_ptr, info_ptr);
//    height = png_get_image_height(png_ptr, info_ptr);
//	
//    /* read file */
//    row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
//    for (y=0; y<height; y++) row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr,info_ptr));
//    
//    png_read_image(png_ptr, row_pointers);
//	
//    fclose(file);
//}

int imgmatrix::red_pixel(int _x, int _y){
    //png_byte* ptr = &(row_pointers[_y][_x*4]);

    int r = imgBuff->dataBuff[4 * _y * imgBuff->w + 4 * _x + 0];
    
    return r;
}

int imgmatrix::green_pixel(int _x, int _y){
    //png_byte* ptr = &(row_pointers[_y][_x*4]);
    
    int g =  imgBuff->dataBuff[4 * _y * imgBuff->w + 4 * _x + 1];
    
    return g;
}

int imgmatrix::blue_pixel(int _x, int _y){
    //png_byte* ptr = &(row_pointers[_y][_x*4]);

    int b =  imgBuff->dataBuff[4 * _y * imgBuff->w + 4 * _x + 2];

    return b;
}
