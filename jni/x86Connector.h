//
//  x86Connector.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_x86Connector_h
#define meteoAppModel_x86Connector_h

#include "IConnector.h"
#include "CommonConnector.h"

class x86Connector : public IConnector
{
    std::unique_ptr<CommonConnector> commonPart;
public:
    x86Connector();
    virtual ~x86Connector();
    
    virtual bool initialize();
    
    virtual bool loadNewestWeatherForecast(location where);
    virtual const Image_t& getLoadedForecastImage();
    virtual forecastResult getNewestWeatherForecast(FORECAST_TYPE type);
    virtual forecastResult newestWeatherForecast(GPSpoint where);
    
    virtual currentForecastResult getCurrentWeather(location where);
    virtual currentForecastResult getCurrentWeather(GPSpoint where);
    
    virtual void workOnLocalImage(Image_t * img, bool onoff);
    
    virtual int loadDefaultLocationList(int &count);
    virtual std::vector<location> getDefaultLocationList();
    virtual location getXYfromGPS(GPSpoint pkt);
};


#endif
