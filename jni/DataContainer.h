//
//  DataContainer.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 28.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__DataContainer__
#define __meteoAppModel__DataContainer__

#include <iostream>

#include "IForecastImageSource.h"
#include "LocationListFileSource.h"
#include "ForecastImageNetSource.h"
#include "pngToRawImage.h"
#include <memory>

class DataContainer
{
    class DataContainerImpl;
    std::unique_ptr<DataContainerImpl> impl;
    
    
    

public:
    DataContainer();
    virtual ~DataContainer();
    
    bool loadCurrentForecastImg(const location &l);
    const Image_t& getCurrentForecastImg();
    //std::shared_ptr<const Image_t> getCurrentForecastImgPtr();
    void setExternalForecastSource(std::unique_ptr<IForecastImageSource> imgSrc);
    
    size_t loadLocationListFromFile();
    LocationListFileSource::location_hash_set_t getLocationListFromFile();
    void setLocationListSource(LocationListFileSource &newSrc);
    
    
};

#endif /* defined(__meteoAppModel__DataContainer__) */
