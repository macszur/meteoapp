//
//  StdProcessor.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 30.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__StdProcessor__
#define __meteoAppModel__StdProcessor__

#include <iostream>

#include <memory>
#include <vector>
//#include "ITaskFun.h"
#include "IProcessor.h"
#include "StdTaskAnsJoiner.h"

class StdProcessor : public IProcessor
{

public:
    StdProcessor();
    ~StdProcessor();
    void addTaskToQueue(std::unique_ptr<ITask> pt);
    //void setProcessingData(const Image_t& data);
    void startTaskQueue();
    void getTasksAnswer(ITaskAnsJoiner & joiner);
    void clear();
};

#endif /* defined(__meteoAppModel__StdProcessor__) */
