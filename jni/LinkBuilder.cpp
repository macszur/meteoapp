//
//  LinkBuilder.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 09.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "LinkBuilder.h"

#include <ctime>
#include <chrono>

std::string LinkBuilder::getProperImageLink(UM_IMAGE_DATE_T t, std::unique_ptr<std::string> customDate)
{
    std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
    std::time_t yesterday_t = std::chrono::system_clock::to_time_t(today - std::chrono::hours(24));
    std::time_t today_t = std::chrono::system_clock::to_time_t(today);
    tm currTm = *std::localtime(&today_t);
    tm yestPtm = *std::localtime(&yesterday_t);

    ss.clear();
    ss.str("");
    //ss.str(host);
    ss<<host<<link_static_part<<"ntype="<<ntype<<"&fdate=";
    if(t == UM_IMAGE_DATE_T::CUSTOM)
    {
        // NOT IMPLEMENTED YET
    }
    else if(t == UM_IMAGE_DATE_T::CURRENT)
    {
        
        
        auto curr_hour = currTm.tm_hour;
        int hour = 0;
        
        if (curr_hour < 6)
        {
            hour = 18;
            currTm = yestPtm;
        }
        else if (curr_hour < 12) hour = 0;
        else if (curr_hour < 18) hour = 6;
        else hour = 12;
        
        char buff[255] = {0};
        sprintf(buff, "%02d%02d%02d%02d", currTm.tm_year + 1900, currTm.tm_mon + 1, currTm.tm_mday, hour);
        
        
        
        ss<<buff; //<<"&row="<<row<<"&col="<<col<<"&lang="<<lang;
    }
    else if(t == UM_IMAGE_DATE_T::PREVIOUS)
    {
        // NOT IMPLEMENTED YET
    }
    ss<<"&row="<<row<<"&col="<<col<<"&lang="<<lang;
    return ss.str();
    
}

auto LinkBuilder::currentUmHour() -> decltype(um_hours.begin())
{
    auto it = um_hours.begin();
    return it;
}