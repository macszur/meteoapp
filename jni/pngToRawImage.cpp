//
//  pngToRawImage.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 17.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "pngToRawImage.h"


bool pngToRawImage::makeRaw(buff_pt pngToConvert, size_t pngSize, Image_t& imageBuff)
{

    imageBuff.dataBuff.clear();
    unsigned int error = lodepng::decode(imageBuff.dataBuff, imageBuff.w, imageBuff.h, pngToConvert, pngSize);
    
    //if there's an error, display it
    if(error)
    {
        Log(std::string("Blad przy konwersji z png to raw image : ") + std::string(lodepng_error_text(error)));
        return false;
    }
    return true;
}