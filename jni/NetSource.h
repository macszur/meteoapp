//
//  NetSource.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__NetSource__
#define __meteoAppModel__NetSource__

#include <iostream>
#include <string>
#include <memory>
#include "globalDefs.h"
#include "Log.h"
#include <netdb.h>

/***
 
 UWAGA: Klasa działa na 90% tzn jestem świadomy że może nie zadziałać, tzn niepoprawnie ściągnąć obrazek
        z serwera. Czemu tak ? Chodzi o chunked-length odpowiedź z serwera http. Jeśli pytanie zadajemy w "standardzie" HTTP 1.1 to właśnie taka zostanie zwrócona. Miałem pisać parser takiej odpowiedzi ale z braku czasu sobie daruje i tego nie zrobię. Jak zatem zrobiłem, że dobrze się ściąga ? Zmieniłem standard requestu na 1.0 gdzie nie było jeszcze chunked-length - okazuje się, że to działa.  ALE ALE ALE ALE ALE ALE - jestem w stanie sobie wyobrazić, że jednak to NIE ZADZIAŁA :/ póki co muszę jednak zostawić ten problem na później bo trzeba nadgonić z pozostałymi częściami modułu.
 
 ***/
class NetSource
{
    static constexpr int MAX_BUFF_SIZE = 1024 * 25;
    static constexpr int MAX_TIMEOUT_SEC = 10;
    std::string url;
    std::string host;
    std::string getStr;
    std::string request;
    //char buff[MAX_BUFF_SIZE];
    addrinfo hints;
    addrinfo *servinfo;
    buff_pt imageBuff = nullptr;
    std::size_t imageBytesNum;
    // addrinfo *p;
    
    bool _isCompleted = false;
    
    bool isConnected = false;
    int port = 80;
    int s_fd = -1;
    int ipAddr = -1;
    
    
    void getDNSip();
    void* get_in_addr(struct sockaddr *sa);
    bool connectToRemote();
    void buildHTTPRequest();
    bool sendHTTPRequest();
    bool getResponseHead();
    bool getResponseData();
    //bool removeHTTPChunksStuff();
public:
    NetSource() = default;
    ~NetSource();
    
    bool init(std::string _url, bool async = false);
    size_t downloadFromUrl();
    const buff_pt getImageData(/*unsigned long & count*/);
    bool isCompleted()
    {
        return _isCompleted;
    }
    
    
};

#endif /* defined(__meteoAppModel__NetSource__) */
