//
//  FileSource.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 27.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_FileSource_h
#define meteoAppModel_FileSource_h

#include <string>
#include <functional>
#include <fstream>
#include <memory>
#include "globalDefs.h"
//#include <android/log.h>

template <class T>
class FileSource {
    
    std::string fileName;
    std::string path;
    std::string getFullPath()
    {
        return path + PATH_DELIMETER + fileName;
    }
    
    std::ifstream file;
    
    std::function<bool(std::ifstream &, T&)> loadHandler;
    
    bool normalRead(T& arg)
    {
        bool ret = false;
        return ret;
    }
    
public:
    void setSpecialLoadHandler(std::function<bool(std::ifstream &, T&)> newHand)
    {
        loadHandler = newHand;
    }
    void setNewFileName(std::string _fname)
    {
        fileName = _fname;
    }
    void setNewPath(std::string _path)
    {
        path = _path;
    }
    
    bool readWholeFile(T & arg)
    {
        bool ret = false;
        file.open(getFullPath().c_str(), std::ios::in);
        
        if (file.is_open())
        {
            if(loadHandler == nullptr)
            {
                ret = normalRead(arg);
            }
            else
                ret = loadHandler(file, arg);
        }
        
        //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Zaladowalem z pliku : %s, ret = %d !!!!!! \n", getFullPath().c_str(), (int)ret);
        file.close();
        return ret;
        
    }
};

#endif
