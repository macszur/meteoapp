//
//  ReadRainTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 07.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadRainTask__
#define __meteoAppModel__ReadRainTask__

#include <iostream>
#include <future>
#include "globalDefs.h"
#include "ITask.h"
#include "ReadRainTaskAns.h"
#include "structures.h"
#include "patto.h"

class ReadRainTask : public ITask
{
    //std::thread workerT;
    ReadRainTaskAns taskAns;
    const Image_t * dataPtr;
    
    
    range rain_range;
    
    patto patternGen;
    imgmatrix imgMatx;
    float rain_with_column(int column);
public:
    ReadRainTask()
    {
        type = std::launch::async;
        rain_range.loc = 137+9;
        rain_range.len = 76;
        
    }
    ~ReadRainTask() = default;
    virtual void setProcessingData(const Image_t * _processData)
    {
        dataPtr = _processData;
        imgMatx.setMatrix(dataPtr);
        patternGen.png = &imgMatx;
    }
    virtual void operator()();
    virtual void getRetValue(ITaskAnsJoiner * joiner);
    
    
    
    //void setCurrImgType(UM_IMAGE_DATE_T type);
};

#endif /* defined(__meteoAppModel__ReadRainTask__) */
