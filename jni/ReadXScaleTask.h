//
//  ReadXScale.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadXScaleTask__
#define __meteoAppModel__ReadXScaleTask__

#include <iostream>
#include <future>
#include "globalDefs.h"
#include "ITask.h"
#include "ReadXScaleTaskAns.h"
#include "structures.h"
#include "patto.h"

class ReadXScaleTask : public ITask
{
    //std::thread workerT;
    ReadXScaleTaskAns taskAns;
    const Image_t * dataPtr;
    FORECAST_TYPE ftype;
    
    char *chars;
    unsigned long long int patterns[11];
    const int PATTERNS_COUNT = 11;
    frame temperature_scale_frame, pressure_scale_frame, wind_scale_frame, rain_scale_frame, cloud_scale_frame;
    patto patternGen;
    imgmatrix imgMatx;
    void first_line(frame _frame, int &v, int &r);
	void last_line(frame _frame, int &v, int &r);
    
    //frame temperature_scale_frame, pressure_scale_frame, wind_scale_frame;
public:
    ReadXScaleTask()
    {
        type = std::launch::async;
        chars = (char *) "1-023456789";
        
        patterns[0] = 434207338892;
        patterns[1] = 589824;
        patterns[2] = 672785844850;
        patterns[3] = 673340286496;
        patterns[4] = 673347373682;
        patterns[5] = 217935742150;
        patterns[6] = 610709637746;
        patterns[7] = 672736561778;
        patterns[8] = 37795291928;
        patterns[9] = 672820447858;
        patterns[10] = 672786844274;
        
        temperature_scale_frame.x = 51;
        rain_scale_frame.x = 137;
        pressure_scale_frame.x = 221;
        wind_scale_frame.x = 307;
        rain_scale_frame.x = 137;
        cloud_scale_frame.x = 519;
        
        temperature_scale_frame.y = pressure_scale_frame.y = wind_scale_frame.y = rain_scale_frame.y = cloud_scale_frame.y = 30;
        temperature_scale_frame.width = pressure_scale_frame.width = wind_scale_frame.width = rain_scale_frame.width = cloud_scale_frame.width = 33;
        temperature_scale_frame.height = pressure_scale_frame.height = wind_scale_frame.height = rain_scale_frame.height = cloud_scale_frame.height = 86;


    }
    ~ReadXScaleTask() = default;
    virtual void setProcessingData(const Image_t * _processData)
    {
        dataPtr = _processData;
        imgMatx.setMatrix(dataPtr);
        patternGen.png = &imgMatx;
    }
    virtual void operator()();
    virtual void getRetValue(ITaskAnsJoiner * joiner);
    
    void setScaleReadType(FORECAST_TYPE _type)
    {
        ftype = _type;
    }
    
//    void setScaleFrame(frame scaleFr)
//    {
//        scaleFrame = scaleFr;
//    }
    
    
    //void setCurrImgType(UM_IMAGE_DATE_T type);
};

#endif /* defined(__meteoAppModel__ReadXScaleTask__) */
