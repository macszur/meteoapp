//
//  ParallelTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 23.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ParallelTask__
#define __meteoAppModel__ParallelTask__

#include <iostream>
#include <thread>
#include "ITask.h"

class ParallelTask : public ITask
{
    std::thread workerT;
public:
    virtual float run();
    virtual bool waitTillDone();
};

#endif /* defined(__meteoAppModel__ParallelTask__) */
