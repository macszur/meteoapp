//
//  StdTaskJoiner.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "StdTaskAnsJoiner.h"
#include <cstdlib>

void StdTaskAnsJoiner::joinTaskAns(ReadAndPrepareTimeTaskAns * task)
{
    testVal = task->getTestVal();
    timestampVect = task->getTimeStampsVector();
}

void StdTaskAnsJoiner::joinTaskAns(ReadXScaleTaskAns * task)
{
    tempScale = task->tempScale;
    pressureScale = task->pressureScale;
    windScale = task->windScale;
    rainScale = task->rainScale;
    cloudScale = task->cloudScale;
}

void StdTaskAnsJoiner::joinTaskAns(ReadTemperatureTaskAns * task)
{
    tempImValues = task->getIntermidiateValuesVector();
}

void StdTaskAnsJoiner::joinTaskAns(ReadWindTaskAns * task)
{
    windImValues = task->getIntermidiateValuesVector();
}

void StdTaskAnsJoiner::joinTaskAns(ReadPressureTaskAns * task)
{
    pressuImValues = task->getIntermidiateValuesVector();
}

void StdTaskAnsJoiner::joinTaskAns(ReadRainTaskAns * task)
{
    rainImValues = task->getIntermidiateValuesVector();
}

void StdTaskAnsJoiner::joinTaskAns(ReadCloudTaskAns * task)
{
    cloudImValues = task->getIntermidiateValuesVector();
}

void StdTaskAnsJoiner::joinTaskAns(TaskAnsB * task)
{
    
}

void StdTaskAnsJoiner::getForecastResult(forecastResult * fResult)
{
    
    fResult->pointsNum = static_cast<int>(timestampVect.size());
    if(fResult->points != nullptr)
        delete [] fResult->points;
    
    // tutaj kopiowanie do nowoutworzonej tablicy o rozmiarze takim jak trzeba zrobic
    
    

    
    fResult->points = new plotPoint[fResult->pointsNum];
    //memset(fResult->points, 0, fResult->pointsNum);
    

    
    if(fResult->ftype == FORECAST_TYPE::temperature)
    {
        fResult->maxScale = tempScale.maxVal;
        fResult->minScale = tempScale.minVal;
        for(int i = 0; i < fResult->pointsNum; ++i)
        {
            
            fResult->points[i].value = tempScale.maxVal + (tempImValues[i] - tempScale.maxY) * (tempScale.minVal - tempScale.maxVal) / (tempScale.minY - tempScale.maxY);;
            fResult->points[i].timeStamp = timestampVect[i];
        }
    }
    else if(fResult->ftype == FORECAST_TYPE::wind)
    {
        fResult->maxScale = windScale.maxVal;
        fResult->minScale = windScale.minVal;
        for(int i = 0; i < fResult->pointsNum; ++i)
        {
            
            fResult->points[i].value = windScale.maxVal + (windImValues[i] - windScale.maxY) * (windScale.minVal - windScale.maxVal) / (windScale.minY - windScale.maxY);;
            fResult->points[i].timeStamp = timestampVect[i];
        }
    }
    
    else if(fResult->ftype == FORECAST_TYPE::pressure)
    {
        fResult->maxScale = pressureScale.maxVal;
        fResult->minScale = pressureScale.minVal;
        for(int i = 0; i < fResult->pointsNum; ++i)
        {
            
            fResult->points[i].value = pressureScale.maxVal + (pressuImValues[i] - pressureScale.maxY) * (pressureScale.minVal - pressureScale.maxVal) / (pressureScale.minY - pressureScale.maxY);;
            fResult->points[i].timeStamp = timestampVect[i];
        }
    }
    
    else if(fResult->ftype == FORECAST_TYPE::rain)
    {
        fResult->maxScale = rainScale.maxVal;
        fResult->minScale = rainScale.minVal;
        for(int i = 0; i < fResult->pointsNum; ++i)
        {
            if(rainImValues[i] == 0)
                fResult->points[i].value = 0;
            else
                fResult->points[i].value = rainScale.maxVal + (rainImValues[i] - rainScale.maxY) * (rainScale.minVal - rainScale.maxVal) / (rainScale.minY - rainScale.maxY);;
            fResult->points[i].timeStamp = timestampVect[i];
        }
    }
    
    else if(fResult->ftype == FORECAST_TYPE::clouds)
    {
        fResult->maxScale = cloudScale.maxVal;
        fResult->minScale = cloudScale.minVal;
        for(int i = 0; i < fResult->pointsNum; ++i)
        {
            if(cloudImValues[i] == 0)
                fResult->points[i].value = 0;
            else
                fResult->points[i].value = cloudScale.maxVal + (cloudImValues[i] - cloudScale.maxY) * (cloudScale.minVal - cloudScale.maxVal) / (cloudScale.minY - cloudScale.maxY);;
            fResult->points[i].timeStamp = timestampVect[i];
        }
    }
    
    
}
