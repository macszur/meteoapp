LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := ICMMeteo
LOCAL_SRC_FILES := ICMMeteo.cpp AndConnector.cpp CCreator.cpp CommonConnector.cpp DataContainer.cpp FileSource.cpp ForecastImageNetSource.cpp IConnector.cpp IProcessor.cpp ITask.cpp ITaskAns.cpp ITaskAnsJoiner.cpp LinkBuilder.cpp LocationListFileSource.cpp Log.cpp NetSource.cpp NullConnector.cpp ParallelTask.cpp ReadAndPrepareTimePTask.cpp ReadAndPrepareTimeTaskAns.cpp ReadCloudTask.cpp ReadCloudTaskAns.cpp ReadPressureTask.cpp ReadPressureTaskAns.cpp ReadRainTask.cpp ReadRainTaskAns.cpp ReadTemperatureTask.cpp ReadTemperatureTaskAns.cpp ReadWindTask.cpp ReadWindTaskAns.cpp ReadXScaleTask.cpp ReadXScaleTaskAns.cpp SingleTask.cpp StdProcessor.cpp StdTaskAnsJoiner.cpp TaskAnsB.cpp iOSConnector.cpp imgmatrix.cpp lodepng.cpp patto.cpp pngToRawImage.cpp x86Connector.cpp
LOCAL_LDLIBS := -llog
LOCAL_CPP_FEATURES := rtti exceptions

include $(BUILD_SHARED_LIBRARY)

