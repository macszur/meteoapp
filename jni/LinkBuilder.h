//
//  LinkBuilder.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 09.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__LinkBuilder__
#define __meteoAppModel__LinkBuilder__

#include <iostream>
#include <string>
#include <memory>
#include <sstream>
#include <list>
#include <sstream>

#include "globalDefs.h"


typedef enum : unsigned int {
    PL = 0,
    EN = 1
} UM_LANG;

//ntype=0u&fdate=2014040906&row=406&col=250&lang=pl

class LinkBuilder
{
    const std::string langs[2] = {"pl", "en"};
    const std::list<std::string> um_hours = {"00", "06", "12", "18"};
    std::stringstream ss;
    
    std::string ntype = "0u";
    std::string lang = "pl";
    //std::string fdate;
    const std::string host = "www.meteo.pl";
    const std::string link_static_part = "/um/metco/mgram_pict.php?";
    
    int row = 0;
    int col = 0;
    
    //char buff[512] = {0};
    
    auto currentUmHour() -> decltype(um_hours.begin());
    
public:
    LinkBuilder()
    {
        
    }
    std::string getProperImageLink(UM_IMAGE_DATE_T t, std::unique_ptr<std::string> customDate = nullptr);
    
    void setNType(std::string n)
    {
        ntype = n;
    }
    void setLang(UM_LANG l)
    {
        lang = langs[l];
    }
    void setRow(int r)
    {
        row = r;
    }
    void setCol(int c)
    {
        col = c;
    }
    
};

#endif /* defined(__meteoAppModel__LinkBuilder__) */
