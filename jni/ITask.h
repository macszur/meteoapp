//
//  ITaskFun.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ITaskFun__
#define __meteoAppModel__ITaskFun__

#include <iostream>
#include <future>
#include "ITaskAnsJoiner.h"
#include "globalDefs.h"

class ITask {

    
public:
    std::launch type = std::launch::async;
    ITask();
    virtual ~ITask();
    virtual void operator()() = 0;
    virtual void setProcessingData(const Image_t * _processData) = 0;
    virtual void getRetValue(ITaskAnsJoiner * joiner) = 0;
};

#endif /* defined(__meteoAppModel__ITaskFun__) */
