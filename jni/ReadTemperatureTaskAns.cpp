//
//  ReadTemperatureTaskAns.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadTemperatureTaskAns.h"

void ReadTemperatureTaskAns::Accept(ITaskAnsJoiner * joiner)
{
    if(joiner != nullptr)
        joiner->joinTaskAns(this);
}