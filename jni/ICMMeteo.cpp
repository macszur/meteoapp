#include <string.h>


#include <iostream>
#include <cstdio>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <fstream>

#include "globalDefs.h"
#include "CCreator.h"
#include "NetSource.h"


#ifdef m__And
#include <jni.h>
#include <android/log.h>
#include "AndConnector.h"
#endif

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */


#ifdef m__And

bool gLibInitialized = false;
CCreator mainFabric;
std::unique_ptr<IConnector> connector;
//location * allLocations = nullptr;


extern "C" {
    JNIEXPORT jstring JNICALL Java_macszur_ndktester_MainActivity_stringFromJNI( JNIEnv* env,
                                                                                jobject thiz )
    {
#if defined(__arm__)
#if defined(__ARM_ARCH_7A__)
#if defined(__ARM_NEON__)
#define ABI "armeabi-v7a/NEON"
#else
#define ABI "armeabi-v7a"
#endif
#else
#define ABI "armeabi"
#endif
#elif defined(__i386__)
#define ABI "x86"
#elif defined(__mips__)
#define ABI "mips"
#else
#define ABI "unknown"
#endif
        
    	std::srand( std::time( NULL ) );
        typedef std::vector < int > VDaneT;
        VDaneT dane( 10 );
        for( std::size_t i = 0; i < dane.size(); ++i )
            dane[ i ] = rand() % 50 + 1;
        
        std::sort( dane.begin(), dane.end(),[]( const int & a, const int & b )->bool { return a > b; } );
        
        for( std::size_t i = 0; i < dane.size(); ++i )
            std::printf( "%d, ", dane[ i ] );
        
        std::cout<<"DUPA !!!";
        
        return (env)->NewStringUTF("Hello from JNI !  Compiled with ABI " ABI ".");
    }
    
    
    JNIEXPORT jstring JNICALL Java_com_example_icmmeteo_view_mainFragments_HelpFragment_creatorAndTest( JNIEnv* env,
                                                                                                       jobject thiz )
    {
        //	    //CCreator mainFabric;
        //	    location * locTab = new location[5000];
        //	    int count = 5000;
        //	    int ret = -1;
        //	    std::unique_ptr<IConnector> connector = mainFabric.createProperFacade();
        //	    connector->initialize();
        //	    ret = connector->getDefaultLocationList(locTab, count);
        //	    if(ret != 1)
        //	    {
        //	    	__android_log_print(ANDROID_LOG_DEBUG, "LOG_TAG", "\n DUUUUUPA !!!!!! \n");
        //	    	return (env)->NewStringUTF("ZDUPIONO LOKALIZACJE");
        //	    }
        //	    for(int i = 0; i < count; ++i)
        //	    {
        //	        std::cout<<"Miasto : "<<locTab[i].name<<std::endl;
        //	        std::cout<<"powiat : "<<locTab[i].powiat<<std::endl;
        //	        std::cout<<"id : "<<locTab[i].locID<<std::endl;
        //	        std::cout<<"X : "<<locTab[i].X<<std::endl;
        //	        std::cout<<"Y : "<<locTab[i].Y<<std::endl;
        //	        std::cout<<std::endl;
        //	    }
        
        return (env)->NewStringUTF("WCZYTANO LOKALIZACJE");
    }
    
    
    JNIEXPORT jboolean JNICALL Java_com_example_icmmeteo_cppNative_CppNative_initializeLib(JNIEnv * env, jobject thiz)
    {
        //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n FZaczynamy Inicjalizacje biblioteki !!!!!! \n");
        jboolean ret = true;
        if(gLibInitialized == true)
        {
            return true;
        }
        
        connector = mainFabric.createProperFacade();
        //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Utworzylem FAKADE!!!!!! \n");
        if(!connector->initialize())
        {
            //__android_log_print(ANDROID_LOG_ERROR, "ICM_METEO_LIB", "\n Blad ladowania biblioteki do wczytywania wykresow !\n");
            return false;
        }
        
        gLibInitialized = true;
        //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Ustawilem gLibInitialized na true !!!!!! \n");
        
        
        return ret;
    }
    
    
    JNIEXPORT jint JNICALL Java_com_example_icmmeteo_cppNative_CppNative_loadCitiesList(JNIEnv * env, jobject thiz)
    {
        
        if(gLibInitialized == false)
        {
            //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n FALSE ma gLibInitialized :( !!!!!! \n");
            return 0;
        }
        
        
        int locCount = 10000;
        
        int msg = connector->loadDefaultLocationList(locCount);
        
        //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Zaladowalem z pliku do tymczasowej pamieci, locCount = %d, msg = %d !!!!!! \n", locCount, msg);
        
        
        return locCount;
    }
    
    
    
    JNIEXPORT jint JNICALL Java_com_example_icmmeteo_cppNative_CppNative_fillCitiesList(JNIEnv * env, jobject thiz, jobjectArray listObj)
    {
        
        if(gLibInitialized == false)
        {
            return 0;
        }
        
        auto p = connector.get();
        
        int ret = ((AndConnector*)p)->getDefaultLocationListOnAndroid(env, thiz, listObj);
        
        
        return ret;
    }
    
    JNIEXPORT jboolean JNICALL Java_com_example_icmmeteo_cppNative_CppNative_loadNewestWeatherForecast(JNIEnv * env, jobject thiz, jobject loc)
    {
        
        auto p = connector.get();
        
        bool ret = ((AndConnector*)p)->loadNewestWeatherForecastOnAndroid(env, thiz, loc);
        
        return ret;
    }
    
    JNIEXPORT jobject JNICALL Java_com_example_icmmeteo_cppNative_CppNative_getNewestWeatherForecastOnAndroid(JNIEnv * env, jobject thiz, jint ftype)
	{

		auto p = connector.get();

		auto ret = ((AndConnector*)p)->getNewestWeatherForecastOnAndroid(env, thiz, ftype);

		return ret;
	}

    JNIEXPORT jbyteArray JNICALL Java_com_example_icmmeteo_cppNative_CppNative_getLoadedWeatherBitmapBytes(JNIEnv * env, jobject thiz)
	{
        
		auto p = connector.get();
        
		return ((AndConnector*)p)->getLoadedForecastImageOnAndroid(env, thiz);
	}
    
    
    
    JNIEXPORT jint JNICALL Java_com_example_icmmeteo_cppNative_CppNative_testDownloadImage(JNIEnv * env, jobject thiz)
    {
    	unsigned long count = 5000;
        //    	NetSource netS;
        //        netS.init("www.meteo.pl/um/metco/mgram_pict.php?ntype=0u&fdate=2014040906&row=406&col=250&lang=pl");
        //        netS.downloadFromUrl();
        //        // "/data/data/com.example.icmmeteo/files/imageDzis.png"
        //        std::ofstream plik("/mnt/sdcard/plik.png", std::ios::out | std::ios::binary);
        //        __android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Czy plik sie otworzyl = %d !!!!!! \n", plik.is_open());
        //        if(plik.is_open())
        //        	plik.write(netS.getImageData(count), count);
        //        plik.close();
        
        return count;
    }
    
}
#endif

