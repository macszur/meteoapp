//
//  StdProcessor.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 30.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "StdProcessor.h"
#include "Log.h"

StdProcessor::StdProcessor()
{
    
}

StdProcessor::~StdProcessor()
{
    
}

void StdProcessor::addTaskToQueue(std::unique_ptr<ITask> pt)
{
    pt->setProcessingData(processData);
    taskTab.push_back(taskPair(std::move(pt), std::future<void>()));
}

//void StdProcessor::setProcessingData(const Image_t& data)
//{
//    
//}

void StdProcessor::startTaskQueue()
{
//    for (int i = 0; i < taskTab.size(); ++i)
//    {
//        taskTab[i].second = std::async(std::launch::any, [&i, this](){
//            (taskTab[i].first)->operator()();
//        }); //
//        Log("jestem w startTaskQueue po wykonaniu std::async");
//    }
    int i = 0;
    for(auto & tp : taskTab)
    {
        tp.second = std::async(tp.first->type, [&tp](){
            tp.first->operator()();
        });
        //tp.second = hand;
        Log(std::string("jestem w startTaskQueue po wykonaniu std::async") + std::to_string(i));
        ++i;
    }
}

void StdProcessor::getTasksAnswer(ITaskAnsJoiner & joiner)
{
    for(auto & tp : taskTab)
    {
        tp.second.wait();
        tp.first->getRetValue(&joiner);
    }
}

void StdProcessor::clear()
{
    taskTab.clear();
}