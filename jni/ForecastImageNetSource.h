//
//  ForecastImageNetSource.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 14.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ForecastImageNetSource__
#define __meteoAppModel__ForecastImageNetSource__

#include <string>
#include <memory>
#include "globalDefs.h"
#include "NetSource.h"
#include "LinkBuilder.h"

#include "IForecastImageSource.h"

class ForecastImageNetSource : public IForecastImageSource
{
    NetSource netS;
    LinkBuilder lb;
    
public:
    ForecastImageNetSource();
    ~ForecastImageNetSource();
    virtual const buff_pt getImageData();
    virtual size_t loadImage(const location &l, UM_IMAGE_DATE_T t);
    //virtual size_t getImageSize();
};

#endif /* defined(__meteoAppModel__ForecastImageNetSource__) */
