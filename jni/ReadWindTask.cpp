//
//  ReadWindTask.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 03.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "ReadWindTask.h"
#include "Log.h"

void ReadWindTask::operator()()
{
    taskAns.setTestVal(123.123);
    int startColumn = FORECAST_STARTING_COLUMN;
    for(int i = 0; i < FORECAST_WIDTH; ++i)
    {
        taskAns.addImVal(wind_with_column(startColumn + i));
    }
    
    
    //Log("zakres od :" + std::to_string(taskAns.firstLineVal) + " do : " + std::to_string(taskAns.lastLineVal));
    
    
}

void ReadWindTask::getRetValue(ITaskAnsJoiner * joiner)
{
    taskAns.Accept(joiner);
    //return 0;
}

float ReadWindTask::wind_with_column(int column)
{
    range _range = wind_range;
    float immidiateWind = 0;
    bool found = false;
    // dwie kolejne 255
    
    for(int i = _range.loc; i < _range.loc + _range.len; ++i)
    {
        if(patternGen.png->red_pixel(column, i) == 17 && patternGen.png->green_pixel(column, i) == 17)
        {
            immidiateWind = i;
            found = true;
            break;
        }
    }
    if(found == false)
    {
        immidiateWind = wind_with_column(column - 1);
    }
    
    
    return immidiateWind;
}