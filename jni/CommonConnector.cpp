//
//  CommonConnector.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "CommonConnector.h"
#include "DataContainer.h"
#include "IProcessor.h"
#include "StdProcessor.h"
#include "ReadAndPrepareTimePTask.h"
#include "ReadTemperatureTask.h"
#include "ReadWindTask.h"
#include "ReadPressureTask.h"
#include "ReadRainTask.h"
#include "ReadCloudTask.h"
#include "structures.h"
#include <memory>

class CommonConnector::CommonConnectorImpl
{
    bool onoff;
    std::unique_ptr<DataContainer> dataCont;
    std::unique_ptr<IProcessor> taskProcessor;
    forecastResult fr;
public:
    bool initialize()
    {
        dataCont = std::unique_ptr<DataContainer>(new DataContainer);
        if(dataCont == nullptr)
            return false;
        
        taskProcessor = std::unique_ptr<StdProcessor>(new StdProcessor);
        if(taskProcessor == nullptr)
            return false;
        
        return true;
    }
    
    /*
     *  Tutaj odbywa się główne ładowanie pogody. Trzeba zrobić tak o :
     *  1. pobrać obrazek z neta i przekonwertować go na tablicę pixeli
     *  2. Załadować go do procesorra i uruchomić przetwarzanie
    *   3. Pobrać sobie funkcją getNewestWeatherForecast
     */
    bool loadNewestWeatherForecast(location where)
    {
        if(dataCont == nullptr)
            return false;
        Log(std::string("Pobieranie obrazka z lokazlizacji -> x:") + std::to_string(where.X) + std::string(" y:") + std::to_string(where.Y));
        if(!dataCont->loadCurrentForecastImg(where))
        {
            Log(std::string("Nie udalo sie pobrac obrazka !!"));
            
            // Tutaj powinno byc ladowanie w takim przypadku poprzedniej prognozy pogody !
            
            return false;
        }
        
        
        
        
        return true;
    }
    
    const Image_t& getLoadedForecastImage()
    {
        Log("Proboje zwrocic najnowszy obrazek pogodowy...");
        if(dataCont == nullptr)
        {
            Log("DataContainer jest null'em wiec musze zwrocic gunwo :(");
            return Image_t();
        }
        
        return dataCont->getCurrentForecastImg();

    }
    
    forecastResult getNewestWeatherForecast(FORECAST_TYPE type)
    {
        if(dataCont == nullptr)
            return forecastResult();
        
        if(taskProcessor == nullptr)
            return forecastResult();
        
        taskProcessor->clear();
        StdTaskAnsJoiner stdTJ;
        
//        frame temperature_scale_frame, pressure_scale_frame, wind_scale_frame;
//        temperature_scale_frame.x = 51;
//        pressure_scale_frame.x = 221;
//        wind_scale_frame.x = 307;
//        
//        temperature_scale_frame.y = pressure_scale_frame.y = wind_scale_frame.y = 30;
//        temperature_scale_frame.width = pressure_scale_frame.width = wind_scale_frame.width = 33;
//        temperature_scale_frame.height = pressure_scale_frame.height = wind_scale_frame.height = 86;
        
        std::unique_ptr<ReadAndPrepareTimePTask> timeTaskPtr = std::unique_ptr<ReadAndPrepareTimePTask>(new ReadAndPrepareTimePTask());
        timeTaskPtr->setCurrImgType(UM_IMAGE_DATE_T::CURRENT);
        
        std::unique_ptr<ReadXScaleTask> scaleTaskPtr = std::unique_ptr<ReadXScaleTask>(new ReadXScaleTask());
        scaleTaskPtr->setScaleReadType(type);
        
        std::unique_ptr<ITask> taskToProcess;
        
        if(type == FORECAST_TYPE::temperature)
        {
            taskToProcess = std::unique_ptr<ReadTemperatureTask>(new ReadTemperatureTask());
        }
        else if(type == FORECAST_TYPE::wind)
        {
            taskToProcess = std::unique_ptr<ReadWindTask>(new ReadWindTask());
        }
        else if(type == FORECAST_TYPE::pressure)
        {
            taskToProcess = std::unique_ptr<ReadPressureTask>(new ReadPressureTask());
        }
        else if(type == FORECAST_TYPE::rain)
        {
            taskToProcess = std::unique_ptr<ReadRainTask>(new ReadRainTask());
        }
        else if(type == FORECAST_TYPE::clouds)
        {
            taskToProcess = std::unique_ptr<ReadCloudTask>(new ReadCloudTask());
        }
        
        
        taskProcessor->setProcessingData(&dataCont->getCurrentForecastImg());
        taskProcessor->addTaskToQueue(std::move(timeTaskPtr));
        taskProcessor->addTaskToQueue(std::move(scaleTaskPtr));
        taskProcessor->addTaskToQueue(std::move(taskToProcess));
        taskProcessor->startTaskQueue();
        taskProcessor->getTasksAnswer(stdTJ);
        fr.ftype = type;
        stdTJ.getForecastResult(&fr);
        return fr;
    }
    
    forecastResult newestWeatherForecast(GPSpoint where)
    {
        return forecastResult();
    }
    
    currentForecastResult getCurrentWeather(location where)
    {
        return currentForecastResult();
    }
    currentForecastResult getCurrentWeather(GPSpoint where)
    {
        return currentForecastResult();
    }
    
    void workOnLocalImage(Image_t * img, bool onoff)
    {
        
    }
    
    LocationListFileSource::location_hash_set_t getDefaultLocationList()
    {
        return dataCont->getLocationListFromFile();
    }
    size_t loadDefaultLocationList(int & count)
    {
        size_t size = dataCont->loadLocationListFromFile();
        if(count < size)
        {
            count = 0;
            return 0;
        }
        count = size;
        return 1;
    }
    
    location getXYfromGPS(GPSpoint pkt)
    {
        return location();
    }
    
};

CommonConnector::CommonConnector() : impl(new CommonConnectorImpl())
{
    
}

CommonConnector::~CommonConnector()
{
    
}


bool CommonConnector::initialize()
{
    return impl->initialize();
}

bool CommonConnector::loadNewestWeatherForecast(location where)
{
    return impl->loadNewestWeatherForecast(where);
}

const Image_t& CommonConnector::getLoadedForecastImage()
{
    return impl->getLoadedForecastImage();
}

forecastResult CommonConnector::getNewestWeatherForecast(FORECAST_TYPE type)
{
    return impl->getNewestWeatherForecast(type);
}
forecastResult CommonConnector::newestWeatherForecast(GPSpoint where)
{
    return impl->newestWeatherForecast(where);
}

currentForecastResult CommonConnector::getCurrentWeather(location where)
{
    return impl->getCurrentWeather(where);
}
currentForecastResult CommonConnector::getCurrentWeather(GPSpoint where)
{
    return impl->getCurrentWeather(where);
}

void CommonConnector::workOnLocalImage(Image_t * img, bool onoff)
{
    impl->workOnLocalImage(img, onoff);
}

LocationListFileSource::location_hash_set_t CommonConnector::getDefaultLocationList()
{
    return impl->getDefaultLocationList();
}
int CommonConnector::loadDefaultLocationList(int & count)
{
    return impl->loadDefaultLocationList(count);
}

location CommonConnector::getXYfromGPS(GPSpoint pkt)
{
    return impl->getXYfromGPS(pkt);
}
