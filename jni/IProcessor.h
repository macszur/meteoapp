//
//  IProcessor.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_IProcessor_h
#define meteoAppModel_IProcessor_h

#include <memory>
#include <vector>
#include <utility>
#include "globalDefs.h"
#include "ITask.h"

using taskPair = std::pair<std::unique_ptr<ITask>, std::future<void>>;

class IProcessor
{
protected:
    
    std::vector<taskPair> taskTab;
    const Image_t * processData;
public:
    IProcessor();
    virtual ~IProcessor();
    virtual void addTaskToQueue(std::unique_ptr<ITask> pt) = 0;
    virtual void setProcessingData(const Image_t * data)
    {
        processData = data;
    }
    virtual void startTaskQueue() = 0;
    virtual void getTasksAnswer(ITaskAnsJoiner & joiner) = 0;
    virtual void clear() = 0;
};

#endif
