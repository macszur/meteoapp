//
//  ReadPressureTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 07.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadPressureTask__
#define __meteoAppModel__ReadPressureTask__

#include <iostream>
#include <future>
#include "globalDefs.h"
#include "ITask.h"
#include "ReadPressureTaskAns.h"
#include "structures.h"
#include "patto.h"

class ReadPressureTask : public ITask
{
    //std::thread workerT;
    ReadPressureTaskAns taskAns;
    const Image_t * dataPtr;
    
    
    range pressure_range;
    
    patto patternGen;
    imgmatrix imgMatx;
    float pressure_with_column(int column);
    
public:
    ReadPressureTask()
    {
        type = std::launch::async;
        pressure_range.loc = 230;
        pressure_range.len = 76;
        
    }
    ~ReadPressureTask() = default;
    virtual void setProcessingData(const Image_t * _processData)
    {
        dataPtr = _processData;
        imgMatx.setMatrix(dataPtr);
        patternGen.png = &imgMatx;
    }
    virtual void operator()();
    virtual void getRetValue(ITaskAnsJoiner * joiner);
    
    
    
    //void setCurrImgType(UM_IMAGE_DATE_T type);
};

#endif /* defined(__meteoAppModel__ReadPressureTask__) */
