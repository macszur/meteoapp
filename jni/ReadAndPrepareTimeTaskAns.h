//
//  TaskAnsA.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__TaskAnsA__
#define __meteoAppModel__TaskAnsA__

#include <iostream>
#include <vector>
#include "globalDefs.h"
#include "ITaskAnsJoiner.h"

class ReadAndPrepareTimeTaskAns : public ITaskAns
{
    float val = -5;
    std::vector<double> timestamps;
    
public:
    ReadAndPrepareTimeTaskAns()
    {
        timestamps.reserve(FORECAST_WIDTH);
    }
    void addNewTs(double tsToAdd)
    {
        timestamps.push_back(tsToAdd);
    }
    std::vector<double> & getTimeStampsVector()
    {
        return timestamps;
    }
    
    void setTestVal(float _val)
    {
        val = _val;
    }
    float getTestVal()
    {
        return val;
    }
    virtual void Accept(ITaskAnsJoiner * joiner);
};

#endif /* defined(__meteoAppModel__TaskAnsA__) */
