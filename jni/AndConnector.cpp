//
//  AndConnector.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "AndConnector.h"


AndConnector::AndConnector() : commonPart(new CommonConnector())
{
    
}

AndConnector::~AndConnector()
{
    
}


bool AndConnector::initialize()
{
    return commonPart->initialize();
}

bool AndConnector::loadNewestWeatherForecast(location where)
{
    return commonPart->loadNewestWeatherForecast(where);
}

const Image_t& AndConnector::getLoadedForecastImage()
{
    return commonPart->getLoadedForecastImage();
}

forecastResult AndConnector::getNewestWeatherForecast(FORECAST_TYPE type)
{
    return commonPart->getNewestWeatherForecast(type);
}
forecastResult AndConnector::newestWeatherForecast(GPSpoint where)
{
    return commonPart->newestWeatherForecast(where);
}

currentForecastResult AndConnector::getCurrentWeather(location where)
{
    return commonPart->getCurrentWeather(where);
}
currentForecastResult AndConnector::getCurrentWeather(GPSpoint where)
{
    return commonPart->getCurrentWeather(where);
}

void AndConnector::workOnLocalImage(Image_t * img, bool onoff)
{
    commonPart->workOnLocalImage(img, onoff);
}

std::vector<location> AndConnector::getDefaultLocationList()
{
    return commonPart->getDefaultLocationList();
}

int AndConnector::loadDefaultLocationList(int & count)
{
    return commonPart->loadDefaultLocationList(count);
}

location AndConnector::getXYfromGPS(GPSpoint pkt)
{
    return commonPart->getXYfromGPS(pkt);
}

#ifdef m__And

void AndConnector::fillCityItemElement(JNIEnv * env, location & ls, jobject obj)
{
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n wartosci %d : %d  !!!!!! \n", env, obj);
    
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Tu 2 !!!!!! \n");
    
    
	jclass cls = env->GetObjectClass(obj);
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Tu 1  !!!!!! \n");
    
	jfieldID idName = env->GetFieldID(cls, "name", "Ljava/lang/String;");
	jfieldID idPowiat = env->GetFieldID(cls, "powiat", "Ljava/lang/String;");
	jfieldID idX= env->GetFieldID(cls, "x", "I");
	jfieldID idY = env->GetFieldID(cls, "y", "I");
	jfieldID idCityID = env->GetFieldID(cls, "cityID", "I");
    
    jstring name = env->NewStringUTF(ls.name);
    jstring powiat = env->NewStringUTF(ls.powiat);
    
    env->SetObjectField(obj, idName, name);
    env->SetObjectField(obj, idPowiat, powiat);
    env->SetIntField(obj, idX, ls.X);
    env->SetIntField(obj, idY, ls.Y);
    env->SetIntField(obj, idCityID, ls.locID);
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Tu 3 !!!!!! \n");
    
    //env->ReleaseStringUTFChars(name, ls.name);
    env->DeleteLocalRef(name);
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Tu 4 !!!!!! \n");
    
    env->DeleteLocalRef(powiat);
    //env->ReleaseStringUTFChars(powiat, ls.powiat);
    
    
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Tu 5 !!!!!! \n");
    
}

int AndConnector::getDefaultLocationListOnAndroid(JNIEnv * env, jobject thiz, jobjectArray listObj)
{
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Czas przepisac z tymczasowej pamieci do tablicy cityitemow !!!!!! \n");
    
    int len = env->GetArrayLength(listObj);
    
    auto l = commonPart->getDefaultLocationList();
    
    jobject obj = (jobject) env->GetObjectArrayElement(listObj, 0);
    jstring name;
    jstring powiat;
    
    jclass cls = env->GetObjectClass(obj);
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Tu 1  !!!!!! \n");
    
	jfieldID idName = env->GetFieldID(cls, "name", "Ljava/lang/String;");
	jfieldID idPowiat = env->GetFieldID(cls, "powiat", "Ljava/lang/String;");
	jfieldID idX= env->GetFieldID(cls, "x", "I");
	jfieldID idY = env->GetFieldID(cls, "y", "I");
	jfieldID idCityID = env->GetFieldID(cls, "cityID", "I");
	env->DeleteLocalRef(obj);
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Przepisze %d itemow !!!!!! \n", len);
    
    for(int i=0; i<len; ++i)
    {
        obj = (jobject) env->GetObjectArrayElement(listObj, i);
        
        //fillCityItemElement(env, l[i], obj);
        
        name = env->NewStringUTF(l[i].name);
		powiat = env->NewStringUTF(l[i].powiat);
        
		env->SetObjectField(obj, idName, name);
		env->SetObjectField(obj, idPowiat, powiat);
		env->SetIntField(obj, idX, l[i].X);
		env->SetIntField(obj, idY, l[i].Y);
		env->SetIntField(obj, idCityID, l[i].locID);
        
		env->DeleteLocalRef(name);
		env->DeleteLocalRef(powiat);
        env->DeleteLocalRef(obj);
        
        
        //_ _android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n jeb 3 %d !!!!!! \n", i);
        
    }
    
    env->DeleteLocalRef(cls);
    
    //__android_log_print(ANDROID_LOG_DEBUG, "ICM_METEO_LIB", "\n Przepisalem %d itemow !!!!!! \n", len);
    return 1;
}

bool AndConnector::loadNewestWeatherForecastOnAndroid(JNIEnv * env, jobject thiz, jobject loc)
{
	jclass cls = env->GetObjectClass(loc);
    
    
    
	//jfieldID idName = env->GetFieldID(cls, "name", "Ljava/lang/String;");
	//jfieldID idPowiat = env->GetFieldID(cls, "powiat", "Ljava/lang/String;");
	jfieldID idX= env->GetFieldID(cls, "x", "I");
	jfieldID idY = env->GetFieldID(cls, "y", "I");
	//jfieldID idCityID = env->GetFieldID(cls, "cityID", "I");
    
	//jstring name = env->NewStringUTF(ls.name);
	//jstring powiat = env->NewStringUTF(ls.powiat);
    
    
	location ls;
	ls.X = env->GetIntField(loc, idX);
	ls.Y = env->GetIntField(loc, idY);
    
	Log(std::string("AndConnector: Ladowanie najnowszej pogody dla lokalizacji :") + std::to_string(ls.X)  + std::string(" ") + std::to_string(ls.Y));
    
	return loadNewestWeatherForecast(ls);
}

jobject AndConnector::getNewestWeatherForecastOnAndroid(JNIEnv * env, jobject thiz, int ftype)
{
	Log("Przetwarzanie najnowszej pogody typu : " + std::to_string(ftype));
    
	FORECAST_TYPE frtype = static_cast<FORECAST_TYPE>(ftype);
    
	forecastResult fr = getNewestWeatherForecast(frtype);
    
	Log("Pogode przetworzono, kopiowanie do javy...");
    
	// trzeba przepisac z forecastResulta do javy
	jclass frClass = env->FindClass("com/example/icmmeteo/cppNative/ForecastResult");
	if (frClass == 0)
	{
		Log("Nie znaleziono klasy ForecastResult :(");
	}
    
	jmethodID frConstructorMtd = env->GetMethodID(frClass, "<init>", "([D[F[I)V");
	if (frConstructorMtd == 0)
	{
		Log("Nie znaleziono konstuktora ForecastResult :(");
	}
    
	const int SCALE_SIZE = 2;
    
	float * valTab = new float[fr.pointsNum];
	double * tsTab = new double[fr.pointsNum];
	int * scaleTab = new int[SCALE_SIZE];
    
	Log("Przepisanie z AoS do dwoch tablic...");
	for(int i = 0; i < fr.pointsNum; ++i)
	{
		valTab[i] = fr.points[i].value;
		tsTab[i] = fr.points[i].timeStamp;
	}
    
	Log(std::string("Przepisanie skali :") + std::to_string(fr.minScale) + std::string(" ") + std::to_string(fr.maxScale));
	scaleTab[0] = fr.minScale;
	scaleTab[1] = fr.maxScale;
    
    
	Log("Kopiowanie do jarrays ...");
	jfloatArray valuesArr = env->NewFloatArray(fr.pointsNum);
	env->SetFloatArrayRegion(valuesArr, 0, fr.pointsNum,  (jfloat *)valTab);
	jdoubleArray timestampsArr = env->NewDoubleArray(fr.pointsNum);
	env->SetDoubleArrayRegion(timestampsArr, 0, fr.pointsNum, (jdouble *)tsTab);
	jintArray scaleArr = env->NewIntArray(SCALE_SIZE);
	env->SetIntArrayRegion(scaleArr, 0, SCALE_SIZE, (jint *)scaleTab);
    
	Log("Tworzenie nowego obiektu java ForecastResult ...");
	jobject frObj = env->NewObject(frClass, frConstructorMtd, timestampsArr, valuesArr, scaleArr);
    
	delete [] valTab;
	delete [] tsTab;
	delete [] scaleTab;
    
	return frObj;
}

jbyteArray AndConnector::getLoadedForecastImageOnAndroid(JNIEnv * env, jobject thiz)
{
    
	Image_t imgRef = getLoadedForecastImage();
    
    //	jobject jbitmap = 0;
    //
    //	jclass jbitmap_class = env->FindClass("android/graphics/Bitmap");
    //	jmethodID createBitmapID = env->GetStaticMethodID(jbitmap_class, "createBitmap", "([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
    //
    //	jstring j_config_name = env->NewStringUTF("ARGB_8888");
    //	jclass bcfg_class = env->FindClass("android/graphics/Bitmap$Config");
    //	jobject java_bitmap_config = env->CallStaticObjectMethod(bcfg_class, env->GetStaticMethodID(bcfg_class, "valueOf", "(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;"), j_config_name);
    
    
	// KONWERSJA RGBA->ARGB
    //	unsigned int* buf = &imgRef.dataBuff[0];
    //	unsigned int* end = buf + imgRef.dataBuff.size()/4;
    //	while(buf < end) {
    //		unsigned int pixel = *buf;
    //		*buf = ((pixel&0xffffff00)>>8) + ((pixel&0xff)<<24);
    //		buf++;
    //	}
    
    //	for(uint pos = 0; pos < imgRef.dataBuff.size(); pos += 4) {
    //	    unsigned char r = imgRef.dataBuff[pos+0];
    //	    unsigned char g = imgRef.dataBuff[pos+1];
    //	    unsigned char b = imgRef.dataBuff[pos+2];
    //	    unsigned char a = imgRef.dataBuff[pos+3];
    //	    imgRef.dataBuff[pos+0] = a;
    //	    imgRef.dataBuff[pos+1] = r;
    //	    imgRef.dataBuff[pos+2] = g;
    //	    imgRef.dataBuff[pos+3] = b;
    //	}
    
	int byte_array_length = imgRef.dataBuff.size();
	Log(std::string("AndConnector: Wielkosc tablicy :") + std::to_string(byte_array_length));
	jbyteArray byte_array = env->NewByteArray(byte_array_length);
	env->SetByteArrayRegion(byte_array, 0, byte_array_length,  (jbyte *)&imgRef.dataBuff[0]);
    
    
    //	jbitmap = env->CallStaticObjectMethod(jbitmap_class, createBitmapID, byte_array, imgRef.w, imgRef.h, java_bitmap_config);
    //
    //		if(jbitmap == 0) {
    //			Log("Could not create image from framebuffer to share");
    //
    //		  }
    
    
    
	//get the BitmapFactory class
    //	env->FindClass()
    //	jclass bitmap_factory_class = loadExternalClass("android/graphics/BitmapFactory");
    //	jmethodID decode_byte_array_method = env->GetStaticMethodID(bitmap_factory_class,
    //		"decodeByteArray", "([BII)Landroid/graphics/Bitmap;");
    //
    //	//get the bitmap itself
    //	jbitmap = env->CallStaticObjectMethod(bitmap_factory_class, decode_byte_array_method,
    //		byte_array, 0, byte_array_length);
    
	//env->DeleteLocalRef(byte_array);
    
	return byte_array;
    
    
    
}

#endif
