//
//  patto.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__patto__
#define __meteoAppModel__patto__

#include "imgmatrix.h"
#include "structures.h"

class patto
{
    char *filename;
    
public:
    patto()
    {
        
    }

    imgmatrix *png;
    
	unsigned long long int pattern_for_frame(frame _frame);
    unsigned long long int full_pattern();
};

#endif /* defined(__meteoAppModel__patto__) */
