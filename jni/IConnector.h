//
//  IConnector.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 23.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_IConnector_h
#define meteoAppModel_IConnector_h

#include "globalDefs.h"
#include <vector>

class IConnector
{
public:
    IConnector() = default;
    virtual ~IConnector();
    
    virtual bool initialize() = 0;
    
    virtual bool loadNewestWeatherForecast(location where) = 0;
    virtual const Image_t& getLoadedForecastImage() = 0;
    virtual forecastResult getNewestWeatherForecast(FORECAST_TYPE type) = 0;
    virtual forecastResult newestWeatherForecast(GPSpoint where) = 0;
    
    virtual currentForecastResult getCurrentWeather(location where) = 0;
    virtual currentForecastResult getCurrentWeather(GPSpoint where) = 0;
    
    virtual void workOnLocalImage(Image_t * img, bool onoff) = 0;
    
    virtual int loadDefaultLocationList(int &count) = 0;
    virtual std::vector<location> getDefaultLocationList() = 0;
    virtual location getXYfromGPS(GPSpoint pkt) = 0;
    
};

#endif
