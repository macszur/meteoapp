//
//  StdTaskJoiner.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__StdTaskAnsJoiner__
#define __meteoAppModel__StdTaskAnsJoiner__

#include <iostream>
#include "globalDefs.h"
#include "ITaskAnsJoiner.h"
#include "ReadAndPrepareTimeTaskAns.h"
#include "ReadXScaleTask.h"
#include "ReadTemperatureTaskAns.h"
#include "ReadWindTaskAns.h"
#include "ReadPressureTaskAns.h"
#include "ReadRainTaskAns.h"
#include "ReadCloudTaskAns.h"

class StdTaskAnsJoiner: public ITaskAnsJoiner
{
    float testVal = 0;
    std::vector<double> timestampVect;
    std::vector<float> tempImValues;
    std::vector<float> windImValues;
    std::vector<float> pressuImValues;
    std::vector<float> rainImValues;
    std::vector<float> cloudImValues;

    
    scale_t tempScale;
    scale_t pressureScale;
    scale_t windScale;
    scale_t rainScale;
    scale_t cloudScale;
    
public:
    virtual void joinTaskAns(ReadAndPrepareTimeTaskAns * task);
    virtual void joinTaskAns(ReadXScaleTaskAns * task);
    virtual void joinTaskAns(ReadTemperatureTaskAns * task);
    virtual void joinTaskAns(ReadWindTaskAns * task);
    virtual void joinTaskAns(ReadPressureTaskAns * task);
    virtual void joinTaskAns(ReadRainTaskAns * task);
    virtual void joinTaskAns(ReadCloudTaskAns * task);
    
    virtual void joinTaskAns(TaskAnsB * task);
    //virtual void joinTaskAns(TaskAnsC *);
    
    virtual void getForecastResult(forecastResult * fResult);
};

#endif /* defined(__meteoAppModel__StdTaskAnsJoiner__) */
