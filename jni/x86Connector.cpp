//
//  x86Connector.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "x86Connector.h"


x86Connector::x86Connector() : commonPart(new CommonConnector())
{
    
}

x86Connector::~x86Connector()
{
    
}


bool x86Connector::initialize()
{
    return commonPart->initialize();
}

bool x86Connector::loadNewestWeatherForecast(location where)
{
    return commonPart->loadNewestWeatherForecast(where);
}

const Image_t& x86Connector::getLoadedForecastImage()
{
    return commonPart->getLoadedForecastImage();
}

forecastResult x86Connector::getNewestWeatherForecast(FORECAST_TYPE type)
{
    return commonPart->getNewestWeatherForecast(type);
}
forecastResult x86Connector::newestWeatherForecast(GPSpoint where)
{
    return commonPart->newestWeatherForecast(where);
}

currentForecastResult x86Connector::getCurrentWeather(location where)
{
    return commonPart->getCurrentWeather(where);
}
currentForecastResult x86Connector::getCurrentWeather(GPSpoint where)
{
    return commonPart->getCurrentWeather(where);
}

void x86Connector::workOnLocalImage(Image_t * img, bool onoff)
{
    commonPart->workOnLocalImage(img, onoff);
}

int x86Connector::loadDefaultLocationList(int &count)
{
    return commonPart->loadDefaultLocationList(count);
}

std::vector<location> x86Connector::getDefaultLocationList()
{
    return commonPart->getDefaultLocationList();
}
location x86Connector::getXYfromGPS(GPSpoint pkt)
{
    return commonPart->getXYfromGPS(pkt);
}
