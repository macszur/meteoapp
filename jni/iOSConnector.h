//
//  iOSConnector.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_iOSConnector_h
#define meteoAppModel_iOSConnector_h

#include "IConnector.h"

class iOSConnector : public IConnector
{
public:
    iOSConnector();
    virtual ~iOSConnector();

    
    virtual bool initialize();
    
    virtual bool loadNewestWeatherForecast(location where);
    virtual const Image_t& getLoadedForecastImage();
    virtual forecastResult getNewestWeatherForecast(FORECAST_TYPE type);
    virtual forecastResult newestWeatherForecast(GPSpoint where);
    
    virtual currentForecastResult getCurrentWeather(location where);
    virtual currentForecastResult getCurrentWeather(GPSpoint where);
    
    virtual void workOnLocalImage(Image_t * img, bool onoff);
    
    virtual int loadDefaultLocationList(int &count);
    virtual std::vector<location> getDefaultLocationList();
    virtual location getXYfromGPS(GPSpoint pkt);
};

#endif
