//
//  iOSConnector.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 26.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "iOSConnector.h"


iOSConnector::iOSConnector()
{
    
}

iOSConnector::~iOSConnector()
{
    
}


bool iOSConnector::initialize()
{
    return false;
}

bool iOSConnector::loadNewestWeatherForecast(location where)
{
    return false;
}

const Image_t& iOSConnector::getLoadedForecastImage()
{
    return Image_t();
}

forecastResult iOSConnector::getNewestWeatherForecast(FORECAST_TYPE type)
{
    return forecastResult();
}
forecastResult iOSConnector::newestWeatherForecast(GPSpoint where)
{
    return forecastResult();
}

currentForecastResult iOSConnector::getCurrentWeather(location where)
{
    return currentForecastResult();
}
currentForecastResult iOSConnector::getCurrentWeather(GPSpoint where)
{
    return currentForecastResult();
}

void iOSConnector::workOnLocalImage(Image_t * img, bool onoff)
{
    
}

int iOSConnector::loadDefaultLocationList(int &count)
{
    return 0;
}

std::vector<location> iOSConnector::getDefaultLocationList()
{
    return std::vector<location>();
}
location iOSConnector::getXYfromGPS(GPSpoint pkt)
{
    return location();
}