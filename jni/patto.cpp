//
//  patto.cpp
//  meteoAppModel
//
//  Created by PrzemcioMac on 02.06.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#include "patto.h"

//patto::patto(char * _filename){
//    png = new pngmatrix(_filename);
//    filename = _filename;
//}

unsigned long long int patto::pattern_for_frame(frame _frame){
    
    unsigned long long int mask = 0;
    int comparator = png -> red_pixel(_frame.y, _frame.x);
    int value = 0;
    
    for (int x = _frame.x; x < _frame.x + _frame.height; x++) {
        for (int y = _frame.y; y < _frame.y + _frame.width; y++) {
            value = png -> red_pixel(y, x);
            mask += comparator != value;
            mask = mask << 1;
            comparator = value;
        }
    }
    return mask;
}

unsigned long long int patto::full_pattern(){
    int width =png->getMatrix()->w;
    int height = png ->getMatrix()->w;
	frame _frame = {0, 0, width, height};
    return pattern_for_frame(_frame);
}