//
//  ReadAndPreparePTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ReadAndPreparePTask__
#define __meteoAppModel__ReadAndPreparePTask__

#include <iostream>
#include <future>
#include "ITask.h"
#include "ReadAndPrepareTimeTaskAns.h"

class ReadAndPrepareTimePTask : public ITask
{
    //std::thread workerT;
    ReadAndPrepareTimeTaskAns taskAns;
    int start_hour = 0;
    tm start_tm;
public:
    ReadAndPrepareTimePTask()
    {
        type = std::launch::async;
    }
    ~ReadAndPrepareTimePTask() = default;
    virtual void setProcessingData(const Image_t * _processData);
    virtual void operator()();
    virtual void getRetValue(ITaskAnsJoiner * joiner);
    
    
    void setCurrImgType(UM_IMAGE_DATE_T type);
};

#endif /* defined(__meteoAppModel__ReadAndPreparePTask__) */
