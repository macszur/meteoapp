//
//  SingleTask.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 23.04.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__SingleTask__
#define __meteoAppModel__SingleTask__

#include <iostream>
#include "ITask.h"

class SingleTask : public ITask {
    
public:
    virtual float run();
    virtual bool waitTillDone();
};

#endif /* defined(__meteoAppModel__SingleTask__) */
