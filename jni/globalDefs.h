//
//  globalDefs.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 25.03.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef meteoAppModel_globalDefs_h
#define meteoAppModel_globalDefs_h

#include <string>
#include <vector>
#include <cwchar>
#include <cstdint>
#include <sstream>
#include <string.h>

#define m__And 1 // STALA DEFINIUJACA NA JAKIM SYSTEMIE ODPALAMY NASZA BIBLIOTEKE

#ifdef m__x86

#define PATH_DELIMETER "/"

#elif m__iOS

#define PATH_DELIMETER "/"

#elif m__And

#define PATH_DELIMETER "/"

namespace std
{
    template<class T>
    std::string to_string(const T& t) {
        std::ostringstream os;
        os << t;
        return os.str();
    }
}

#endif


#ifdef m__x86
#define LOCATION_LIST_PATH "/Users/szurMac/Desktop/projekty/meteoAppModel"
#elif m__iOS
#define LOCATION_LIST_PATH "/Users/szurMac/Desktop/projekty/meteoAppModel"
#elif m__And
#define LOCATION_LIST_PATH "/data/data/com.example.icmmeteo/lib"
#endif

#define LOCATION_LIST_FILENAME "libtxt.so"
#define LOCATION_LIST_FULLPATH LOCATION_LIST_PATH PATH_DELIMETER LOCATION_LIST_FILENAME



// Uzywane typy do trzymania danych
using buff_t = unsigned char;
using buff_pt = buff_t *;

/*
 *
 * Rodzaj pogody
 *
 */
enum class FORECAST_TYPE: int
{
    blank = -1,
    temperature = 0,
    wind,
    pressure,
    rain,
    clouds,
    humidity
    
};

/*
 *
 * Modeluje jedna lokalizacje w ICM Meteo
 *
 */
struct location
{
    int X = 0;
    int Y = 0;
    int locID = 0;
    char name[255] = {0};
    char powiat[255] = {0};
    
    location & operator= (const location & l)
    {
        if (this != &l) // protect against invalid self-assignment
        {
            X = l.X;
            Y = l.Y;
            locID = l.locID;
            strcpy(name, l.name);
            strcpy(powiat, l.powiat);
        }
        
        return *this;
        
    }
};

/*
 *
 * Jeden punkt z wykresu
 *
 */
struct plotPoint // to jeszcze do ew poprawek
{
    double timeStamp;
    float value;
};

/*
 *
 * Struktura z punktami wynikowymi danego wykresu pogodowego
 *
 */
struct forecastResult
{
    int pointsNum = 0;
    int maxScale = 0;
    int minScale = 0;
    plotPoint * points;
    FORECAST_TYPE ftype;
};

/*
 *
 * Aktualny stan pogody w danej lokalizacji
 *
 */
struct currentForecastResult
{
    double timeStamp;
    float temp;
    float pressure;
    float wind;
    float humidity;
    
};

/*
 *
 * Modeluje punkt we wspolrzednych GPS
 *
 */
struct GPSpoint
{
    double X; // to sie chyba inaczej powinno nazywac niz X i Y
    double Y;
};


/*
 *
 * Struktura do podawania obrazka w postaci bajtow z zewnatrz biblioteki do wewnatrz
 *
 */
struct Image_t
{
    unsigned long size;
    unsigned int w;
    unsigned int h;
    std::vector<buff_t> dataBuff;
};


/*
 *
 * Aktualnosc danego zrodla z obrazkiem pogodowym
 *
 */
typedef enum : int {
    CURRENT = 0,
    PREVIOUS,
    CUSTOM
} UM_IMAGE_DATE_T;

const int FORECAST_WIDTH = 413;
const int FORECAST_HOUR_WIDTH = 59;
const int ONE_HOUR_PX_SIZE = 7;
const int FORECAST_STARTING_COLUMN = 65;

struct scale_t
{
    int maxVal;
    int maxY;
    int minVal;
    int minY;
};

#endif
