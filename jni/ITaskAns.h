//
//  ITaskAns.h
//  meteoAppModel
//
//  Created by PrzemcioMac on 31.05.2014.
//  Copyright (c) 2014 PrzemcioMac. All rights reserved.
//

#ifndef __meteoAppModel__ITaskAns__
#define __meteoAppModel__ITaskAns__

#include <iostream>
//#include "ITaskAnsJoiner.h"

class ITaskAnsJoiner;
class ITaskAns
{
public:
    virtual void Accept(ITaskAnsJoiner * joiner) = 0;
};

#endif /* defined(__meteoAppModel__ITaskAns__) */
